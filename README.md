npm install ngx-socket-io

ionic cordova plugin add cordova-sqlite-storage
npm install --save @ionic/storage

ionic cordova plugin add cordova-plugin-file
npm install @ionic-native/file

ionic cordova plugin add cordova-plugin-filepath
npm install @ionic-native/file-path

ionic cordova plugin add cordova-plugin-camera
npm install @ionic-native/camera

ionic cordova plugin add cordova-plugin-file-transfer
npm install @ionic-native/file-transfer

ionic cordova plugin add cordova-plugin-advanced-http
npm install @ionic-native/http

ionic cordova plugin add cordova-plugin-streaming-media
npm install @ionic-native/streaming-media

ionic cordova plugin add com-sarriaroman-photoviewer
npm install @ionic-native/photo-viewer

ionic cordova plugin add cordova-plugin-chooser
npm install @ionic-native/chooser

ionic cordova plugin add cordova-plugin-media
npm install @ionic-native/media

ionic cordova plugin add cordova.plugins.diagnostic
npm install @ionic-native/diagnostic

ionic cordova plugin add cordova-plugin-nativeaudio
npm install @ionic-native/native-audio

ionic cordova plugin add com.paypal.cordova.mobilesdk
npm install @ionic-native/paypal

ionic cordova plugin add cordova-plugin-inapppurchase
npm install @ionic-native/in-app-purchase

ionic cordova plugin add cordova-plugin-ionic-keyboard
npm install @ionic-native/keyboard