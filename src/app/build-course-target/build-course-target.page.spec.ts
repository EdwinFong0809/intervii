import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCourseTargetPage } from './build-course-target.page';

describe('BuildCourseTargetPage', () => {
  let component: BuildCourseTargetPage;
  let fixture: ComponentFixture<BuildCourseTargetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCourseTargetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCourseTargetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
