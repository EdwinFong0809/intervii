import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseTargetPage } from './build-course-target.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseTargetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseTargetPageRoutingModule {}
