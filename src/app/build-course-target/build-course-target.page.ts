import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
@Component({
  selector: 'app-build-course-target',
  templateUrl: './build-course-target.page.html',
  styleUrls: ['./build-course-target.page.scss'],
})
export class BuildCourseTargetPage implements OnInit {
  data_ready: any = {
    require_tools: "",
    require_knowledge: "",
    learn_result: "",
    target_student: ""
  };
  course_id: any;
  course: any = {
    require_tools: "",
    require_knowledge: "",
    learn_result: "",
    target_student: ""
  };

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.course = {
      require_tools: "",
      require_knowledge: "",
      learn_result: "",
      target_student: ""
    };
    //console.log('ionViewDidLoad BuildyourcourseCreateTargetPage');
    this.course = this.global_service.create_course;
    this.course_id = this.course.id;

  }
  async Dismiss() {
    if (this.course.require_tools == "" || this.course.require_knowledge == "" ||
      this.course.learn_result == "" || this.course.target_student == "") {
      let msg_title = "請填上所有項目";
      let msg_option_1 = "確認";
      if (this.global_service.lang == 'en') {
        msg_title = "Please fill in all items";
        msg_option_1 = "Confirm";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "请填上所有项目";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [{ text: msg_option_1 }]
      });
      await alert_box.present();
    } else {
      this.UploadData();
    }
  }
  async GoBack() {
    if (this.course.require_tools == "" || this.course.require_knowledge == "" ||
      this.course.learn_result == "" || this.course.target_student == "") {
      this.global_service.resetLoading();
      this.navCtrl.pop();

    } else {
      let msg_title = "確認不保存離開?";
      let msg_option_1 = "確認";
      let msg_option_2 = "取消";
      if (this.global_service.lang == 'en') {
        msg_title = "Confirm leave without save";
        msg_option_1 = "Confirm";
        msg_option_2 = "Cancel";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "确认不保存离开?";
        msg_option_1 = "确认";
        msg_option_2 = "取消";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [{
          text: msg_option_1, handler: () => {
            this.global_service.resetLoading();
            this.navCtrl.pop();
          }
        },
        {
          text: msg_option_2,
          role: 'cancel'
        }]
      });
      await alert_box.present();
    }
  }
  UploadData() {
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id,
      "learn_result": this.course.learn_result,
      "require_knowledge": this.course.require_knowledge,
      "require_tools": this.course.require_tools,
      "target_student": this.course.target_student,
    };
    let body = { "update_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        this.global_service.create_course = this.course;
        this.global_service.resetLoading();
        this.navCtrl.pop();
      });
  }
  async InfoAchievement() {
    let msg_title = "一項技能或真實的成品(例如: 自建火箭登陸月球背面)";
    if (this.global_service.lang == 'en') {
      msg_title = "Skill or item can make (for example : build rocket and reach the backside of moon)";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "一项技能或真实的成品(例如: 自建火箭登陆月球背面)";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      backdropDismiss:false,
      cssClass: 'custom-alertInfo',
    });
    await alert_box.present();
  }
  async InfoBackground() {
    let msg_title = "例如: 會西班牙語";
    if (this.global_service.lang == 'en') {
      msg_title = "For example: understanding Spanish";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "例如: 会西班牙语";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      backdropDismiss:false,
      cssClass: 'custom-alertInfo',
    });
    await alert_box.present();
  }
  async InfoTools() {
    let msg_title = "軟件，工具和材料等 (例如: Photoshop CC 2015)";
    if (this.global_service.lang == 'en') {
      msg_title = "Software, tools and materials (for example: Photoshop CC 2015)";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "软件，工具和材料等 (例如: Photoshop CC 2015)";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      backdropDismiss:false,
      cssClass: 'custom-alertInfo',
    });
    await alert_box.present();
  }
  async InfoWho() {
    let msg_title = "例如喜歡攝影的人";
    if (this.global_service.lang == 'en') {
      msg_title = "For example people like to take photo";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "例如喜欢摄影的人";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      backdropDismiss:false,
      cssClass: 'custom-alertInfo',
    });
    await alert_box.present();
  }

}
