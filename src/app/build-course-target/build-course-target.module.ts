import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseTargetPageRoutingModule } from './build-course-target-routing.module';

import { BuildCourseTargetPage } from './build-course-target.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseTargetPageRoutingModule
  ],
  declarations: [BuildCourseTargetPage]
})
export class BuildCourseTargetPageModule {}
