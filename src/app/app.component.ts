import { Component } from '@angular/core';

import { Platform, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private nav: NavController,
    private androidPermissions: AndroidPermissions,
     private Ctrl: AlertController,
     private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    console.log("initializeApp");
    // alert("initializeApp");
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      console.log("OneSignal.startInit");
      var iosSettings = {
        "kOSSettingsKeyAutoPrompt":true,
        "kOSSettingsKeyInAppLaunchURL":false
      };
          // iosSettings["kOSSettingsKeyAutoPrompt"] = true;
          // iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

      window["plugins"].OneSignal.getPermissionSubscriptionState((status) => {
        this.storage.set('notification_user_id', status.subscriptionStatus.userId);
        console.log("notification_user_id: "+status.subscriptionStatus.userId);
        // status.subscriptionStatus.pushToken; // String: Device Identifier from FCM/APNs
      });
      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      window["plugins"].OneSignal
        .startInit("c379db79-414d-431a-a084-89a7c6b8f95f", "249362446595")
        .handleNotificationOpened(notificationOpenedCallback)
        // .iOSSettings(iosSettings)
        // .setSubscription(true)
        // .allowNotification(true)
        // .setLogLevel({logLevel: 6, visualLevel: 6})
        .endInit();

      window["plugins"].OneSignal.getPermissionSubscriptionState((status) => {
        // let alert = this.alertCtrl.create({
        //   subTitle: '',
        //   buttons: ['OK']
        // });
        this.storage.set('notification_user_id', status.subscriptionStatus.userId);
      });



      if (this.platform.is('android')) {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
          result => console.log('Has permission?',result.hasPermission),
          err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
        );
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
          result => console.log('Has permission?',result.hasPermission),
          err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
        );
        
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE]);
      }

      this.nav.navigateRoot('tabs/home');
      // this.nav.navigateRoot('build-course-payment');
    });
  }
}
