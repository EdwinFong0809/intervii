import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseTitlePage } from './build-course-title.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseTitlePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseTitlePageRoutingModule {}
