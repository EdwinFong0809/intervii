import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseTitlePageRoutingModule } from './build-course-title-routing.module';

import { BuildCourseTitlePage } from './build-course-title.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseTitlePageRoutingModule
  ],
  declarations: [BuildCourseTitlePage]
})
export class BuildCourseTitlePageModule {}
