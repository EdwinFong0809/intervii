import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCourseTitlePage } from './build-course-title.page';

describe('BuildCourseTitlePage', () => {
  let component: BuildCourseTitlePage;
  let fixture: ComponentFixture<BuildCourseTitlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCourseTitlePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCourseTitlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
