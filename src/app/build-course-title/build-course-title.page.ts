import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-build-course-title',
  templateUrl: './build-course-title.page.html',
  styleUrls: ['./build-course-title.page.scss'],
})
export class BuildCourseTitlePage implements OnInit {
  data_ready: any = {
    title: "",
    intro: "",
    icon: ""
  };
  file_name: string = "";
  new_icon: string = "";
  base64_data: any;
  course_id: any;
  course: any = {
    title: "",
    intro: "",
    icon: ""
  };
  loading: any;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private camera: Camera,
    private file: File, 
    private filePath: FilePath,
    private photoViewer: PhotoViewer,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.course = {
      title: "",
      intro: "",
      icon: ""
    };

    this.file_name = "";
    this.new_icon = "";
    //console.log('ionViewDidLoad BuildyourcourseCreateTitlePage');
    // this.course = this.navParams.get('course');
    this.course = this.global_service.create_course;
    //console.log(JSON.stringify(this.course));
    this.course_id = this.course.id;
    if (this.course.icon == undefined || this.course.icon == null) {
      this.course.icon = "";
    } else {
      this.new_icon = "https://intervii.com/media/" + this.course.icon;
    }
    this.file_name = this.course.icon;
  }
  async Dismiss() {
    if (this.course.intro != "" && this.course.title != "") {
      this.UploadData();
    } else {
      let msg_title = "請填上所有項目";
      let msg_option_1 = "確認";
      if (this.global_service.lang == 'en') {
        msg_title = "Please fill in all items";
        msg_option_1 = "Confirm";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "请填上所有项目";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [{ text: msg_option_1 }]
      });
      await alert_box.present();
    }

  }
  UploadData() {
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id,
      //"icon" : this.course.new_cover, //upload fuction not ready
      "intro": this.course.intro,
      "title": this.course.title
    };
    let body = { "update_course": JSON.stringify(json) }
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);

        let return_data = [this.course.title, this.course.intro, this.course.icon];
        // this.viewCtrl.dismiss(return_data);
        this.global_service.create_course = this.course;
        this.global_service.resetLoading();
        this.navCtrl.pop();

      });
  }
  async GoBack() {
    if (this.course.title == "" && this.course.intro == "" &&
      this.course.new_cover == "") {
        this.global_service.resetLoading();
        this.navCtrl.pop();

    } else {
      let msg_title = "確認不保存離開?";
      let msg_option_1 = "確認";
      let msg_option_2 = "取消";
      if (this.global_service.lang == 'en') {
        msg_title = "Confirm leave without save";
        msg_option_1 = "Confirm";
        msg_option_2 = "Cancel";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "确认不保存离开?";
        msg_option_1 = "确认";
        msg_option_2 = "取消";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [{ text: msg_option_1, handler: () => { 
          this.global_service.resetLoading();
          this.navCtrl.pop();
         } },
        { text: msg_option_2,
          role: 'cancel' }]
      });
      await alert_box.present();
    }

  }
  uploadPhoto() {
    this.presentActionSheet();
  }
  async presentActionSheet() {
    //console.log(this.new_icon);
    let msg_option_1 = "從手機相簿選擇";
    let msg_option_2 = "拍攝新照片";
    let msg_option_3 = "取消";
    let msg_option_4 = "查看當前照片";
    if (this.global_service.lang == 'en') {
      msg_option_1 = "Camera roll";
      msg_option_2 = "Take photo";
      msg_option_3 = "Cancel";
      msg_option_4 = "View current image";
    }
    if (this.global_service.lang == 'cn') {
      msg_option_1 = "从手机相簿选择";
      msg_option_2 = "拍摄新照片";
      msg_option_3 = "取消";
      msg_option_4 = "查看当前照片";
    }
    if (this.new_icon == undefined || this.new_icon == "") {
      let actionSheet = await this.actionSheetCtrl.create({
        header: 'Select Image Source',
        buttons: [
          {
            text: msg_option_1,
            handler: () => {
              //console.log('Load from Library');
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            }
          },
          {
            text: msg_option_2,
            handler: () => {
              //console.log('Load Camera');
              this.takePicture(this.camera.PictureSourceType.CAMERA);
            }
          },
          {
            text: msg_option_3,
            role: 'cancel'
          }
        ]
      });
      await actionSheet.present();
    } else {

      let actionSheet = await this.actionSheetCtrl.create({
        header: msg_option_1,
        buttons: [
          {
            text: msg_option_1,
            handler: () => {
              //console.log('Load from Library');
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            }
          },
          {
            text: msg_option_2,
            handler: () => {
              //console.log('Load Camera');
              this.takePicture(this.camera.PictureSourceType.CAMERA);
            }
          },
          {
            text: msg_option_4,
            handler: () => {
              this.photoViewer.show(this.new_icon);
            }
          },
          {
            text: msg_option_3,
            role: 'cancel'
          }
        ]
      });
      await actionSheet.present();
    }
  }

  uploadPhotoData() {
    let upload_data = {
      "course_id": this.course_id,
      "file_data": this.base64_data,
      "file_type": "jpg"
    }

    let url = "https://intervii.com/backend/request.php";
    var body = { "update_course_icon": JSON.stringify(upload_data) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        this.course.icon = temp.data.icon;
        if (this.course.icon == undefined || this.course.icon == null) {
          this.course.icon = "";
        } else {
          this.new_icon = "https://intervii.com/media/" + this.course.icon;
        }
        this.file_name = this.course.icon;
        // this.navCtrl.pop();
      });
  }

  takePicture(sourceType) {
    //console.log('TakePicture');
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      // saveToPhotoAlbum: false,
      // correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 1000,
      targetHeight: 1000
    };

    this.camera.getPicture(options).then((imageData) => {
      this.base64_data = 'data:image/jpeg;base64,' + imageData;
      //console.log("imageData");
      //console.log(this.base64_data);
      this.uploadPhotoData();
      this.file_name = "";
      this.new_icon = "";
    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }

}
