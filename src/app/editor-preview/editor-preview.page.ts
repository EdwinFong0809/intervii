import { Component, OnInit } from '@angular/core';
import { NavController, NavParams,AlertController } from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-editor-preview',
  templateUrl: './editor-preview.page.html',
  styleUrls: ['./editor-preview.page.scss'],
})
export class EditorPreviewPage implements OnInit {

  content:any = "";
  constructor(
    public navCtrl: NavController,
    private sanitizer: DomSanitizer, 
    public global_service: GlobalService,
    // public navParams: NavParams
    ) {
    // this.course = {
    //   profile: ""
    // };
    this.content = "";
    var temp = this.global_service.editor_preview_text.replace(/\n/g, "<br>");
    this.content = this.sanitizer.bypassSecurityTrustHtml(temp);
    //console.log(this.content);
  }

  ngOnInit() {
  }

  GoBack() {
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
}
