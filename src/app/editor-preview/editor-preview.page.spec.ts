import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditorPreviewPage } from './editor-preview.page';

describe('EditorPreviewPage', () => {
  let component: EditorPreviewPage;
  let fixture: ComponentFixture<EditorPreviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorPreviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditorPreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
