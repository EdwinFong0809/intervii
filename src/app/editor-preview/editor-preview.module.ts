import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditorPreviewPageRoutingModule } from './editor-preview-routing.module';

import { EditorPreviewPage } from './editor-preview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditorPreviewPageRoutingModule
  ],
  declarations: [EditorPreviewPage]
})
export class EditorPreviewPageModule {}
