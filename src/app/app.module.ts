import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { IonicStorageModule } from '@ionic/storage';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { HTTP } from '@ionic-native/http/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { Media } from '@ionic-native/media/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { PayPal } from "@ionic-native/paypal/ngx";
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

const config: SocketIoConfig = { url: 'https://intervii.com:3781', options: {} };

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config)
  ],
  providers: [ 
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    File,
    FilePath,
    Camera,
    FileTransfer,
    HTTP,
    Keyboard,
    StreamingMedia,
    PhotoViewer,
    Chooser,
    Media,
    Diagnostic,
    NativeAudio,
    PayPal,
    Facebook,
    InAppPurchase,
    InAppBrowser,
    AndroidPermissions,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
