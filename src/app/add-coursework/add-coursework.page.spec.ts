import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddCourseworkPage } from './add-coursework.page';

describe('AddCourseworkPage', () => {
  let component: AddCourseworkPage;
  let fixture: ComponentFixture<AddCourseworkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCourseworkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddCourseworkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
