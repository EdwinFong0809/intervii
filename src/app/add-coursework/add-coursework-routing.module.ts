import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCourseworkPage } from './add-coursework.page';

const routes: Routes = [
  {
    path: '',
    component: AddCourseworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCourseworkPageRoutingModule {}
