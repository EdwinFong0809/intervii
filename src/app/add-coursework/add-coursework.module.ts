import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCourseworkPageRoutingModule } from './add-coursework-routing.module';

import { AddCourseworkPage } from './add-coursework.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCourseworkPageRoutingModule
  ],
  declarations: [AddCourseworkPage]
})
export class AddCourseworkPageModule {}
