import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';

@Component({
  selector: 'app-add-coursework',
  templateUrl: './add-coursework.page.html',
  styleUrls: ['./add-coursework.page.scss'],
})
export class AddCourseworkPage implements OnInit {
  multi_coursework: any = { "assignment_id": 0, "course_id": 0, "lecturer_id": 0, "user_id": 0, "content": "", "file_data": "", "file_type": "" };
  number_of_coursework: any = 0;
  course: any;
  course_id: any;
  assignment_id: any = 0;
  user_id: any;
  loading: any;
  uploading: boolean = false;
  coursework_data: any;
  coursework_list: any = [];
  base64_data: any = "";

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private camera: Camera,
    private file: File,
    private filePath: FilePath,
    private transfer: FileTransfer,
    public actionSheetCtrl: ActionSheetController,
    private chooser: Chooser,
  ) {
    this.coursework_data = { "assignment_id": 0, "course_id": 0, "lecturer_id": 0, "user_id": 0, "content": "", "file_data": "", "file_type": "" };
    this.coursework_list = [];
    this.assignment_id = 0;
    this.base64_data = "";
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.course = this.global_service.course_data;
    this.coursework_data.lecturer_id = this.course.lecturer_id;
    this.coursework_data.course_id = this.course.id;
    this.course_id = this.course.id;
    this.user_id = this.global_service.user_data.id;
    this.coursework_data.user_id = this.global_service.user_data.id;
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_assignments": JSON.stringify(this.course.assignment_list) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        if (JSON.parse(data.data).result == "success") {
          let assignments = JSON.parse(data.data).data;
          this.coursework_list = assignments;
        }
        this.global_service.dismiss();
      });

    if (this.global_service.back_update) {
      this.coursework_data = this.global_service.new_coursework;
      this.global_service.back_update = false;
      this.coursework_data.content = this.global_service.editor_text;
    } else {
      if (this.global_service.new_coursework != null) {
        this.coursework_data = this.global_service.new_coursework;
      }
    }
  }
  async Dismiss() {
    let msg_title = "請輸入所有內容";
    let msg_option_1 = "確認";
    if (this.global_service.lang == 'en') {
      msg_title = "Please fill in all content";
      msg_option_1 = "Confirm";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "请输入所有内容";
      msg_option_1 = "确认";
    }
    let is_data_pass = true;
    if (this.coursework_data.file_data == "" || this.coursework_data.content == "" || this.coursework_data.assignment_id == 0) {
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
      is_data_pass = false;
    }
    //console.log(this.uploading);
    //console.log("this.uploading");
    //console.log(is_data_pass);
    //console.log("is_data_pass");
    if (this.uploading == false && is_data_pass) {
      this.uploading = true;
      this.UpdateFinalList();
    }
  }
  EnterContent() {
    //not ready
    this.global_service.new_coursework = this.coursework_data;
    this.global_service.editor_text = this.coursework_data.content;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/editor-page']);
    // let modal2 = this.modalCtrl.create(AddCourseworkInputPage, { coursework: this.coursework_data });
    // modal2.onDidDismiss(data => {
    //   //console.log(data);
    //   if (data != undefined) {
    //   }

    // })
    // modal2.present();
  }

  SelectedCourseworkChange($event) {
    //console.log($event);
    // this.assignment_id = $event;
    //console.log(this.assignment_id);
    this.coursework_data.assignment_id = this.assignment_id;
  }

  UpdateFinalList() {
    let url = "https://intervii.com/backend/request.php";
    // //console.log(this.coursework_data);
    let body = { "new_assignment_record": JSON.stringify(this.coursework_data) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    // //console.log(this.coursework_data);
    this.http.post(url, body, headers)
      .then(async data => {
        //console.log(data);
        this.global_service.dismiss();
        this.uploading = false;
        let temp = JSON.parse(data.data);
        let msg_title = "提交成功";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Success";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "提交成功";
          msg_option_1 = "确认";
        }
        if (temp.result == "success") {
          const alert_box = await this.alert.create({
            message: msg_title,
            cssClass: 'custom-alertControl',
            backdropDismiss:false,
            buttons: [{
              text: msg_option_1, handler: () => {
                this.global_service.new_coursework = null;
                this.global_service.resetLoading();
                this.navCtrl.pop();
              }
            }]
          });
          await alert_box.present();
        } else {
          msg_title = "提交失敗";
          if (this.global_service.lang == 'en') {
            msg_title = "Fail";
          }
          if (this.global_service.lang == 'cn') {
            msg_title = "提交失败";
          }
          const alert_box = await this.alert.create({
            message: msg_title,
            cssClass: 'custom-alertControl',
            backdropDismiss:false,
            buttons: [msg_option_1]
          });
          await alert_box.present();
        }
        this.global_service.dismiss();
      });
  }
  GoBack() {
    this.global_service.new_coursework = null;
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  uploadPhoto() {
    this.presentActionSheet();
  }
  async presentActionSheet() {
    let msg_option_1 = "從手機相簿選擇";
    let msg_option_2 = "拍攝新照片";
    let msg_option_3 = "取消";
    if (this.global_service.lang == 'en') {
      msg_option_1 = "Camera roll";
      msg_option_2 = "Take photo";
      msg_option_3 = "Cancel";
    }
    if (this.global_service.lang == 'cn') {
      msg_option_1 = "从手机相簿选择";
      msg_option_2 = "拍摄新照片";
      msg_option_3 = "取消";
    }
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: msg_option_1,
          handler: () => {
            //console.log('Load from Library');
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: msg_option_2,
          handler: () => {
            //console.log('Load Camera');
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: msg_option_3,
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  takePicture(sourceType) {
    //console.log('TakePicture');
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      // saveToPhotoAlbum: false,
      // correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // allowEdit: true,
      targetWidth: 1000,
      targetHeight: 1000
    };
    let msg_title = "圖片載入完成";
    let msg_option_1 = "確認";
    if (this.global_service.lang == 'en') {
      msg_title = "Load success";
      msg_option_1 = "Confirm";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "图片载入完成";
      msg_option_1 = "确认";
    }


    this.camera.getPicture(options).then(async (imageData) => {
      this.base64_data = 'data:image/jpeg;base64,' + imageData;
      this.coursework_data.file_data = this.base64_data;
      this.coursework_data.file_type = "jpg";
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }, async (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
      msg_title = "圖片載入失敗";
      if (this.global_service.lang == 'en') {
        msg_title = "Load fail";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "图片载入失败";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    });
  }
}
