import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { EventsService } from '../events.service';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-follow-tutor',
  templateUrl: './follow-tutor.page.html',
  styleUrls: ['./follow-tutor.page.scss'],
})
export class FollowTutorPage implements OnInit {
  lecturer_list: any = [];
  latest_course: any = [];
  latest_course_has_preview: any = [];
  latest_course_count: any = 0;
  has_preview_count: any = 0;
  user_data: any  = null;
  logined: boolean = false;
  selected_lecturer: any = 0;
  price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];
  missing_text1: string = "";
  missing_text2: string = "";
  missing_text3: string = "";
  connecting_server: boolean = false;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
    public events: EventsService,
  ) {
    this.events.getObservable().subscribe((data) => {
      console.log(data);
      if(data.data == "refresh_follow"){
        this.price_list = this.global_service.price_list;
        this.connecting_server = true;
        this.missing_text1 = "";
        this.missing_text2 = "";
        this.missing_text3 = "";
        this.lecturer_list = [];
        this.selected_lecturer = 0;
    
        this.user_data = this.global_service.user_data;
        if (this.user_data != null) {
          let url = "https://intervii.com/backend/request.php";
          var json = {
            "user_id": this.user_data.id
          };
          let body = { "get_user_by_id": JSON.stringify(json) };
          //let body = { "current_datetime_string" : ""};
          let headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
          };
          this.global_service.isLoading = true;
          this.global_service.presentLoading();
          this.http.post(url, body, headers)
            .then(data => {
              this.global_service.dismiss();
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                this.user_data = temp.data;
                let url2 = "https://intervii.com/backend/request.php";
                var json2 = {
                  "user_id_list": this.user_data.following
                };
                let body2 = { "get_user_by_id_list": JSON.stringify(json2) };
                //let body = { "current_datetime_string" : ""};
                let headers2 = {
                  'Content-Type': 'application/x-www-form-urlencoded'
                };
                this.global_service.isLoading = true;
                this.global_service.presentLoading();
                this.http.post(url2, body2, headers2)
                  .then(data => {
                    this.global_service.dismiss();
                    let temp2 = JSON.parse(data.data);
                    if (temp2.result == "success") {
                      this.lecturer_list = temp2.data;
                      //console.log(this.lecturer_list);
                      if (this.lecturer_list.length > 0) {
                        // this.SelectLecturer(this.lecturer_list[0].id);
                        this.GetClasses();
                      } else {
                        if(this.global_service.lang == 'en'){
                          this.missing_text1 = "No following Tutor";
                        }else if(this.global_service.lang == 'cn'){
                          this.missing_text1 = "沒有追踪中的导师";
                        }else{
                          this.missing_text1 = "没有追踪中的導師";
                        }
                    
                      }
                    } else {
                      if(this.global_service.lang == 'en'){
                        this.missing_text1 = "No following Tutor";
                      }else if(this.global_service.lang == 'cn'){
                        this.missing_text1 = "沒有追踪中的导师";
                      }else{
                        this.missing_text1 = "没有追踪中的導師";
                      }
                    }
                    this.connecting_server = false;
                  });
              } else {
                this.user_data = {};
                if(this.global_service.lang == 'en'){
                  this.missing_text1 = "Please login first";
                }else if(this.global_service.lang == 'cn'){
                  this.missing_text1 = "請先登入";
                }else{
                  this.missing_text1 = "请先登入";
                }
                this.connecting_server = false;
              }
            });
        }else{
          if(this.global_service.lang == 'en'){
            this.missing_text1 = "Please login first";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text1 = "請先登入";
          }else{
            this.missing_text1 = "请先登入";
          }
          this.connecting_server = false;
        }
    
        this.latest_course = [];
        this.latest_course_has_preview = [];
          
      }
    });
   }

  ngOnInit() {
  }

  ionNavDidChange(){
    console.log("ionNavDidChange");
  }

  ionViewWillEnter() {
    if(this.global_service.lang == 'en'){
      this.missing_text1 = "Please login first";
    }else if(this.global_service.lang == 'cn'){
      this.missing_text1 = "請先登入";
    }else{
      this.missing_text1 = "请先登入";
    }
    this.price_list = this.global_service.price_list;
    this.connecting_server = true;
    this.missing_text1 = "";
    this.missing_text2 = "";
    this.missing_text3 = "";
    this.lecturer_list = [];
    this.selected_lecturer = 0;

    this.user_data = this.global_service.user_data;
    if (this.user_data != null) {
      let url = "https://intervii.com/backend/request.php";
      var json = {
        "user_id": this.user_data.id
      };
      let body = { "get_user_by_id": JSON.stringify(json) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.user_data = temp.data;
            let url2 = "https://intervii.com/backend/request.php";
            var json2 = {
              "user_id_list": this.user_data.following
            };
            let body2 = { "get_user_by_id_list": JSON.stringify(json2) };
            //let body = { "current_datetime_string" : ""};
            let headers2 = {
              'Content-Type': 'application/x-www-form-urlencoded'
            };
            this.global_service.isLoading = true;
            this.global_service.presentLoading();
            this.http.post(url2, body2, headers2)
              .then(data => {
                this.global_service.dismiss();
                let temp2 = JSON.parse(data.data);
                if (temp2.result == "success") {
                  this.lecturer_list = temp2.data;
                  //console.log(this.lecturer_list);
                  if (this.lecturer_list.length > 0) {
                    // this.SelectLecturer(this.lecturer_list[0].id);
                    this.GetClasses();
                  } else {
                    if(this.global_service.lang == 'en'){
                      this.missing_text1 = "No following Tutor";
                    }else if(this.global_service.lang == 'cn'){
                      this.missing_text1 = "沒有追踪中的导师";
                    }else{
                      this.missing_text1 = "没有追踪中的導師";
                    }
                  }
                } else {
                  if(this.global_service.lang == 'en'){
                    this.missing_text1 = "No following Tutor";
                  }else if(this.global_service.lang == 'cn'){
                    this.missing_text1 = "沒有追踪中的导师";
                  }else{
                    this.missing_text1 = "没有追踪中的導師";
                  }
                }
                this.connecting_server = false;
              });
          } else {
            this.user_data = {};
            if(this.global_service.lang == 'en'){
              this.missing_text1 = "Please login first";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text1 = "請先登入";
            }else{
              this.missing_text1 = "请先登入";
            }
            this.connecting_server = false;
          }
        });
    }else{
      if(this.global_service.lang == 'en'){
        this.missing_text1 = "Please login first";
      }else if(this.global_service.lang == 'cn'){
        this.missing_text1 = "請先登入";
      }else{
        this.missing_text1 = "请先登入";
      }
      this.connecting_server = false;
    }

    this.latest_course = [];
    this.latest_course_has_preview = [];
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad FollowtutorPage');
    document.getElementById('row_card_course').style.width = this.latest_course.length * 90 + "%";
    document.getElementById('row_card_trial').style.width = this.latest_course.length * 90 + "%";
  }

  AddFavouriteCourse(id) {
    if (this.user_data != undefined && this.user_data != null && this.user_data.id != 0 && this.user_data.id != undefined) {
      //console.log(this.user_data);

      let url = "https://intervii.com/backend/request.php";
      let body = { "follow_course": JSON.stringify({ "user_id": this.global_service.user_data.id, "course_id": id }) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      //console.log("==================add favourite body ");
      //console.log(body);
      // this.global_service.isLoading = true;
      // this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          // this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log(data.data);
          if (temp.result == "success") {
            this.user_data.follow_course = temp.data;
            this.global_service.user_data = this.user_data;
            // this.storage.set('user_data', this.user_data);


            this.latest_course.forEach((course_data, index) => {
              if(course_data.id == id){
                if(this.user_data.follow_course.includes(id)){
                  if(!course_data.followers.includes(this.user_data.id)){
                    course_data.followers.push(this.user_data.id);
                  }
                }else{
                  if(course_data.followers.includes(this.user_data.id)){
                    let index = course_data.followers.indexOf(this.user_data.id, 0);
                    if (index > -1) {
                      course_data.followers.splice(index, 1);
                    }        
                  }
                }
              }
            });
          }
          //console.log(this.user_data);
        });
    }
  }

  GetClasses() {
    let id_list = [];
    for (let i = 0; i < this.lecturer_list.length; i++) {
      id_list.push(this.lecturer_list[i].id);
    }

    let url = "https://intervii.com/backend/request.php";
    let json = { user_id_list: id_list };
    let body = { "get_multiple_user_open_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        // //console.log(data.data);
        if (temp.result == "success") {
          //console.log(temp.data);
          for (let i = 0; i < temp.data.length; i++) {
            if (temp.data[i].rank == undefined) {
              temp.data[i].rank = 0;
            }
          }
          this.latest_course = temp.data;
          this.latest_course_count = this.latest_course.length;
          this.latest_course_has_preview = [];

          this.latest_course_has_preview = this.latest_course.filter(d => d.has_preview != undefined && d.has_preview);

          this.has_preview_count = this.latest_course_has_preview.length;

          if (this.latest_course.length == 0) {
            if(this.global_service.lang == 'en'){
              this.missing_text2 = "No Course";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text2 = "没有课堂";
            }else{
              this.missing_text2 = "沒有課堂";
            }
          }
          if (this.latest_course.length == 0) {
            if(this.global_service.lang == 'en'){
              this.missing_text3 = "No Preview";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text3 = "没有试堂";
            }else{
              this.missing_text3 = "沒有試堂";
            }
          }
        } else {
          if(this.global_service.lang == 'en'){
            this.missing_text2 = "No Course";
            this.missing_text3 = "No Preview";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text2 = "没有课堂";
            this.missing_text3 = "没有试堂";
          }else{
            this.missing_text2 = "沒有課堂";
            this.missing_text3 = "沒有試堂";
          }
        }
        this.connecting_server = false;
      });
  }

  SelectLecturer(id) {
    if (this.selected_lecturer == 0) {
      this.selected_lecturer = id;
      this.latest_course_count = 0;//this.latest_course.length;
      for (let i = 0; i < this.latest_course.length; i++) {
        if (this.latest_course[i].lecturer_id == id) {
          this.latest_course_count += 1;
        }
      }
      this.has_preview_count = 0;//this.latest_course_count.length;
      for (let i = 0; i < this.latest_course_has_preview.length; i++) {
        if (this.latest_course_has_preview[i].lecturer_id == id) {
          this.has_preview_count += 1;
        }
      }
    } else {
      if (this.selected_lecturer == id) {
        this.selected_lecturer = 0;
        this.latest_course_count = this.latest_course.length;
        this.has_preview_count = this.latest_course_has_preview.length;
      } else {
        this.selected_lecturer = id;
        this.latest_course_count = 0;//this.latest_course.length;
        for (let i = 0; i < this.latest_course.length; i++) {
          if (this.latest_course[i].lecturer_id == id) {
            this.latest_course_count += 1;
          }
        }
        this.has_preview_count = 0;//this.latest_course_count.length;
        for (let i = 0; i < this.latest_course_has_preview.length; i++) {
          if (this.latest_course_has_preview[i].lecturer_id == id) {
            this.has_preview_count += 1;
          }
        }
      }
    }
  }

  OpenCourse(course) {
    // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
    this.global_service.course_data = course;
    this.global_service.from_follow = true;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/course-detail']);
  }
  GoProfile(id) {
    // this.app.getRootNav().push(MyaccountPage, { id: id });
    if (this.user_data == undefined || this.user_data == null || this.user_data.id == 0) {
    } else {
      this.global_service.from_follow = true;
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      this.global_service.view_user_id = id;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    }
  }

  login(){
    this.navCtrl.navigateForward(['/login']);
  }
}
