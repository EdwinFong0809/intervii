import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowTutorPageRoutingModule } from './follow-tutor-routing.module';

import { FollowTutorPage } from './follow-tutor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowTutorPageRoutingModule
  ],
  declarations: [FollowTutorPage]
})
export class FollowTutorPageModule {}
