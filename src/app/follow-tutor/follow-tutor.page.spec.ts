import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FollowTutorPage } from './follow-tutor.page';

describe('FollowTutorPage', () => {
  let component: FollowTutorPage;
  let fixture: ComponentFixture<FollowTutorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowTutorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FollowTutorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
