import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowTutorPage } from './follow-tutor.page';

const routes: Routes = [
  {
    path: '',
    component: FollowTutorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowTutorPageRoutingModule {}
