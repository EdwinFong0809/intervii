import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseFinishPage } from './build-course-finish.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseFinishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseFinishPageRoutingModule {}
