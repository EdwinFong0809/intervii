import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseFinishPageRoutingModule } from './build-course-finish-routing.module';

import { BuildCourseFinishPage } from './build-course-finish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseFinishPageRoutingModule
  ],
  declarations: [BuildCourseFinishPage]
})
export class BuildCourseFinishPageModule {}
