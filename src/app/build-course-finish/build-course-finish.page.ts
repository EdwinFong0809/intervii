import { Component, OnInit } from '@angular/core';
import { GlobalService } from "../global.service";
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-build-course-finish',
  templateUrl: './build-course-finish.page.html',
  styleUrls: ['./build-course-finish.page.scss'],
})
export class BuildCourseFinishPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public global_service: GlobalService,
  ) { }

  ngOnInit() {
  }

  Finished(){
    this.navCtrl.navigateRoot(['/tabs/home']);
  }
}
