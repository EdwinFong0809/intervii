import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  icon_base64: any = "";
  banner_base64: any = "";
  user_data: any = {};
  profile: any = {};
  info: string = "";
  display_name: string = "";
  phone: string = "";
  fb_name: string = "";
  website: string = "";
  password: string = "";
  loading: any;
  from: string = "";
  no_email: boolean = false;

  constructor(
    private storage: Storage,
    public http: HTTP,
    private camera: Camera,
    private file: File,
    private filePath: FilePath,
    private alert: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public global_service: GlobalService,
    public navCtrl: NavController,
  ) {
    // this.user_data = this.navParams.get('user_data');
    // this.profile = this.navParams.get('profile');
    // this.from = this.navParams.get('page');
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.user_data = this.global_service.user_data;
    if(this.user_data.email == ""){
      this.no_email = true;
    }else{
      this.no_email = false;
    }
    // if (this.ValidatePassword(this.password)) {
    // }
    this.profile = this.global_service.user_profile;
    this.from = this.global_service.edit_profile_from;
    this.info = this.user_data.info;
    this.phone = this.user_data.phone;
    this.display_name = this.user_data.display_name;
    this.fb_name = this.user_data.fb_name;
    this.website = this.user_data.website;
    if(this.global_service.back_update){
      this.global_service.back_update = false;
      this.user_data.info = this.global_service.editor_text;
      if(this.global_service.editing_profile == true){
        this.global_service.editing_profile = false;
        let temp = JSON.parse(this.global_service.editing_profile_data);
        this.info = temp.info;
        this.phone = temp.phone;
        this.display_name = temp.display_name;
        this.fb_name = temp.fb_name;
        this.website = temp.website;
      }
    }else{
      let url = "https://intervii.com/backend/request.php";
      var json = {
        "user_id": this.user_data.id
      };
      let body = { "get_user_by_id": JSON.stringify(json) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(async data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.user_data = temp.data;
            this.profile = temp.data;
            this.storage.set('user_data', temp.data);
            this.global_service.user_data = temp.data;
            if(this.user_data.email == ""){
              this.no_email = true;
            }else{
              this.no_email = false;
            }
            this.profile = this.global_service.user_profile;
            this.from = this.global_service.edit_profile_from;
            this.info = this.user_data.info;
            this.phone = this.user_data.phone;
            this.display_name = this.user_data.display_name;
            this.fb_name = this.user_data.fb_name;
            this.website = this.user_data.website;
            if(this.global_service.editing_profile == true){
              this.global_service.editing_profile = false;
              let temp = JSON.parse(this.global_service.editing_profile_data);
              this.info = temp.info;
              this.phone = temp.phone;
              this.display_name = temp.display_name;
              this.fb_name = temp.fb_name;
              this.website = temp.website;
            }
          }
        });
    }
    
  }

  GoBack() {
    this.global_service.back_update = true;
    // this.navCtrl.pop();
    if (this.from == "login") {
      // this.app.getRootNav().setRoot(TabsPage);
      // this.viewCtrl.dismiss();
        this.global_service.resetLoading();
        this.navCtrl.navigateRoot(['/tabs/tab3']);
    } else {
      // this.viewCtrl.dismiss(return_data);
        this.global_service.resetLoading();
      this.navCtrl.pop();
    }
  }


  async update() {
    let url = "https://intervii.com/backend/request.php";
    var json = {};
    if(this.password != "" && this.password.length>8){
      json = {
        "id": this.user_data.id,
        "info": this.user_data.info,
        "phone": this.phone,
        "email": this.user_data.email,
        "fb_name": this.fb_name,
        "display_name": this.display_name,
        "website": this.website,
        "password": this.password
      };
    }else{
      json = {
        "id": this.user_data.id,
        "info": this.user_data.info,
        "phone": this.phone,
        "email": this.user_data.email,
        "fb_name": this.fb_name,
        "display_name": this.display_name,
        "website": this.website
      };
    }
    if ((this.display_name.length < 3 || this.display_name.length > 20) || this.TextRemover(this.display_name) != "") {
      let msg_title = "顯示名稱不正確，請輸入3-20位英數字元";
    let msg_option_1 = "確定";
    if(this.global_service.lang == 'en'){
      msg_title = "Display name format incorrect, please input 3-20 alphabet or number";
      msg_option_1 = "Confirm";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "显示名称不正确，请输入3-20位英数字元";
      msg_option_1 = "确定";
    }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        cssClass: 'custom-alertControl',
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }else{
    let body = { "update_user": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(async data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.user_data = temp.data;
          this.profile = temp.data;
          this.storage.set('user_data', temp.data);
          this.global_service.user_data = temp.data;

          let return_data = { "user_data": this.user_data, "profile": this.profile };
          // this.viewCtrl.dismiss(return_data);
          if (this.display_name == undefined || this.display_name == null || this.display_name == "") {
            let msg_option_1 = "請先輸入顯示名稱";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Please input display name";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "请先输入显示名称";
            }
            const alert_box = await this.alert.create({
              message: "請先輸入顯示名稱",
              backdropDismiss:false,
            });
            await alert_box.present();
          } else {
            if (this.from == "login") {
              // this.app.getRootNav().setRoot(TabsPage);
              // this.viewCtrl.dismiss();
                this.global_service.resetLoading();
                this.navCtrl.navigateRoot(['/tabs/home']);
            } else {
              this.global_service.resetLoading();
              // this.viewCtrl.dismiss(return_data);
              this.navCtrl.pop();
            }
          }


        } else {
          if (data.data = "name exist") {
              alert("顯示名稱已被使用");
          }
        }
      });
    }
  }

  async uploadIcon() {
    let msg_option_1 = "從手機相簿選擇";
    let msg_option_2 = "拍攝新照片";
    let msg_option_3 = "取消";
    let msg_option_4 = "查看當前照片";
    if (this.global_service.lang == 'en') {
      msg_option_1 = "Camera roll";
      msg_option_2 = "Take photo";
      msg_option_3 = "Cancel";
      msg_option_4 = "View current image";
    }
    if (this.global_service.lang == 'cn') {
      msg_option_1 = "从手机相簿选择";
      msg_option_2 = "拍摄新照片";
      msg_option_3 = "取消";
      msg_option_4 = "查看当前照片";
    }
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: msg_option_1,
          handler: () => {
            //console.log('Load from Library');
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: msg_option_2,
          handler: () => {
            //console.log('Load Camera');
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: msg_option_3,
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  takePicture(sourceType) {
    var options = {
      quality: 100,
      sourceType: sourceType,
      // saveToPhotoAlbum: false,
      // correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500
    };

    this.camera.getPicture(options).then((imageData) => {
      this.icon_base64 = 'data:image/jpeg;base64,' + imageData;
      this.uploadIconData();
    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }

  uploadIconData() {
    let upload_data = {
      "user_id": this.user_data.id,
      "file_data": this.icon_base64,
      "file_type": "jpg"
    };

    let url = "https://intervii.com/backend/request.php";
    var body = { "update_user_icon": JSON.stringify(upload_data) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.user_data.icon = temp.data.icon;
          this.profile.icon = temp.data.icon;
          this.storage.set('user_data', this.user_data);
          this.global_service.user_data = temp.data;
        }
      });
  }



  async uploadBanner() {
    let msg_option_1 = "從手機相簿選擇";
    let msg_option_2 = "拍攝新照片";
    let msg_option_3 = "取消";
    let msg_option_4 = "查看當前照片";
    if (this.global_service.lang == 'en') {
      msg_option_1 = "Camera roll";
      msg_option_2 = "Take photo";
      msg_option_3 = "Cancel";
      msg_option_4 = "View current image";
    }
    if (this.global_service.lang == 'cn') {
      msg_option_1 = "从手机相簿选择";
      msg_option_2 = "拍摄新照片";
      msg_option_3 = "取消";
      msg_option_4 = "查看当前照片";
    }
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: msg_option_1,
          handler: () => {
            //console.log('Load from Library');
            this.takeBanner(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: msg_option_2,
          handler: () => {
            //console.log('Load Camera');
            this.takeBanner(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: msg_option_3,
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  takeBanner(sourceType) {
    var options = {
      quality: 100,
      sourceType: sourceType,
      // saveToPhotoAlbum: false,
      // correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 1000,
      targetHeight: 1000
    };

    this.camera.getPicture(options).then((imageData) => {
      this.banner_base64 = 'data:image/jpeg;base64,' + imageData;
      this.uploadBannerData();
    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }

  uploadBannerData() {
    let upload_data = {
      "user_id": this.user_data.id,
      "file_data": this.banner_base64,
      "file_type": "jpg"
    }

    let url = "https://intervii.com/backend/request.php";
    var body = { "update_user_banner": JSON.stringify(upload_data) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.user_data.banner = temp.data.banner;
          this.profile.banner = temp.data.banner;
          this.storage.set('user_data', this.user_data);
          this.global_service.user_data = temp.data;
          if(this.global_service.lang == 'en'){
            alert("Upload done");
          }else if(this.global_service.lang == 'cn'){
            alert("上载封面完成");
          }else{
            alert("上載封面完成");
          }
        }
      });
  }
  EnterContent() {
    //not ready

    this.global_service.editing_profile = true;
    let temp_data = {
     info:this.info,
    phone:this.phone,
    display_name: this.display_name,
    fb_name: this.fb_name,
    website: this.website
    }
    this.global_service.editing_profile_data = JSON.stringify(temp_data);

    this.global_service.editor_text = this.user_data.info;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/editor-page']);


    // let modal2 = this.modalCtrl.create(UserInfoInputPage,{user_data: this.user_data});
    // modal2.onDidDismiss(data => {
    //   //console.log(data);
    //   if(data!= undefined){
    //   }

    // })
    // modal2.present();
  }


  TextRemover(text) {
    return text.toLowerCase().replace(/[a-zA-Z0-9]+/g, "").replace(/-/g, "").replace(/_/g, "");
  }
  ValidatePassword(pw_string) {
    var regex_upper = new RegExp("[A-Z]+");
    var regex_lower = new RegExp("[a-z]+");
    var regex_numeric = new RegExp("[0-9]+");
    var regex_length = String(pw_string);
    var regex_space = /\s/;
    if (!(regex_upper.test(pw_string) || regex_lower.test(pw_string))) {
      if(this.global_service.lang == 'en'){
        alert("Password require alphabets");
      }else if(this.global_service.lang == 'cn'){
        alert("密码需要包含字母");
      }else{
        alert("密碼需要包含字母");
      }
      return false;
    }
    if (!regex_numeric.test(pw_string)) {
      if(this.global_service.lang == 'en'){
        alert("Password require number");
      }else if(this.global_service.lang == 'cn'){
        alert("密码需要包含数字");
      }else{
        alert("密碼需要包含數字");
      }
      return false;
    }
    if (regex_space.test(pw_string)) {
      if(this.global_service.lang == 'en'){
        alert("Password not accept blank key");
      }else if(this.global_service.lang == 'cn'){
        alert("密码不能包含空白键");
      }else{
        alert("密碼不能包含空白鍵");
      }
      return false;
    }
    if (regex_length.length < 8) {
      if(this.global_service.lang == 'en'){
        alert("Password not less than 8 alphabet");
      }else if(this.global_service.lang == 'cn'){
        alert("密码不能少於8个字");
      }else{
        alert("密碼不能少於8個字");
      }
      return false;
    }
    return true;
  }
}
