import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { HTTP } from "@ionic-native/http/ngx";
import { Storage } from "@ionic/storage";
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.page.html',
  styleUrls: ['./forget-password.page.scss'],
})
export class ForgetPasswordPage implements OnInit {
  email: string = "";
  pressed: boolean = false;
  constructor(
    public platform: Platform,
    private storage: Storage,
    public http: HTTP,
    public navCtrl: NavController,
    private alert: AlertController,
    public global_service: GlobalService
  ) { }

  ngOnInit() {
  }

  async Reset() {
    if (!this.pressed) {
      this.pressed = true;
      let url = "https://intervii.com/backend/request.php";
      let body = { forget_password: this.email };
      let headers = {
        "Content-Type": "application/x-www-form-urlencoded"
      };
      this.http.post(url, body, headers).then(async data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let msg_title = "請留意郵箱";
          if (this.global_service.lang == 'en') {
            msg_title = "Please check mailbox";
          }
          if (this.global_service.lang == 'cn') {
            msg_title = "请留意邮箱";
          }
          const alert_box = await this.alert.create({
            message: msg_title,
            backdropDismiss:true,
          });
          await alert_box.present();
          this.global_service.resetLoading();
          this.navCtrl.pop();
        } else {
          const alert_box = await this.alert.create({
            message: temp.data,
            backdropDismiss:true,
          });
          await alert_box.present();
        }
        this.pressed = false;
      });
    } else {
      let msg_title = "請勿重覆提交";
      if (this.global_service.lang == 'en') {
        msg_title = "Please don't apply more than once";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "请勿重覆提交";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:true,
      });
      await alert_box.present();
      this.global_service.resetLoading();
      this.navCtrl.pop();
    }
  }
}
