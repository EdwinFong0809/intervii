import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCourseContentPage } from './build-course-content.page';

describe('BuildCourseContentPage', () => {
  let component: BuildCourseContentPage;
  let fixture: ComponentFixture<BuildCourseContentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCourseContentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCourseContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
