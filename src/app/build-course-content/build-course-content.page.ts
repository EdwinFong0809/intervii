import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-build-course-content',
  templateUrl: './build-course-content.page.html',
  styleUrls: ['./build-course-content.page.scss'],
})
export class BuildCourseContentPage implements OnInit {
  enter_content: any = "";
  category: any = "";
  list_of_choice: any = ['運動','藝術','美容'];
  course_id: any;
  course: any = {
    tag: "",
    profile: ""
  };

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }


  ionViewDidEnter() {
    //console.log('ionViewDidLoad BuildyourcourseCreateContentPage');
    this.course = this.global_service.create_course;
    if(this.global_service.back_update){
      this.global_service.back_update = false;
      this.course.profile = this.global_service.editor_text;
    }
    this.LoadCategory();
    //console.log(JSON.stringify(this.course));
    this.course_id = this.course.id;//this.navParams.get('course_id');
  }
  async Dismiss() {
    if(this.course.tag == ""){
      let msg_title = "請選擇類別";
    let msg_option_1 = "確認";
    if (this.global_service.lang == 'en') {
      msg_title = "Please select category";
      msg_option_1 = "Confirm";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "请选择类别";
      msg_option_1 = "确认";
    }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }else {
      this.UploadData();
    }

  }
  UploadData(){
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id ,
      "tag" : this.course.tag,
      "profile" : this.course.profile
     };
    let body = { "update_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        let return_data = this.course.tag;
        this.global_service.create_course = this.course;
        this.global_service.dismiss();
        this.global_service.resetLoading();
        this.navCtrl.pop();
      });
  }
  GoBack(){
    // this.viewCtrl.dismiss();
        this.global_service.resetLoading();
        this.navCtrl.pop();
  }
  LoadCategory(){
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_all_active_category": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        if(temp.result=="success"){
          this.global_service.categories = temp.data;
        }
      });
  }
  EnterContent(){
    //console.log('EnterContent');
    this.global_service.editor_text = this.course.profile;
    this.navCtrl.navigateForward(['/editor-page']);
  }
}
