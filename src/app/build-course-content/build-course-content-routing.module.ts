import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseContentPage } from './build-course-content.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseContentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseContentPageRoutingModule {}
