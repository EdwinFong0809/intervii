import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, AlertController, NavParams, Platform } from '@ionic/angular';
import { EventsService } from '../events.service';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
// import { HomePage } from '../home/home';
// import { EditProfilePage } from '../edit-profile/edit-profile';
// import { SettingPage } from '../setting/setting';
// import { HomeLatestCoursePage } from '../home-latest-course/home-latest-course';
// import { VlogDetailPage } from '../vlog-detail/vlog-detail';
// import { PaymentRecordPage } from '../payment-record/payment-record';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from "../global.service";


@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {
  call_course_count: number = 0;
  latest_course: any;
  current_select: string;
  user_data: any;
  profile: any;
  current_id: any;
  vlogs: any;
  price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];
  load_info_done: boolean = false;
  missing_text:string = "";
  connecting_server: boolean = false;
  constructor(
    private sanitizer: DomSanitizer,
    public http: HTTP,
    private alert: AlertController,
    private storage: Storage,
    // public zone: NgZone, 
    public platform: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public events: EventsService,
  ) {
    // this.LoadData();
    this.current_select = 'latest_course';
  }

  ngOnInit() {

  }

  pressBack(){
    console.log("pressBack");
    if(this.global_service.from_follow){
      this.global_service.from_follow = false;
      this.events.publishSomeData({data:"refresh_follow"});
    }
  }

  ionViewWillEnter() {
    this.price_list = this.global_service.price_list;
    if(this.global_service.back_update){
      this.global_service.back_update = false;
      this.LoadData();
    }
    else{
      this.LoadData();
    }
  }

  GoBack() {
    this.global_service.vlog_data = null;
    // this.navCtrl.navigateBack('/tabs/tab4');
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  LoadData() {
    this.missing_text = "";
    this.vlogs = [];
    this.storage.get('user_data').then((val) => {
      if (val != undefined && val != null) {
        this.user_data = val;
        let url = "https://intervii.com/backend/request.php";
        var json = {
          "user_id": this.user_data.id
        };
        let body = { "get_user_by_id": JSON.stringify(json) };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            this.global_service.dismiss();
            let temp = JSON.parse(data.data);
            this.user_data = temp.data;
            console.log(this.user_data);


          });
      } else {
        this.user_data = 0;
      }
      //console.log("user_data");
      //console.log(this.user_data);
    });
    this.segmentChanged(null, 'latest_course');
    let id = this.global_service.view_user_id;
    this.current_id = id;
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "user_id": id
    };
    let body = { "get_user_by_id": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        //console.log("profile");
        //console.log(temp.data);
        if (temp.data.rank == undefined) {
          temp.data.rank = 0;
        }
        //console.log(temp.data.info);
        // this.zone.run(() => {
        temp.data.info = this.sanitizer.bypassSecurityTrustHtml(temp.data.info);
        this.profile = temp.data;
        console.log(this.profile);
        this.load_info_done = true;
        // });
      });

    this.latest_course = [];


  }

  async logout() {
    let msg_title = "你想登出嗎？";
    let msg_option_1 = "確定";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "You want to logout?";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "你想登出吗？";
      msg_option_1 = "确定";
      msg_option_2 = "取消";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      cssClass: 'custom-alertInfo',
      backdropDismiss:false,
      buttons: [{
        text: msg_option_1, handler: () => {
          this.global_service.user_id = 0;
          this.global_service.user_data = null;
          this.storage.clear();
          this.global_service.resetLoading();
          this.navCtrl.navigateRoot(['/tabs/tab3']);
        }
      },
      { text: msg_option_2 }]
    });
    await alert_box.present();
  }

  GoSetting() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/setting']);
    // this.app.getRootNav().push(SettingPage);
  }
  OpenCourse(course) {
    //console.log(course);
        this.global_service.resetLoading();
    this.global_service.course_data = course;
    this.navCtrl.navigateForward(['/course-detail']);
    // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
  }

  OpenVlog(vlog) {
    this.global_service.vlog_data = vlog;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/vlog-detail']);
  }

  GoEdit() {
    this.global_service.user_profile = this.profile;
    this.global_service.edit_profile_from = "";
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/edit-profile']);
  }


  segmentChanged($event, current_select?) {
    if (current_select == undefined){
      this.current_select = $event.detail.value;
    }
    else{
      this.current_select = current_select;
    }
    this.missing_text = "";

    let url = "https://intervii.com/backend/request.php";
    var json = {
      "user_id": this.current_id
    };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };

    this.connecting_server = true;
    if (this.current_select == "latest_course") {
      this.latest_course = [];

      let body = { "get_user_open_course": JSON.stringify(json) };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if((this.user_data != 0 && (temp.data[i].lecturer_id == this.user_data.id)) || (temp.data[i].status == 'completed' )){
                let temp_data = temp.data[i];//{"course_title":temp.data[i].title,"rank":0,"course_price":temp.data[i].price,"course_rate":"","course_id":temp.data[i].id,"total_duration":temp.data[i].total_duration,"has_live":temp.data[i].has_live,"has_video":temp.data[i].has_video,"lecturer_icon":temp.data[i].lecturer_icon,"course_img_src":"https://intervii.com/media/"+temp.data[i].icon};
                if (temp.data[i].rank != undefined) {
                  temp_data.rank = temp.data[i].rank;
                }
                this.latest_course.push(temp_data);
              }
            }
            this.call_course_count = this.latest_course.length;
            if(this.latest_course.length == 0){
              if(this.global_service.lang == 'en'){
                this.missing_text = "沒有課堂";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "No course";
              }else{
                this.missing_text = "没有课堂";
              }
            }
          }else{
            if(this.global_service.lang == 'en'){
              this.missing_text = "沒有課堂";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "No course";
            }else{
              this.missing_text = "没有课堂";
            }
          }
          this.connecting_server = false;
        });
    }

    if (this.current_select == "trial_course") {
      this.latest_course = [];

      let body = { "get_user_open_course": JSON.stringify(json) };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if (temp.data[i].has_preview) {
                let temp_data = temp.data[i];//{"course_title":temp.data[i].title,"rank":0,"course_price":temp.data[i].price,"course_rate":"","course_id":temp.data[i].id,"total_duration":temp.data[i].total_duration,"has_live":temp.data[i].has_live,"has_video":temp.data[i].has_video,"lecturer_icon":temp.data[i].lecturer_icon,"course_img_src":"https://intervii.com/media/"+temp.data[i].icon};
                if (temp.data[i].rank != undefined) {
                  temp_data.rank = temp.data[i].rank;
                }
                this.latest_course.push(temp_data);
              }
            }
            if(this.latest_course.length == 0){
              if(this.global_service.lang == 'en'){
                this.missing_text = "沒有試堂";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "No preview";
              }else{
                this.missing_text = "没有试堂";
              }
            }
          }else{
            if(this.global_service.lang == 'en'){
              this.missing_text = "沒有試堂";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "No preview";
            }else{
              this.missing_text = "没有试堂";
            }
          }
          this.connecting_server = false;
        });
    }

    if (this.current_select == "live") {
      this.latest_course = [];
      let body = { "get_my_live_course": JSON.stringify(json) };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          //console.log(data);
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if (temp.data[i].has_live) {
                let temp_data = temp.data[i];//{"course_title":temp.data[i].title,"rank":0,"course_price":temp.data[i].price,"course_rate":"","course_id":temp.data[i].id,"total_duration":temp.data[i].total_duration,"has_live":temp.data[i].has_live,"has_video":temp.data[i].has_video,"lecturer_icon":temp.data[i].lecturer_icon,"course_img_src":"https://intervii.com/media/"+temp.data[i].icon};
                if (temp.data[i].rank != undefined) {
                  temp_data.rank = temp.data[i].rank;
                }
                this.latest_course.push(temp_data);
              }
            }
            if(this.latest_course.length == 0){
              if(this.global_service.lang == 'en'){
                this.missing_text = "沒有直播";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "No live";
              }else{
                this.missing_text = "没有直播";
              }
            }
          }else{
            if(this.global_service.lang == 'en'){
              this.missing_text = "沒有直播";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "No live";
            }else{
              this.missing_text = "没有直播";
            }
          }
          this.connecting_server = false;
        });
    }

    if (this.current_select == "vlog") {
      this.latest_course = [];
      this.missing_text = "";
      let body2 = { "get_vlogs_by_id": JSON.stringify(json) };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body2, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.vlogs = temp.data;
            if(this.vlogs.length == 0){
              if(this.global_service.lang == 'en'){
                this.missing_text = "沒有VLOG";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "No VLOG";
              }else{
                this.missing_text = "没有VLOG";
              }
            }
          }else{
            if(this.global_service.lang == 'en'){
              this.missing_text = "沒有VLOG";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "No VLOG";
            }else{
              this.missing_text = "没有VLOG";
            }
          }
          this.connecting_server = false;
        });
    }
  }
  Follow() {
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "user_id": this.user_data.id,
      "target_id": this.profile.id
    };
    let body = { "follow_target": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.LoadData();
        }
      });
  }
  AddFavouriteCourse(id) {
    if (this.user_data != undefined && this.user_data != null && this.user_data.id != 0 && this.user_data.id != undefined) {

      let url = "https://intervii.com/backend/request.php";
      let body = { "follow_course": JSON.stringify({ "user_id": this.global_service.user_data.id, "course_id": id }) };
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.http.post(url, body, headers)
        .then(data => {
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.user_data.follow_course = temp.data;
            this.global_service.user_data = this.user_data;


            this.latest_course.forEach((course_data, index) => {
              if(course_data.id == id){
                if(this.user_data.follow_course.includes(id)){
                  if(!course_data.followers.includes(this.user_data.id)){
                    course_data.followers.push(this.user_data.id);
                  }
                }else{
                  if(course_data.followers.includes(this.user_data.id)){
                    let index = course_data.followers.indexOf(this.user_data.id, 0);
                    if (index > -1) {
                      course_data.followers.splice(index, 1);
                    }        
                  }
                }
              }
            });
          }
        });
    }
  }
  Unfollow() {
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "user_id": this.user_data.id,
      "target_id": this.profile.id
    };
    let body = { "unfollow_target": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.LoadData();
        }
      });
  }
  GoPayment() {
    // this.app.getRootNav().push(PaymentRecordPage);
  }
  CallEmail() {
    if (this.profile.email != undefined && this.profile.email != null && this.profile.email != '') {
      setTimeout(() => {
        window.open('mailto:' + this.profile.email, '_system');
      }, 500);
    }
  }
  CallPhone() {
    if (this.profile.phone != undefined && this.profile.phone != null && this.profile.phone != '') {
      setTimeout(() => {
        window.open('tel:' + this.profile.phone);
      }, 500);
    }
  }
  CallFacebook() {
    if (this.profile.fb_name != undefined && this.profile.fb_name != null && this.profile.fb_name != '') {
      setTimeout(() => {
        window.open('https://facebook.com/' + this.profile.fb_name);
      }, 500);
    }
  }
  CallWebsite() {
    if (this.profile.website != undefined && this.profile.website != null && this.profile.website != '') {
      setTimeout(() => {
        window.open('https://' + this.profile.website);
      }, 500);
    }
  }

  Econ(){
    this.navCtrl.navigateForward(['/econ-info']);
  }
}
