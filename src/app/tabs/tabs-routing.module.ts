import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'my-course',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../my-course/my-course.module').then(m => m.MyCoursePageModule)
          }
        ]
      },
      {
        path: 'follow-tutor',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../follow-tutor/follow-tutor.module').then(m => m.FollowTutorPageModule)
          }
        ]
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'vlog',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../vlog/vlog.module').then(m => m.VlogPageModule)
          }
        ]
      },
      {
        path: 'build-course',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../build-course/build-course.module').then(m => m.BuildCoursePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
