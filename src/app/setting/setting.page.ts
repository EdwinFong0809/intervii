import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, AlertController, NavParams } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
  lang:string;
  zone:string;
  notification:boolean = false;
  notification_id:string = "";
  user_data:any;
  logined: boolean = false;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    private storage: Storage,
    // public zone: NgZone, 
    public navCtrl: NavController,
    public global_service: GlobalService,
    ) { }

  ngOnInit() {
    this.user_data = {"id":-1,"notification_on":false};
      // this.storage.get('user_data').then((val) => {
      //   if (val != undefined && val != null) {
          this.user_data = this.global_service.user_data;
          if(this.user_data != undefined && this.user_data != null){
          let url = "https://intervii.com/backend/request.php";
          var json = {
            "user_id": this.user_data.id
          };
          let body = { "get_user_by_id": JSON.stringify(json) };
          let headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
          };
          this.global_service.isLoading = true;
          this.global_service.presentLoading();
          this.http.post(url, body, headers)
            .then(data => {
              this.global_service.dismiss();
              let temp = JSON.parse(data.data);
                this.user_data = temp.data;
            });
          }
      //   }else {
      //     this.user_data = 0;
      //   }
      // });
      this.storage.get('lang').then((val) => {
        if (val != undefined && val != null) {
          this.lang = val;
        }else {
          this.lang = "zh";
        }
      });

      // this.storage.get('zone').then((val) => {
      //   if (val != undefined && val != null) {
      //     this.zone = val;
      //   }else {
      //     this.zone = "hk";
      //   }
      // });

      this.storage.get('notification').then((val) => {
        if (val != undefined && val != null) {
          this.notification = val;
        }else {
          this.notification = false;
        }
      });
        this.storage.get('notification_user_id').then((val) => {
          if (val != undefined && val != null) {
            this.notification_id = val;
          }else {
            this.notification_id = "";
          }
        });
  }

  GoBack() {
    this.global_service.vlog_data = null;
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  ionViewDidEnter(){
    this.storage.get('notification_user_id').then((val) => {
      if (val != undefined && val != null) {
        this.notification_id = val;
      }else {
        this.notification_id = "";
      }
    });
    this.user_data = {"id":-1,"notification_on":false};
      // this.storage.get('user_data').then((val) => {
      //   if (val != undefined && val != null) {
          this.user_data = this.global_service.user_data;
          if(this.user_data != undefined && this.user_data != null){
            let url = "https://intervii.com/backend/request.php";
            var json = {
              "user_id": this.user_data.id
            };
            let body = { "get_user_by_id": JSON.stringify(json) };
            let headers = {
              'Content-Type': 'application/x-www-form-urlencoded'
            };
            this.global_service.isLoading = true;
            this.global_service.presentLoading();
            this.http.post(url, body, headers)
              .then(data => {
                this.global_service.dismiss();
                let temp = JSON.parse(data.data);
                  this.user_data = temp.data;
              });
          }
      //   }else {
      //     this.user_data = 0;
      //   }
      // });
      this.storage.get('lang').then((val) => {
        if (val != undefined && val != null) {
          this.lang = val;
          this.global_service.lang = val;
        }else {
          this.lang = "zh";
          this.global_service.lang = "zh";
        }
      });

      // this.storage.get('zone').then((val) => {
      //   if (val != undefined && val != null) {
      //     this.zone = val;
      //   }else {
      //     this.zone = "hk";
      //   }
      // });

      this.storage.get('notification').then((val) => {
        if (val != undefined && val != null) {
          this.notification = val;
        }else {
          this.notification = false;
        }
      });
  }
  updateLang(){
    this.storage.set('lang', this.lang);
    this.global_service.lang = this.lang;
  }
  updateZone(){
    //console.log("updateZone");
    // this.storage.set('zone', this.zone);
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.user_data.id,
      "region": this.user_data.region,
    };
    let body = { "update_user": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(async data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.user_data = temp.data;
          // this.region = temp.data.region;
          this.storage.set('user_data', temp.data);
          this.global_service.user_data = temp.data;
        }
      });
  }

  updateNotification(){
    console.log("updateNotification");
      console.log(this.user_data);
    this.storage.set('user_data', this.user_data);
        let url = "https://intervii.com/backend/request.php";
        var json = {
          "id": this.user_data.id,
          "notification_on":  ""
        };
        if(this.user_data.notification_on){
          json.notification_on = "true";
        }else{
          json.notification_on = "false";
        }
        let body = { "update_user": JSON.stringify(json) };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
          //console.log(JSON.stringify(json));
          this.global_service.isLoading = true;
          this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            if(temp.result == "success"){
              this.user_data = temp.data;
              this.global_service.user_data = temp.data;
              this.storage.set('user_data',temp.data);
            }
          });
  }

  async logout(){
    let msg_title = "你想登出嗎？";
    let msg_option_1 = "確認";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "Do you really want to logout？";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "你想登出吗？";
      msg_option_1 = "确认";
      msg_option_2 = "取消";
    }

      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertInfo',
        backdropDismiss:false,
        buttons: [{text:msg_option_1,handler:()=>{

            let url = "https://intervii.com/backend/request.php";
            var json = {};
            if(this.user_data.fb_id != undefined){
              json = { "fb_id": this.user_data.fb_id, "notification_id":this.notification_id };
            }else{
              json = { "email": this.user_data.email, "notification_id":this.notification_id };
            }
            let body = { "logout": JSON.stringify(json) };
            //let body = { "current_datetime_string" : ""};
            // alert(JSON.stringify(json));
            let headers = {
              'Content-Type': 'application/x-www-form-urlencoded'
            };
            this.global_service.isLoading = true;
            this.global_service.presentLoading();
            this.http.post(url, body, headers)
              .then(data => {
                this.global_service.dismiss();
                this.global_service.user_id = 0;
                this.global_service.user_data = null;
                this.storage.clear();
                this.storage.set('notification_user_id', this.notification_id);
                this.global_service.resetLoading();
                this.navCtrl.navigateRoot(['/tabs/home']);
              }, error => {
                //console.log(error);
              });
        }},
        {text: msg_option_2}]
      });
      await alert_box.present();

  }

  contact(){
    window.open("https://intervii.com/contact_us.html", "_blank");
  }

  privacy(){
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/privacy']);
  }
}
