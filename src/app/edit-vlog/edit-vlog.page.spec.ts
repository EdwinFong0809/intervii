import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditVlogPage } from './edit-vlog.page';

describe('EditVlogPage', () => {
  let component: EditVlogPage;
  let fixture: ComponentFixture<EditVlogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVlogPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditVlogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
