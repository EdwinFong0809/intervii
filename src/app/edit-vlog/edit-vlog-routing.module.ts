import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditVlogPage } from './edit-vlog.page';

const routes: Routes = [
  {
    path: '',
    component: EditVlogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditVlogPageRoutingModule {}
