import { Component, OnInit, NgZone } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { GlobalService } from "../global.service";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { EventsService } from '../events.service';

@Component({
  selector: 'app-edit-vlog',
  templateUrl: './edit-vlog.page.html',
  styleUrls: ['./edit-vlog.page.scss'],
})
export class EditVlogPage implements OnInit {
  vlog: any;
  logined: boolean = false;

  list_of_choice: any = ['運動', '藝術', '美容'];
  user_id: any;
  loading: any;
  uploading: boolean = false;
  vlog_data: any = { "user_id": 0, "title": "", "content": "", "splash_data": "", "file_name": "", "file_type": "", "category": "" };
  base64_data: any;
  button_text: string = "下一步";
  preview_splash: string = "";
  preview_video_name: string = "";

  constructor(
    public http: HTTP,
    private alert: AlertController,
    private streamingMedia: StreamingMedia,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private camera: Camera,
    private file: File,
    private filePath: FilePath,
    private transfer: FileTransfer,
    private zone: NgZone,
    public actionSheetCtrl: ActionSheetController,
    public events: EventsService,
  ) {
    if (this.global_service.user_data != undefined && this.global_service.user_data != null) {
      this.logined = true;
    }
    this.list_of_choice = ['運動', '藝術', '美容'];
    if (this.global_service.vlog_data != undefined && this.global_service.vlog_data != null) {
      this.vlog_data = this.global_service.vlog_data;
    }else{
      this.vlog_data = { "user_id": 0, "title": "", "content": "", "splash_data": "", "file_name": "", "file_type": "", "file_vimeo": "", "category": "" };
    }
    this.base64_data = "";
    this.preview_splash = "https://intervii.com/media/"+this.vlog_data.splash;
    this.preview_video_name = this.vlog_data.video;
    let msg_option_1 = "上載中";
    if (this.global_service.lang == 'en') {
      msg_option_1 = "Uploading";
    }
    if (this.global_service.lang == 'cn') {
      msg_option_1 = "上载中";
    }
    this.loading = this.loadingCtrl.create({
      message: msg_option_1+'...',
      duration: 2000
    });
    this.vlog_data.user_id = this.global_service.user_id;
    if(this.global_service.lang == 'en'){
      this.button_text = "Next";
    }else if(this.global_service.lang == 'cn'){
      this.button_text = "下一步";
    }else{
      this.button_text = "下一步";
    }
  }
  Pop() {
    this.global_service.add_vlog = null;
    this.global_service.editor_title = "";
    this.global_service.editor_text = "";
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    //console.log("ionViewDidEnter");
    if (this.global_service.back_update) {
      this.global_service.back_update = false;
      if (this.global_service.add_vlog != null) {
        this.vlog_data = this.global_service.vlog_data;
      }
      this.vlog_data.content = this.global_service.editor_text;
    }
    this.LoadCategory();
  }

  EnterContent() {
    this.global_service.add_vlog = this.vlog_data;
    this.global_service.editor_title = this.vlog_data.title;
    this.global_service.editor_text = this.vlog_data.content;
    this.global_service.back_url = "/add-vlog";
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/editor-page']);
  }

  async UploadData() {
    //console.log(this.vlog_data);
    if (this.vlog_data.title == "" || this.vlog_data.content == "" || this.vlog_data.splash_data == "" || this.vlog_data.file_name == "" || this.vlog_data.category == "") {

    let msg_title = "請輸入所有內容";
    let msg_option_1 = "確定";
    if(this.global_service.lang == 'en'){
      msg_title = "Please don't leave blank";
      msg_option_1 = "Confirm";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "请输入所有内容";
      msg_option_1 = "确定";
    }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [{
          text: msg_option_1,
          handler: () => {
          }
        }]
      });
      await alert_box.present();
    } else {
      // this.loading.present();

      let url = "https://intervii.com/backend/request.php";
      let body;
      console.log(this.vlog_data);
      body = { "edit_vlog": JSON.stringify(this.vlog_data) };
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.http.post(url, body, headers)
        .then(async data => {
          let temp = JSON.parse(data.data);
          //console.log(data.data);
          // this.loading.dismiss();
          if (temp.result == "success") {
            this.global_service.back_update = true;
            this.events.publishSomeData({data:"update_vlog"});
            this.global_service.resetLoading();
            this.navCtrl.pop();
          } else {
            let msg_title = "提交失敗";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Fail";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "提交失败";
              msg_option_1 = "确定";
            }
            const alert_box = await this.alert.create({
              message: msg_title,
              cssClass: 'custom-alertControl',
              backdropDismiss:false,
              buttons: [{
                text: msg_option_1,
                handler: () => {
                }
              }]
            });
            await alert_box.present();
          }
        });
    }
  }


  uploadPhoto() {
    this.presentActionSheet();
  }
  async presentActionSheet() {
    let msg_option_1 = "從手機相簿選擇";
    let msg_option_2 = "拍攝新照片";
    let msg_option_3 = "取消";
    if (this.global_service.lang == 'en') {
      msg_option_1 = "Camera roll";
      msg_option_2 = "Take photo";
      msg_option_3 = "Cancel";
    }
    if (this.global_service.lang == 'cn') {
      msg_option_1 = "从手机相簿选择";
      msg_option_2 = "拍摄新照片";
      msg_option_3 = "取消";
    }
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image Source',
      buttons: [
        {
          text: msg_option_1,
          handler: () => {
            //console.log('Load from Library');
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: msg_option_2,
          handler: () => {
            //console.log('Load Camera');
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: msg_option_3,
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }
  LoadCategory(){
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_all_active_category": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        if(temp.result=="success"){
          this.global_service.categories = temp.data;
        }
      });
  }

  async takePicture(sourceType) {
    var options = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 1000,
      targetHeight: 1000
    };

    this.camera.getPicture(options).then((imageData) => {
      this.base64_data = 'data:image/jpeg;base64,' + imageData;
      this.vlog_data.splash_data = this.base64_data;
      this.preview_splash = this.base64_data;
    }, async (err) => {
      let msg_title = "發生錯誤";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Error";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "发生错误";
        msg_option_1 = "确定";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    });
  }

  CallLoadVideo() {
    this.CallLoadVideoDo();
  }
  CallLoadVideoDo() {
    var options = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      // saveToPhotoAlbum: false,
      // correctOrientation: true,
      // destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      let file_data = data;
      if (this.plt.is('android')) {

        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if (file_type == 'MOV') {
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.vlog_data.file_name = file_name;
            this.vlog_data.file_type = file_type;
            this.preview_video_name = file_name;

            const fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            // this.loading.present();
            fileTransfer.onProgress((progressEvent) => {
              // this.zone.run(() => {
              if (progressEvent.lengthComputable) {
                // //console.log(progressEvent);
                this.zone.run(() => {
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                });
              } else {
                // this.loading.dismiss();
              }
              // });
            });
            // fileTransfer.upload(file_data, 'https://intervii.com/upload.php', options)
            fileTransfer.upload(file_data, 'https://intervii.com/upload_vimeo.php', options)
              .then((data) => {
                //console.log(data);
                // success
                // this.loading.dismiss();
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                    let url = "https://intervii.com/backend/request.php";
                    let body;
                    //console.log(this.vlog_data);
                    body = { "vimeo_transfer": JSON.stringify({"file_name":this.vlog_data.file_name}) };
                    let headers = {
                      'Content-Type': 'application/x-www-form-urlencoded'
                    };
                    this.http.post(url, body, headers)
                      .then(async data => {
                        let temp = JSON.parse(data.data);
                        console.log("------vimeo");
                        console.log(data.data);
                        // this.loading.dismiss();
                        if (temp.result == "success") {
                          this.vlog_data.file_vimeo = temp.data;
                          let msg_title = "上傳成功";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload success";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载成功";
                          }
                          alert(msg_title);
                        } else {
                          let msg_title = "上傳失敗";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload fail";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载失败";
                          }
                          alert(msg_title);
                        }
                      });
                  // this.vlog_data.file_vimeo = data.response;
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                // alert(JSON.stringify(data));
                this.global_service.dismiss();
              }, (err) => {
                //console.log(err);
                // error
                // this.loading.dismiss();
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                // alert(JSON.stringify(err));
                this.global_service.dismiss();
              });
          });
      } else {
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            // file_data = file_data.replace('file://',"");
            //console.log(file_data);
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if (file_type == 'MOV') {
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.vlog_data.file_name = file_name;
            this.vlog_data.file_type = file_type;
            this.preview_video_name = file_name;

            const fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            // this.loading.present();
            fileTransfer.onProgress((progressEvent) => {
              // this.zone.run(() => {
              if (progressEvent.lengthComputable) {
                // //console.log(progressEvent);
                this.zone.run(() => {
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                });
              } else {
                // this.loading.dismiss();
              }
              // });
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload_vimeo.php', options)
              .then((data) => {
                // success
                // this.loading.dismiss();
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  console.log("upload_vimeo");
                    let url = "https://intervii.com/backend/request.php";
                    let body;
                    //console.log(this.vlog_data);
                    body = { "vimeo_transfer": JSON.stringify({"file_name":this.vlog_data.file_name}) };
                    let headers = {
                      'Content-Type': 'application/x-www-form-urlencoded'
                    };
                    this.http.post(url, body, headers)
                      .then(async data => {
                        let temp = JSON.parse(data.data);
                        console.log("------vimeo");
                        console.log(data.data);
                        // this.loading.dismiss();
                        if (temp.result == "success") {
                          this.vlog_data.file_vimeo = temp.data;
                          let msg_title = "上傳成功";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload success";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载成功";
                          }
                          alert(msg_title);
                        } else {
                          let msg_title = "上傳失敗";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload fail";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载失败";
                          }
                          alert(msg_title);
                        }
                      });
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                this.global_service.dismiss();
              }, (err) => {
                // error
                // this.loading.dismiss();
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                this.global_service.dismiss();
              });
          });
      }
    }, (err) => {
    });
  }

}
