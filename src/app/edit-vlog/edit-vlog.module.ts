import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditVlogPageRoutingModule } from './edit-vlog-routing.module';

import { EditVlogPage } from './edit-vlog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditVlogPageRoutingModule
  ],
  declarations: [EditVlogPage]
})
export class EditVlogPageModule {}
