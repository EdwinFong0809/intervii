import { Component, OnInit, ViewChild, ViewChildren, QueryList, ElementRef, Renderer2 } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton, IonInfiniteScroll } from '@ionic/angular';
import { DomSanitizer} from '@angular/platform-browser';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { Socket } from 'ngx-socket-io';
import { element } from 'protractor';
import { count } from 'console';
import {NgZone} from '@angular/core';
import {Observable, OperatorFunction} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('top_tabs', { "read": IonTabs, "static": true }) tabRef: IonTabs;
  // @ViewChild('first_tab', { "read": HTMLButtonElement, "static": false }) tab1: any;
  // @ViewChild('second_tab', { "read": HTMLButtonElement, "static": false }) tab2: any;
  // @ViewChild('third_tab', { "read": HTMLButtonElement, "static": false }) tab3: any;
  // @ViewChild('forth_tab', { "read": HTMLButtonElement, "static": false }) tab4: any;
  is_logged_in: boolean = false;
  can_refresh:boolean = true;

  // tab1Root = AboutPage;
  // tab2Root = AboutPage;
  // tab3Root = AboutPage;
  // tab4Root = AboutPage;
  current_page: any = 0;
  categories_data: any = {
    category: ""
  };
  price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];
  user_id: any;
  temp_event: any;
  current_course: any;
  current_course_tag: string = "最新";
  latest_course: any;
  trial_course: any;
  live_course: any;
  filter_course: any;
  user_data: any = { follow_course: [] };
  direction: string = "";
  logined: boolean = false;
  searched: boolean = false;
  current_scroll: number = 0;
  message_unread: boolean = false;
  data_package: any;
  image_heart: any = [];
  has_intro: boolean = false;
  has_profile: boolean = false;
  missing_text: string = "";
  alert_count:number = 0;

  just_reload: boolean = false;
  init: boolean = false;
  isIOS: boolean = false;
  unread_count:number = 0;

  data_ready: any = { type: "全部", course_categories: "所有類別", sort: "由最新" };
  keyword: string = "";
  callback: any;
  categories: any = "全部";
  list_of_choice: any = ['全部', '運動', '藝術', '美容'];
  parent_page: any;

  //new
  current_tab: string;

  all_video_data = [];
  currentPlaying = null;
  stickyVideo: HTMLVideoElement = null;
  stickyPlaying = false;
  loaded_video_id_list = [];

  show_preview_count = 3;

  connecting_server: boolean = false;
  refresher:any = null;

  @ViewChild('stickyplayer', { static: false }) stickyPlayer: ElementRef;
  @ViewChildren('player') videoPlayers: QueryList<any>;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(
    private http: HTTP,
    // public events: Events,
    // public app: App,
    public platform: Platform,
    private alert: AlertController,
    public sanitizer: DomSanitizer,
    private storage: Storage,
    // public modalCtrl: ModalController,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private socket: Socket,
    private renderer: Renderer2,
    private zone: NgZone
    // public navParams: NavParams
  ) {

    this.storage.get('user_data').then((val) => {
      if (val != undefined && val != null) {
        this.global_service.user_data = val;
        this.user_data = val;
        this.global_service.socket_page = "home";
        this.ConnectToNJS(this.global_service.user_data.id);
        this.platform.resume.subscribe(async () => {
          this.global_service.home_socket == false;
          this.ConnectToNJS(this.global_service.user_data.id);
        });
        this.LoadUserCourseData();
      } else {
        this.global_service.user_data = null;
      }
      // //console.log("==================init user_data " + this.global_service.user_data);
    });
    this.storage.get('lang').then((val) => {
      if (val != undefined && val != null) {
        this.global_service.lang = val;
      }else {
        this.global_service.lang = "zh";
      }
    });

  }

  ngOnInit() {
    this.LoadPrice();
    this.current_tab = 'first_tab';
    this.IsLogin();
    this.loadIAP();
    this.latest_course = [];
    this.current_course = [];
    this.missing_text = "";
    this.trial_course = [];
    this.live_course = [];
    //console.log("==================init 1");
    // this.LoadInit();
    // this.LoadLatest();
    if (this.platform.is('ios')) {
      this.isIOS = true;
    }
  }

  ionViewDidEnter() {
    this.price_list = this.global_service.price_list;
    this.LoadPrice();
    this.loadIAP();
    this.LoadCategory();
    this.loaded_video_id_list = [];

    //console.log("==================init 3");
    this.LoadInit();
    // this.storage.set('course_filter', { type: "全部", course_categories: "所有類別", sort: "由最新" });
    if (!this.init) {
      this.init = true;
      this.LoadLatest();
    }else{
      // if(this.current_course.length==0){
      //   alert("data empty");
      // }
    switch (this.current_tab) {
      case "first_tab":
          this.current_course_tag = "最新";
          this.latest_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadLatest();
        break;
      case "second_tab":
          this.current_course_tag = "試堂";
          this.missing_text = "";
          this.all_video_data = [];
          this.loadAllPreview();
        // }
        break;
      case "third_tab":
          this.current_course_tag = "直播";
          this.latest_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadLive();
        break;
      case "forth_tab":
        this.current_course_tag = "分類";
        this.filter_course = [];
        this.missing_text = "";
        this.current_course = [];
        this.searched = false;
        break;
    }
    }
    // this.loadAllPreview();
  }

  doRefresh(refresher){
    console.log("refresh");
    this.refresher = refresher;
    this.just_reload = true;
    this.can_refresh = false;
    setTimeout(function(){
      this.can_refresh = true;
    }, 10000);
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        // if (this.current_course_tag != "最新") {
          this.current_course_tag = "最新";
          this.latest_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadLatest();
        // }
        break;
      case "second_tab":
        // //console.log(this.tab2);
        // if (this.current_course_tag != "試堂") {
          this.current_course_tag = "試堂";
          // this.latest_course = [];
          this.missing_text = "";
          // this.current_course = [];
          // this.LoadTrial();
          this.all_video_data = [];
          this.loadAllPreview();
        // }
        break;
      case "third_tab":
        // //console.log(this.tab3);
        // if (this.current_course_tag != "直播") {
          this.current_course_tag = "直播";
          this.latest_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadLive();
        // }
        break;
      case "forth_tab":
        // //console.log(this.tab4);
        this.current_course_tag = "分類";
        this.filter_course = [];
        this.missing_text = "";
        this.current_course = [];
        this.searched = false;
        // this.InitFilter();
        break;
    }
    setTimeout(() => {
      refresher.target.complete();
    }, 2000);
  }

  LoadCategory(){
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_all_active_category": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        console.log("Category");
        console.log(data);
        let temp = JSON.parse(data.data);
        if(temp.result=="success"){
          this.global_service.categories = temp.data;
        }
      });
  }

  LoadPrice(){
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_price_list": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.price_list = JSON.parse(data.data);
        this.price_list = this.global_service.price_list;
      });
  }

  loadIAP(){
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_iap_debug": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.iap_is_debug = JSON.parse(data.data);
      });
  }

  loadAllPreview() {
    this.connecting_server = true;

    this.zone.run(
      () => { this.all_video_data = [];}
    )
    this.show_preview_count = 0;
    let url = "https://intervii.com/backend/request.php";
    // let body = { "get_course_by_case": JSON.stringify(val) };
    let body = { "get_preview_videos": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.connecting_server = false;
        // //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let temp_all_videos = temp.data;
          for(let i=0; i<temp_all_videos.length; i++){
            temp_all_videos[i].preview_file_name = temp_all_videos[i].preview_file_name.replace("\n", "");
            if(temp_all_videos[i].preview_file_vimeo != undefined && temp_all_videos[i].preview_file_vimeo!= null && temp_all_videos[i].preview_file_vimeo!=''){
              temp_all_videos[i].vimeo_safe_link = this.VimeoURL('https://player.vimeo.com/video/'+temp_all_videos[i].preview_file_vimeo);
            }
            // temp_all_videos[i]. = temp_all_videos[i].preview_file_name.replace("\n", "");
          }
          this.zone.run(
            () => {
              this.all_video_data = temp_all_videos.filter(d => (d.preview_file_name != null && d.preview_file_name != '' && (d.preview_file_name.includes('.mp4') || d.preview_file_name.includes('.mov') || d.preview_file_name.includes('.MOV'))) ||  (d.preview_file_vimeo != undefined && d.preview_file_vimeo != null && d.preview_file_vimeo != ''));
              this.show_preview_count = 3;
              setTimeout(function(){
                this.didScroll();
              }, 500);
            }
          )
          //console.log(temp.data);
        } else {
          if(this.global_service.lang == 'en'){
            this.missing_text = "No Preview";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text = "没有Preview";
          }else{
            this.missing_text = "沒有Preview";
          }
        }

      });
  }

  loadData(event) {
    setTimeout(() => {
      this.show_preview_count += 3;
      event.target.complete();
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.show_preview_count >= this.all_video_data.length) {
        event.target.disabled = true;
      }
    }, 500);
  }

  videoLoaded(video_id) {
    this.loaded_video_id_list.push(video_id);
  }

  openFullscreen(elem) {
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    }
    else if (elem.webkitEnterFullscreen) {
      elem.webkitEnterFullscreen();
      elem.enterFullscreen();
    }
  }

  playOnSide(elem) {
    if (this.stickyVideo) {
      this.renderer.removeChild(this.stickyPlayer.nativeElement, this.stickyVideo);
    }

    this.stickyVideo = elem.cloneNode(true);

    this.renderer.appendChild(this.stickyPlayer.nativeElement, this.stickyVideo);

    if (this.currentPlaying) {
      const playPosition = this.currentPlaying.currentTime;
      this.currentPlaying.pause();
      this.currentPlaying = null;
      this.stickyVideo.currentTime = playPosition;
    }

    this.stickyVideo.muted = false;
    this.stickyVideo.play();
    this.stickyPlaying = true;
  }

  closeSticky() {
    if (this.stickyVideo) {
      this.renderer.removeChild(this.stickyPlayer.nativeElement, this.stickyVideo);
      this.stickyVideo = null;
      this.stickyPlaying = false;
    }
  }

  playOrPauseSticky() {
    if (this.stickyPlaying) {
      this.stickyVideo.pause();
      this.stickyPlaying = false;
    } else {
      this.stickyVideo.play();
      this.stickyPlaying = true;
    }
  }

  didScroll() {
    // if (this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
    //   return;
    // } else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
    //   this.currentPlaying.pause();
    //   this.currentPlaying = null;
    // }
    console.log("this.videoPlayers.length");
    console.log(this.videoPlayers.length);
    this.videoPlayers.forEach(player => {
      // // //console.log(player);
      // if(this.just_reload){
      //   this.just_reload = false;
      // }else{
      //   if (this.currentPlaying) {
      //     return;
      //   }
      // }

      // const nativeElement = player.nativeElement;
      // const inView = this.isElementInViewport(nativeElement);

      // if (this.stickyVideo && this.stickyVideo.src == nativeElement.src) {
      //   return;
      // }

      // if (inView) {
      //   this.currentPlaying = nativeElement;
      //   this.currentPlaying.muted = true;
      //   this.currentPlaying.play();
      // }
      // const nativeElement = player.nativeElement;
      if(this.isElementInViewport(player.nativeElement)){
        if(player.nativeElement.paused){
          player.nativeElement.muted = true;
          player.nativeElement.play();
        }
      }else{
        // player.nativeElement.stop();
      }

    });
  }

  isElementInViewport(el) {
    const rect = el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  segmentChanged($event) {
    //console.log($event.detail.value);
    this.current_tab = $event.detail.value;
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        if (this.current_course_tag != "最新") {
          this.current_course_tag = "最新";
          this.latest_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadLatest();
        }
        break;
      case "second_tab":
        // //console.log(this.tab2);
        if (this.current_course_tag != "試堂") {
          this.current_course_tag = "試堂";
          // this.latest_course = [];
          this.missing_text = "";
          // this.current_course = [];
          // this.LoadTrial();
          this.all_video_data = [];
          this.loadAllPreview();
        }
        break;
      case "third_tab":
        // //console.log(this.tab3);
        if (this.current_course_tag != "直播") {
          this.current_course_tag = "直播";
          this.latest_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadLive();
        }
        break;
      case "forth_tab":
        // //console.log(this.tab4);
        this.current_course_tag = "分類";
        this.filter_course = [];
        this.missing_text = "";
        this.current_course = [];
        this.searched = false;
        // this.InitFilter();
        break;
    }
    // if ($event.tabTitle == undefined) {
    //   // let CallbackFunction = (paramBoo, data) => {
    //   //   //console.log("Callback");
    //   //   this.LoadTab();
    //   // }
    //   // this.tabRef.select(this.current_page);
    //   // this.app.getRootNav().push(HomeCategoriesPage, { category: this.categories_data.category, callback: CallbackFunction, parent_page: this });
    // } else {
    //   this.current_page = parseInt($event.id.split("t1-")[1]);
    //   // this.tabRef.select(this.current_page);
    //   this.temp_event = $event;
    //   if (this.current_course_tag != $event.tabTitle) {
    //     this.current_course_tag = $event.tabTitle;
    //     this.latest_course = [];
    //     this.current_course = [];
    //     if (this.current_course_tag == "最新") {
    //       // this.LoadLatest();
    //     } else if (this.current_course_tag == "試堂") {
    //       // this.LoadTrial();
    //     } else if (this.current_course_tag == "直播") {
    //       // this.LoadLive();
    //     }
    //   }
    // }
    // //console.log(this.current_page);
  }

  searchClick(){
    if(this.current_course_tag == "分類"){
      this.current_course_tag = "分類";
      this.filter_course = [];
      this.missing_text = "";
      this.current_course = [];
      this.searched = false;
    }
  }

  LoadLatest() {
    this.latest_course = [];
    this.current_course = [];
    this.filter_course = [];
    this.trial_course = [];
    this.live_course = [];

    this.connecting_server = true;
    // this.storage.get('course_filter').then((val) => {
    //   //console.log(val);
    //   if (val != undefined && val != null) {
    let url = "https://intervii.com/backend/request.php";
    // let body = { "get_course_by_case": JSON.stringify(val) };
    let body = { "get_course_by_case": JSON.stringify({ type: "全部", course_categories: "所有類別", sort: "由最新" }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        // //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          //console.log(temp.data);
          for (let i = 0; i < temp.data.length; i++) {
            if (temp.data[i].rank == undefined) {
              temp.data[i].rank = 0;
            }
          }
          this.latest_course = temp.data;
          this.current_course = temp.data;
          // //console.log(this.current_course);
          if (this.current_course.length == 0) {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No Latest Course";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有最新课堂";
            }else{
              this.missing_text = "沒有最新課堂";
            }
          }
        } else {
          if(this.global_service.lang == 'en'){
            this.missing_text = "No Latest Course";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text = "没有最新课堂";
          }else{
            this.missing_text = "沒有最新課堂";
          }
        }
        this.connecting_server = false;
        this.refresher.target.complete();
      });
    //   }
    // });
  }

  LoadTrial() {
    this.connecting_server = true;
    this.latest_course = [];
    this.current_course = [];
    this.filter_course = [];
    this.trial_course = [];
    this.live_course = [];
    // this.storage.get('course_filter').then((val) => {
    //   //console.log(val);
    //   if (val != undefined && val != null) {
    let url = "https://intervii.com/backend/request.php";
    // let body = { "get_trial_by_case": JSON.stringify(val) };{ type: "全部", course_categories: "所有類別", sort: "由最新" }
    let body = { "get_trial_by_case": JSON.stringify({ type: "全部", course_categories: "所有類別", sort: "由最新" }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          for (let i = 0; i < temp.data.length; i++) {
            if (temp.data[i].rank == undefined) {
              temp.data[i].rank = 0;
            }
          }
          this.trial_course = temp.data;
          this.current_course = temp.data;
          if (this.current_course.length == 0) {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No Preview";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有试堂";
            }else{
              this.missing_text = "沒有試堂";
            }
          }
        } else {
          if(this.global_service.lang == 'en'){
            this.missing_text = "No Preview";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text = "没有试堂";
          }else{
            this.missing_text = "沒有試堂";
          }
        }
        this.connecting_server = false;
        this.refresher.target.complete();
      });
    //   }
    // });
  }

  LoadLive() {
    this.connecting_server = true;
    this.latest_course = [];
    this.current_course = [];
    this.filter_course = [];
    this.trial_course = [];
    this.live_course = [];
    // this.storage.get('course_filter').then((val) => {
    //   //console.log(val);
    //   if (val != undefined && val != null) {
    //     let url = "https://intervii.com/backend/request.php";
    //     let body = { "get_trial_by_case": JSON.stringify(val) };
    //     //let body = { "current_datetime_string" : ""};
    //     let headers = {
    //       'Content-Type': 'application/x-www-form-urlencoded'
    //     };
    //     this.http.post(url, body, headers)
    //       .then(data => {
    //         //console.log(data.data);
    //         let temp = JSON.parse(data.data);
    //         if (temp.result == "success") {
    //           for (let i = 0; i < temp.data.length; i++) {
    //             if (temp.data[i].rank == undefined) {
    //               temp.data[i].rank = 0;
    //             }
    //           }
    //           this.trial_course = temp.data;
    //           this.current_course = temp.data;
    //         }
    //       });
    //   }
    // });
    let url = "https://intervii.com/backend/request.php";
    // let body = { "get_trial_by_case": JSON.stringify(val) };{ type: "全部", course_categories: "所有類別", sort: "由最新" }
    let body = { "get_live_course": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        // //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          for (let i = 0; i < temp.data.length; i++) {
            if (temp.data[i].rank == undefined) {
              temp.data[i].rank = 0;
            }
          }
          this.live_course = temp.data;
          this.current_course = temp.data;
          if (this.current_course.length == 0) {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No Live";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有直播";
            }else{
              this.missing_text = "沒有直播";
            }
          }
        } else {
          if(this.global_service.lang == 'en'){
            this.missing_text = "No Live";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text = "没有直播";
          }else{
            this.missing_text = "沒有直播";
          }
        }
        this.connecting_server = false;
        this.refresher.target.complete();
      });
  }

  InitFilter() {
    this.connecting_server = true;
    this.searched = true;
    this.latest_course = [];
    this.current_course = [];
    this.filter_course = [];
    this.trial_course = [];
    this.live_course = [];

    // this.storage.get('course_filter').then((val) => {
    //   //console.log(val);
    //   if (val != undefined && val != null) {
    let url = "https://intervii.com/backend/request.php";
    // let body = { "get_course_by_case": JSON.stringify(val) };
    let body = { "get_course_by_case": JSON.stringify(this.data_ready) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        // //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          for (let i = 0; i < temp.data.length; i++) {
            if (temp.data[i].rank == undefined) {
              temp.data[i].rank = 0;
            }
          }
          this.filter_course = temp.data;
          this.current_course = temp.data;
          // //console.log(this.current_course);
          if (this.current_course.length == 0) {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No Course";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有课堂";
            }else{
              this.missing_text = "沒有課堂";
            }
          }
        } else {
          if(this.global_service.lang == 'en'){
            this.missing_text = "No Course";
          }else if(this.global_service.lang == 'cn'){
            this.missing_text = "没有课堂";
          }else{
            this.missing_text = "沒有課堂";
          }
        }
        this.connecting_server = false;
      });
    //   }
    // });
  }

  OpenCourse(course, video_id?) {

    if (course == null) {
      let found = false
      this.latest_course.forEach((course_data, index) => {
        if (course_data.video_list.includes(video_id)) {
          found = true;
          // this.global_service.dismiss();
          if (this.current_course_tag == "最新") {
            // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
            this.global_service.course_data = course_data;
            this.global_service.resetLoading();
            this.navCtrl.navigateForward(['/course-detail']);
          }
          if (this.current_course_tag == "試堂") {
            // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
            this.global_service.course_data = course_data;
            this.global_service.resetLoading();
            this.navCtrl.navigateForward(['/course-detail']);
          }
          if (this.current_course_tag == "直播") {
            // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
            this.global_service.course_data = course_data;
            this.global_service.resetLoading();
            this.navCtrl.navigateForward(['/course-detail']);
          }
        }
        else {
          if (index == this.latest_course.length - 1 && !found) {
            if(this.global_service.lang == 'en'){
              this.global_service.presentToast("Course not found");
            }else if(this.global_service.lang == 'cn'){
              this.global_service.presentToast("未能找到課堂");
            }else{
              this.global_service.presentToast("未能找到課堂");
            }
          }
        }
      });
    }
    else {
      if (this.current_course_tag == "最新") {
        // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
        this.global_service.course_data = course;
        this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/course-detail']);
      }
      if (this.current_course_tag == "試堂") {
        // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
        this.global_service.course_data = course;
        this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/course-detail']);
      }
      if (this.current_course_tag == "直播") {
        // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
        this.global_service.course_data = course;
        this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/course-detail']);
      }
    }


  }

  IsLogin() {
    // this.zoomService.isLoggedIn()
    // .then((success: boolean) => {//console.log(success); this.is_logged_in = success;})
    // .catch((error: any) => //console.log(error));
  }

  Login() {
    if (this.global_service.user_id == 0 || this.global_service.user_id == undefined) {
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/login']);
    } else {
      // this.app.getRootNav().push(MyaccountPage, { id: this.user_id });
      // this.navCtrl.push(MyaccountPage, { id: this.user_id });
      this.global_service.view_user_id = this.global_service.user_id;
      this.global_service.back_update = true;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    }
  }

  GoProfile(id) {
    // if (this.global_service.user_data.id == 0 || this.global_service.user_data.id == undefined) {
    // } else {
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      this.global_service.view_user_id = id;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    // }
  }
  GoSetting() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/setting']);
  }

  chat() {
    if (this.global_service.user_data.id == 0 || this.global_service.user_data.id == undefined) {
    } else {
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      // this.global_service.view_user_id = id;
        this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/chatlist']);
    }
  }

  // LoginAndStartMeeting() {
  //   this.zoomService.logout()
  //     .then((success: boolean) => { this.LoginAndStartMeetingAction(); })
  //     .catch((error: any) => { this.LoginAndStartMeetingAction(); });
  // }

  // LoginAndStartMeetingAction() {
  //   this.zoomService.login("rex@innpression.com", "Innpression2019!")
  //     .then((success: any) => { //console.log(success); this.RunStartMeeting(); })
  //     .catch((error: any) => //console.log(error));
  // }

  // StartMeeting() {
  //   this.zoomService.isLoggedIn()
  //     .then((success: boolean) => { if (success) { this.RunStartMeeting() } else { this.LoginAndStartMeeting(); } })
  //     .catch((error: any) => //console.log(error));
  // }

  RunStartMeeting() {

    let url = "https://intervii.com/restapi/zoom_request.php";
    let body = { "create_meeting": JSON.stringify({ "user_id": "OyVTvn3fTIefAVdQQTUiaA", "type": 1 }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        // //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let meeting_data = JSON.parse(temp.data);

          // this.zoomService.joinMeeting(meeting_data.id.toString(), meeting_data.password, "Tutor", {})
          //   .then((success: any) => //console.log(success))
          //   .catch((error: any) => //console.log(error));
        }
      });
  }

  JoinMeetingAction2() {
    let url = "https://intervii.com/restapi/zoom_request.php";
    let body = { "get_current_meeting": '' };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);

        // this.zoomService.joinMeeting(temp.id, temp.password, "Student", {})
        //   .then((success: any) => //console.log(success))
        //   .catch((error: any) => //console.log(error));
      });
  }

  LoadInit() {
    // var top_zone = document.getElementById('top-zone');
    // if (this.platform.is('ios')) {
    //   top_zone.style.top = (-(0.25 * this.platform.width())) + "px";
    // } else {
    //   top_zone.style.top = (-(0.3 * this.platform.width())) + "px";
    // }

    this.storage.get('user_id').then((val) => {
      if (val != undefined && val != null) {
        this.global_service.user_id = val;
      } else {
        this.global_service.user_id = 0;
      }
      this.user_id = this.global_service.user_id;
      // //console.log("==================init user_id " + this.global_service.user_id);
    });

    this.storage.get('user_data').then((val) => {
      if (val != undefined && val != null) {
        this.global_service.user_data = val;
        this.user_data = val;
        // this.global_service.socket_page = "home";
        // this.ConnectToNJS(this.global_service.user_data.id);
        // this.platform.resume.subscribe(async () => {
        //   this.global_service.home_socket == false;
        //   this.ConnectToNJS(this.global_service.user_data.id);
        // });
        this.LoadUserCourseData();
      } else {
        this.global_service.user_data = null;
      }
      // //console.log("==================init user_data " + this.global_service.user_data);
    });

    // this.user_id = this.global_service.user_id;
  }
  AddFavouriteCourse(id) {
    if (this.user_id != 0 && this.user_id != undefined) {
      //console.log(this.user_data);

      let url = "https://intervii.com/backend/request.php";
      let body = { "follow_course": JSON.stringify({ "user_id": this.global_service.user_data.id, "course_id": id }) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      //console.log("==================add favourite body ");
      //console.log(body);
      // this.global_service.isLoading = true;
      // this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          // this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log(data.data);
          if (temp.result == "success") {
            this.user_data.follow_course = temp.data;
            this.global_service.user_data = this.user_data;
            this.storage.set('user_data', this.user_data);


            this.latest_course.forEach((course_data, index) => {
              if(course_data.id == id){
                if(this.user_data.follow_course.includes(id)){
                  if(!course_data.followers.includes(this.user_data.id)){
                    course_data.followers.push(this.user_data.id);
                  }
                }else{
                  if(course_data.followers.includes(this.user_data.id)){
                    let index = course_data.followers.indexOf(this.user_data.id, 0);
                    if (index > -1) {
                      course_data.followers.splice(index, 1);
                    }        
                  }
                }
              }
            });
          }
          //console.log(this.user_data);
        });
    }
  }
  async ConnectToNJS(name) {
    // if (this.global_service.home_socket == false) {
      this.global_service.home_socket = true;
      this.socket.connect();

      this.socket.on('disconnect', (reason) => {
        if (reason === 'io server disconnect') {
          this.socket.connect();
        }else{

          this.socket.connect();
        }
        // else the socket will automatically try to reconnect
      });

      this.socket.emit('reg_user', name);
      this.socket.fromEvent('message').subscribe(async message => {
        if(this.alert_count <=0 && this.global_service.socket_page != "room"){
          this.alert_count++;
          // if (this.global_service.socket_page == "home") {
            this.LoadUserCourseData();
            let msg_title = "你有新訊息";
            let msg_option_1 = "確認";
            if(this.global_service.lang == 'en'){
              msg_title = "You've got new message";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "你有新讯息";
              msg_option_1 = "确认";
            }
            const alert_box = await this.alert.create({
              message: msg_title,
              backdropDismiss:false,
              buttons: [
                {
                  text: msg_option_1,
                  // role: 'cancel',
                  handler: () => {
                    this.alert_count--;
                  }
                }
              ]
            });
            await alert_box.present();
          // }
        }
      });
      this.socket.fromEvent('my_message').subscribe(async message => {
        // if (this.global_service.socket_page == "home") {
        //   this.LoadUserCourseData();
        //   const alert_box = await this.alert.create({
        //     message: "你有新訊息",
        //     buttons: [
        //       {
        //         text: '確認',
        //         // role: 'cancel',
        //         handler: () => {
        //         }
        //       }
        //     ]
        //   });
        //   await alert_box.present();
        // }
      });
    // }
  }


  LoadUserCourseData() {
    this.global_service.unread_count = 0;
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_lecturer_chatrooms": JSON.stringify({ "lecturer_id": this.global_service.user_data.id }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          for(var i=0; i<temp.data.length; i++){
            console.log(temp.data[i]);
            if(temp.data[i].lecturer_unread){
              this.global_service.unread_count++;
            }
          }
        } else {
        }
        this.LoadLecturerData();
      });
  }
  LoadLecturerData() {
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_chatrooms": JSON.stringify({ "user_id": this.global_service.user_data.id }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          for(var i=0; i<temp.data.length; i++){
            console.log(temp.data[i]);
            if(temp.data[i].user_unread){
              this.global_service.unread_count++;
            }
          }
        } else {
        }
        console.log("unread_count : "+this.global_service.unread_count);
      });
  }

  VimeoURL(url){
  
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  
  }
}
