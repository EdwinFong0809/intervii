import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EconInfoPage } from './econ-info.page';

const routes: Routes = [
  {
    path: '',
    component: EconInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EconInfoPageRoutingModule {}
