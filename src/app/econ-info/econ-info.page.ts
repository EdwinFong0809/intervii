import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-econ-info',
  templateUrl: './econ-info.page.html',
  styleUrls: ['./econ-info.page.scss'],
})
export class EconInfoPage implements OnInit {
  current_tab: any;
  current_tag: any;
  course_data:any = [];
  expands:any = [];
  income:any = [];
  payer_data:any = [];
  monthly_econ:any = [];
  course_income_econ:any = [];
  course_incomes:any = [];
  expands_econ:any = [];
  income_total:number = 0;

  expand_loaded:boolean = false;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
    private socket: Socket,
  ) { 
  }

  ngOnInit() {
    this.current_tab = "first_tab";
    this.current_tag = "month";
    this.GetTutorCourse();
    // 
    // this.LoadExpand();
  }


  segmentChanged($event) {
    this.current_tab = $event.detail.value;
    switch (this.current_tab) {
      case "first_tab":
        if (this.current_tag != "month") {
          this.current_tag = "month";
        }
        break;
      case "second_tab":
        if (this.current_tag != "course") {
          this.current_tag = "course";
        }
        break;
      case "third_tab":
        if (this.current_tag != "expand") {
          this.current_tag = "expand";
          if(!this.expand_loaded){
            this.expand_loaded = true;
            this.loadExpand();
          }
        }
        break;
    }
  }

  loadExpand(){
    let url = "https://intervii.com/backend/request.php";
    let json = { user_id: this.global_service.user_data.id };
    let body = { "expend_by_user": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.expands = temp.data;
        } 
        this.LoadExpandLayout();
      });
  }

GetTutorCourse() {
  let url = "https://intervii.com/backend/request.php";
  let json = { user_id: this.global_service.user_data.id };
  let body = { "get_user_open_course": JSON.stringify(json) };
  let headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  };
  this.global_service.isLoading = true;
  this.global_service.presentLoading();
  this.http.post(url, body, headers)
    .then(data => {
      this.global_service.dismiss();
      let temp = JSON.parse(data.data);
      if (temp.result == "success") {
        this.course_data = temp.data;
        this.LoadEconData();
      } 
    });
}

  LoadEconData(){
    let url = "https://intervii.com/backend/request.php";
    let json = { user_id: this.global_service.user_data.id };
    // let body = { "expend_by_id": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    // this.http.post(url, body, headers)
    //   .then(data => {
    //     this.global_service.dismiss();
    //     let temp = JSON.parse(data.data);
    //     if (temp.result == "success") {
    //       this.expands = temp.data;
    //     } 
    //   });

    let body2 = { "income_of_user": JSON.stringify(json) };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body2, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.income = temp.data;
          // this.GetPayerData();
        } 
        this.LoadMonthIncome();
        this.LoadCourseIncome();
      });
  }

  GetPayerData(){
    var payer_ids = [];
    for (var i = 0; i < this.income.length; i++) {
        if (!payer_ids.includes(this.income[i].payer)) {
            payer_ids.push(this.income[i].payer);
        }
    }
    let url = "https://intervii.com/backend/request.php";
    let json = { user_id_list: payer_ids};
    let body = { "get_user_by_id_list": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.payer_data = temp.data;
        } 
        this.LoadMonthIncome();
        this.LoadCourseIncome();
      });
  }

  getCourseName(id){
    for(let i=0; i<this.course_data.length; i++){
      if(this.course_data[i].id == id){
        return this.course_data[i].title;
      }
    }
    return "";
  }

  LoadMonthIncome(){
    var temp_data = [];
    for (var i = 0; i < this.income.length; i++) {
        var year = this.income[i].create_date.substring(0, 4) + "-";
        var month = this.income[i].create_date.substring(5, 7) + "";
        if (temp_data[year] == undefined) {
            temp_data[year] = [];
        }
        if (temp_data[year][month] == undefined) {
            temp_data[year][month] = [];
        }
        temp_data[year][month].push(this.income[i]);
    }

    var temp_data_before_sort = [];
    for (var year_key in temp_data) {
        var month_sort = [];
        for (var month_key in temp_data[year_key]) {
            month_sort.push({ "month_key": month_key, "datas": temp_data[year_key][month_key], "sum": 0 });
        }
        var temp_month = [];
        for (var i = month_sort.length - 1; i >= 0; i--) {
            var sum = 0;
            for (var pay_data of month_sort[i].datas) {
                sum += pay_data.amount;
            }
            month_sort[i].sum = sum;
            temp_month.push(month_sort[i]);
        }
        temp_data_before_sort.push({ "year_key": year_key, "months": temp_month });
    }
    this.monthly_econ = [];
    for (var i = temp_data_before_sort.length - 1; i >= 0; i--) {
      this.monthly_econ.push(temp_data_before_sort[i]);
    }
    console.log(this.monthly_econ);
    console.log("monthly_econ");
    console.log(JSON.stringify(this.monthly_econ));
  }

  LoadCourseIncome(){
    this.income_total = 0;
    this.course_incomes = [];
    console.log(this.course_data);
    for (let x = 0; x < this.course_data.length; x++) {
        let temp_data = [];
        console.log("xxxxxxxxxxxxxxx");
        for (var i = 0; i < this.income.length; i++) {
          console.log(this.income[i]);
          console.log( this.course_data[x].id);
            if (this.income[i].course_id == this.course_data[x].id) {
                var year = this.income[i].create_date.substring(0, 4) + "-";
                var month = this.income[i].create_date.substring(5, 7) + "";
                if (temp_data[year] == undefined) {
                    temp_data[year] = [];
                }
                if (temp_data[year][month] == undefined) {
                    temp_data[year][month] = [];
                }
                temp_data[year][month].push(this.income[i]);
                console.log(temp_data[year][month]);
            }
        }


        console.log("temp_data");
        console.log(temp_data);
        let temp_data_before_sort = [];
        for (let year_key in temp_data) {
            let month_sort = [];
            for (var month_key in temp_data[year_key]) {
                month_sort.push({ "month_key": month_key, "datas": temp_data[year_key][month_key], "sum": 0 });
            }
            let temp_month = [];
            for (var i = month_sort.length - 1; i >= 0; i--) {
                var sum = 0;
                for (var pay_data of month_sort[i].datas) {
                    sum += pay_data.amount;
                }
                month_sort[i].sum = sum;
                temp_month.push(month_sort[i]);
            }
            temp_data_before_sort.push({ "year_key": year_key, "months": temp_month });
        }
        let final_datas = [];
        for (var i = temp_data_before_sort.length - 1; i >= 0; i--) {
          final_datas.push(temp_data_before_sort[i]);
        }
        console.log("final_datas");
        console.log(JSON.stringify(final_datas));
        this.course_incomes.push(final_datas);
    }
  }


  LoadExpandLayout(){
    var temp_data = [];
    for (var i = 0; i < this.expands.length; i++) {
        var year = this.expands[i].create_date.substring(0, 4) + "-";
        var month = this.expands[i].create_date.substring(5, 7) + "";
        if (temp_data[year] == undefined) {
            temp_data[year] = [];
        }
        if (temp_data[year][month] == undefined) {
            temp_data[year][month] = [];
        }
        temp_data[year][month].push(this.expands[i]);
    }

    var temp_data_before_sort = [];
    for (var year_key in temp_data) {
        var month_sort = [];
        for (var month_key in temp_data[year_key]) {
            month_sort.push({ "month_key": month_key, "datas": temp_data[year_key][month_key], "sum": 0 });
        }
        var temp_month = [];
        for (var i = month_sort.length - 1; i >= 0; i--) {
            var sum = 0;
            for (var pay_data of month_sort[i].datas) {
                sum += pay_data.amount;
            }
            month_sort[i].sum = sum;
            temp_month.push(month_sort[i]);
        }
        temp_data_before_sort.push({ "year_key": year_key, "months": temp_month });
    }
    this.expands_econ = [];
    for (var i = temp_data_before_sort.length - 1; i >= 0; i--) {
      this.expands_econ.push(temp_data_before_sort[i]);
    }
    console.log("expands_econ");
    console.log(JSON.stringify(this.expands_econ));
  }

  Pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
  }

  async RefundPayment(index){
    let msg_title = "確定要申請退款？";
    let msg_option_1 = "確認";
    let msg_option_2 = "取消";
    if (this.global_service.lang == 'en') {
      msg_title = "Confirm refund?";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "确定要申请退款？";
      msg_option_1 = "确认";
      msg_option_2 = "取消";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [{
        text: msg_option_1,
        handler: () => {
          let url = "https://intervii.com/backend/request.php";
          let json = { id: index,status_remark: "on-hold"};
          let body = { "update_payment": JSON.stringify(json) };
          let headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
          };
          this.global_service.isLoading = true;
          this.global_service.presentLoading();
          this.http.post(url, body, headers)
            .then(data => {
              this.LoadEconData();
            });
        }

      }, {
        text: msg_option_2,
        handler: () => {}
      }],
      cssClass: 'custom-alertInfo',
    });
    alert_box.present();
  }

  GetTime(data){
    let date = new Date(data);
    return date.getFullYear() + '/' + this.Pad((date.getMonth() + 1), 2,'') + '/' + this.Pad(date.getDate(), 2,'');
  }

  CheckDate(data){
    var today = new Date();
    var record_date = new Date(data.create_date);
    record_date.setDate(record_date.getDate() + 30);
    if (record_date > today && data.payment_status != "paid") {
      return true;
    }else{
      return false;
    }
  }

  PayerName(payer){
    for (var t = 0; t < this.payer_data.length; t++) {
      if (this.payer_data[t].id == payer) {
          return this.payer_data[t].display_name;
      }
    }
    return "";
  }

  MonthMainShow(data){
    var today = new Date();
    for (var b = 0; b < data.months.length; b++) {
    for (var c = 0; c < data.months[b].datas.length; c++) {
        if (data.months[b].datas[c].payment_status == "paid" && (data.months[b].datas[c].status_remark == "" || data.months[b].datas[c].status_remark == "settle")) {
            var record_date = new Date(data.months[b].datas[c].create_date);
            record_date.setDate(record_date.getDate() + 30);
            if (record_date < today) {
              return true;
            }
          }
        }
      }
        return false;
  }

  MonthShow(data){
    var today = new Date();
        if (data.payment_status == "paid" && (data.status_remark == "" || data.status_remark == "settle")) {
            var record_date = new Date(data.create_date);
            record_date.setDate(record_date.getDate() + 30);
            if (record_date < today) {
              return true;
            }
          }
  }

  MonthSum(month_data){
    let sum = 0;
    var today = new Date();
    for (var c = 0; c < month_data.datas.length; c++) {
        if (month_data.datas[c].payment_status == "paid" && (month_data.datas[c].status_remark == "" || month_data.datas[c].status_remark == "settle")) {
            var record_date = new Date(month_data.datas[c].create_date);
            record_date.setDate(record_date.getDate() + 30);
            if (record_date < today) {
              if (month_data.datas[c].amount <= 0) {
              } else {
                  // sum += month_data.datas[c].amount;
                  sum += this.loadPrice(month_data.datas[c]);
              }
            }
          }
        }
        return sum;
  }

  CourseSum(i){
    var total = 0;
    var today = new Date();
    for (var a = 0; a < this.course_incomes[i].length; a++) {
        for (var b = 0; b < this.course_incomes[i][a].months.length; b++) {
            for (var n = 0; n < this.course_incomes[i][a].months[b].datas.length; n++) {
                if (this.course_incomes[i][a].months[b].datas[n].payment_status == "paid" && (this.course_incomes[i][a].months[b].datas[n].status_remark == "" || this.course_incomes[i][a].months[b].datas[n].status_remark == "settle")) {
                    var record_date = new Date(this.course_incomes[i][a].months[b].datas[n].create_date);
                    record_date.setDate(record_date.getDate() + 30);
                    if (record_date < today) {
                        // total += this.course_incomes[i][a].months[b].datas[n].amount;
                        total += this.loadPrice(this.course_incomes[i][a].months[b].datas[n]);
                    }
                }
            }
        }
    }
    return total;
  }

  CourseTotal(){
    var total = 0;
    var today = new Date();
    for (var i= 0; i < this.course_incomes.length; i++) {
    for (var a = 0; a < this.course_incomes[i].length; a++) {
        for (var b = 0; b < this.course_incomes[i][a].months.length; b++) {
            for (var n = 0; n < this.course_incomes[i][a].months[b].datas.length; n++) {
                if (this.course_incomes[i][a].months[b].datas[n].payment_status == "paid" && (this.course_incomes[i][a].months[b].datas[n].status_remark == "" || this.course_incomes[i][a].months[b].datas[n].status_remark == "settle")) {
                    var record_date = new Date(this.course_incomes[i][a].months[b].datas[n].create_date);
                    record_date.setDate(record_date.getDate() + 30);
                    if (record_date < today) {
                        // total += this.course_incomes[i][a].months[b].datas[n].amount;
                        total += this.loadPrice(this.course_incomes[i][a].months[b].datas[n]);
                    }
                }
            }
        }
      }
    }
    return total;
  }

  // loadPrice(data){
  //   if((data.ios_receipt != undefined && data.ios_receipt != null && data.ios_receipt != "") || data.payment_order_data == null || data.payment_order_data == ""){
  //     return Math.floor( data.amount * 0.6 );
  //   }else{
  //     return Math.floor( data.amount * 0.8 );
  //   }
  // }
  loadPrice(data) {
      if ((data.ios_receipt != undefined && data.ios_receipt != null && data.ios_receipt != "") || data.payment_order_data == null || data.payment_order_data == "") {
          return Math.floor(data.amount * 0.6);
      } else {
          return Math.floor(data.amount - (data.amount * 0.134) - 2.35);
      }
  }
  
  loadPriceWithString(data) {
      if ((data.ios_receipt != undefined && data.ios_receipt != null && data.ios_receipt != "") || data.payment_order_data == null || data.payment_order_data == "") {
          return Math.floor(data.amount * 0.6) + " [iOS]";
      } else {
          return Math.floor(data.amount - (data.amount * 0.134) - 2.35) + " [PayPal]";
      }
  }
}
