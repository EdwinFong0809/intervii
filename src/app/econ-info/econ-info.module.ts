import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EconInfoPageRoutingModule } from './econ-info-routing.module';

import { EconInfoPage } from './econ-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EconInfoPageRoutingModule
  ],
  declarations: [EconInfoPage]
})
export class EconInfoPageModule {}
