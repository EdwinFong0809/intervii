import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EconInfoPage } from './econ-info.page';

describe('EconInfoPage', () => {
  let component: EconInfoPage;
  let fixture: ComponentFixture<EconInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EconInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
