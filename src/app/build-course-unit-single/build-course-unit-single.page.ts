import { Component, OnInit, NgZone } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'app-build-course-unit-single',
  templateUrl: './build-course-unit-single.page.html',
  styleUrls: ['./build-course-unit-single.page.scss'],
})
export class BuildCourseUnitSinglePage implements OnInit {
  video_type: any;
  multi_unit: any;
  number_of_unit: any = 0;
  unit_count: any = 0;
  course: any;
  course_id: any;
  user_id: any;
  success_array: any = [];
  loading: any;
  loaded: boolean = false;
  uploading: boolean = false;
  button_text: string = "下一步";
  edit_video: any = null;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    private streamingMedia: StreamingMedia,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private camera: Camera,
    private file: File,
    private filePath: FilePath,
    private transfer: FileTransfer,
    private photoViewer: PhotoViewer,
    private zone: NgZone,
    public actionSheetCtrl: ActionSheetController,
  ) {
    if(this.global_service.lang == 'en'){
      this.button_text = "Next";
    }else if(this.global_service.lang == 'cn'){
      this.button_text = "下一步";
    }else{
      this.button_text = "下一步";
    }
   }

  ngOnInit() {
  }


  ionViewDidEnter() {
    //console.log('ionViewDidLoad BuildyourcourseCreateUnitPage');
    this.multi_unit = [
    ];
    this.course_id = this.global_service.create_course.id;
    this.success_array = [];
    this.course = this.global_service.create_course;
    //console.log(this.course);
    this.user_id = this.global_service.user_id;
    //load unit datas
    this.edit_video = this.global_service.edit_course_unit;
  }
  async Dismiss() {
    let is_data_pass = true;
    if (this.uploading == false && is_data_pass) {
      this.uploading = true;
      this.UploadData();
    }

  }
  UploadData() {
    //console.log("UploadData");
    let is_live;
    if (this.edit_video.video_type == "live_streaming") {
      is_live = true;
    } else {
      is_live = false;
    }
    let url = "https://intervii.com/backend/request.php";
    var json;
    if (is_live) {
      if(this.edit_video.live_streaming_date == "" || this.edit_video.live_streaming_date <= 0){
        if(this.global_service.lang == 'en'){
          alert("Length cannot be empty or 0");
        }else if(this.global_service.lang == 'cn'){
          alert("预计长度不能空白或0");
        }else{
          alert("預計長度不能空白或0");
        }
        return;
      }
      let temp_live_date = new Date(this.edit_video.live_streaming_date);
      temp_live_date.setHours(temp_live_date.getHours() - 8);
      let date_string = temp_live_date.getFullYear() + "-" + (temp_live_date.getMonth() + 1).toString().padStart(2, '0') + "-" + temp_live_date.getDate().toString().padStart(2, '0') + "T" + temp_live_date.getHours().toString().padStart(2, '0') + ":" + temp_live_date.getMinutes().toString().padStart(2, '0') + ":00";
      json = {
        "id": this.edit_video.id,
        "user_id": this.user_id,
        "title": this.edit_video.video_title,
        // "is_preview": false,
        "is_live": is_live,
        "live_length": parseInt(this.edit_video.live_streaming_period),
        "live_date": date_string,
        "live_link": this.edit_video.live_link,
      };
    } else {
      if(this.edit_video.duration == "" || this.edit_video.duration <= 0){
        if(this.global_service.lang == 'en'){
          alert("Length cannot be empty or 0");
        }else if(this.global_service.lang == 'cn'){
          alert("影片长度不能空白或0");
        }else{
          alert("影片長度不能空白或0");
        }
        return;
      }
      json = {
        "id": this.edit_video.id,
        "user_id": this.user_id,
        "title": this.edit_video.video_title,
        // "is_preview": false,
        "is_live": is_live,
        "file_type": this.edit_video.file_type,
        "file_name": this.edit_video.file_name,
        "file_vimeo": this.edit_video.file_vimeo,
        // "file_data": this.edit_video.file_data,
        "preview_file_type": this.edit_video.preview_file_type,
        "preview_file_name": this.edit_video.preview_file_name,
        "preview_file_vimeo": this.edit_video.preview_file_vimeo,
        "duration": this.edit_video.duration//,
        // "preview_file_data": this.edit_video.preview_file_data
      };
    }
    //console.log(json);
    let body;
    if (this.edit_video.id > -1) {
      body = { "update_video": JSON.stringify(json) };
    } else {
      body = { "new_video": JSON.stringify(json) };
    }
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.success_array = this.global_service.create_course.video_list;
          if (!this.success_array.includes(temp.data.id)) {
            this.success_array.push(temp.data.id);
            this.UpdateFinalList();
          } else {
            //back
        this.global_service.resetLoading();
            this.navCtrl.pop();
          }
        } else {
          //update fail
          // alert
        this.uploading = false;
          alert(temp.data);
        }
      });
  }
  UpdateFinalList() {
    this.course.video_list = this.success_array;
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id,
      "video_list": this.course.video_list
    };
    let body = { "update_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        this.uploading = false;
        if (JSON.parse(data.data).result == "success") {
          this.global_service.create_course = JSON.parse(data.data).data;
          this.global_service.resetLoading();
          this.navCtrl.pop();
        }
      });
  }
  GoBack() {
    // this.viewCtrl.dismiss();
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  CallLoadVideo() {
    this.CallLoadVideoDo();
  }
  CallLoadVideoDo() {
    var options = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      // alert(data);


      let file_data = data;
      if (this.plt.is('android')) {
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.http.post(url, body, headers)
          .then(data => {
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if (file_type == 'MOV') {
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.zone.run(() => {
            this.edit_video.file_name = file_name;
            this.edit_video.file_type = file_type;
            });

            const fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            fileTransfer.onProgress(async (progressEvent) => {
              if (progressEvent.lengthComputable) {
                // //console.log(progressEvent);
                this.zone.run(() => {
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                });
              } else {
              }
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload_vimeo.php', options)
              .then((data) => {
                // success
                // if(data)
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  // alert("上傳成功");
                  let url = "https://intervii.com/backend/request.php";
                  let body;
                  //console.log(this.vlog_data);
                      console.log("upload_vimeo");
                  body = { "vimeo_transfer": JSON.stringify({"file_name":this.edit_video.file_name}) };
                  let headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  };
                  this.http.post(url, body, headers)
                    .then(async data => {
                      let temp = JSON.parse(data.data);
                      console.log(data.data);
                      // this.loading.dismiss();
                      if (temp.result == "success") {
                        this.edit_video.file_vimeo = temp.data;
                        let msg_title = "上傳成功";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload success";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载成功";
                        }
                        alert(msg_title);
                      } else {
                        let msg_title = "上傳失敗";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload fail";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载失败";
                        }
                        alert(msg_title);
                      }
                    });
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
              }, (err) => {
                // error
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                // alert(JSON.stringify(err));
              });
          });

      } else {
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.http.post(url, body, headers)
          .then(data => {
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if (file_type == 'MOV') {
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.zone.run(() => {
            this.edit_video.file_name = file_name;
            this.edit_video.file_type = file_type;
            });

            let fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            fileTransfer.onProgress((progressEvent) => {
              // alert(JSON.stringify(progressEvent));
              if (progressEvent.lengthComputable) {
                this.zone.run(() => {
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                });
              } else {
              }
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload_vimeo.php', options)
              .then((data) => {
                // success
                // if(data)
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  // alert("上傳成功");
                  let url = "https://intervii.com/backend/request.php";
                  let body;
                  //console.log(this.vlog_data);
                      console.log("upload_vimeo");
                  body = { "vimeo_transfer": JSON.stringify({"file_name":this.edit_video.file_name}) };
                  let headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  };
                  this.http.post(url, body, headers)
                    .then(async data => {
                      let temp = JSON.parse(data.data);
                      console.log(data.data);
                      // this.loading.dismiss();
                      if (temp.result == "success") {
                        this.edit_video.file_vimeo = temp.data;
                        let msg_title = "上傳成功";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload success";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载成功";
                        }
                        alert(msg_title);
                      } else {
                        let msg_title = "上傳失敗";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload fail";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载失败";
                        }
                        alert(msg_title);
                      }
                    });
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
              }, (err) => {
                // error
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
              });
          });
      }
    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }
  CallLoadPreviewVideo() {
    this.CallLoadPreviewVideoDo();
  }
  CallLoadPreviewVideoDo() {
    var options = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      let file_data = data;
      if (this.plt.is('android')) {
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if (file_type == 'MOV') {
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.zone.run(() => {
            this.edit_video.preview_file_name = file_name;
            this.edit_video.preview_file_type = file_type;
            });
            let fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            fileTransfer.onProgress(async (progressEvent) => {
              if (progressEvent.lengthComputable) {
                // //console.log(progressEvent);
                this.zone.run(() => {
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                });
              } else {
              }
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload_vimeo.php', options)
              .then((data) => {
                // success
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  // alert("上傳成功");
                  let url = "https://intervii.com/backend/request.php";
                  let body;
                  //console.log(this.vlog_data);
                      console.log("upload_vimeo");
                  body = { "vimeo_transfer": JSON.stringify({"file_name":this.edit_video.preview_file_name}) };
                  let headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  };
                  this.http.post(url, body, headers)
                    .then(async data => {
                      let temp = JSON.parse(data.data);
                      console.log(data.data);
                      // this.loading.dismiss();
                      if (temp.result == "success") {
                        this.edit_video.preview_file_vimeo = temp.data;
                        let msg_title = "上傳成功";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload success";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载成功";
                        }
                        alert(msg_title);
                      } else {
                        let msg_title = "上傳失敗";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload fail";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载失败";
                        }
                        alert(msg_title);
                      }
                    });
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                this.global_service.dismiss();
                // alert(JSON.stringify(data));
              }, (err) => {
                // error
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                this.global_service.dismiss();
              });
          });
      } else {
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if (file_type == 'MOV') {
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.zone.run(() => {
            this.edit_video.preview_file_name = file_name;
            this.edit_video.preview_file_type = file_type;
            });

            let fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            fileTransfer.onProgress(async (progressEvent) => {
              if (progressEvent.lengthComputable) {
                // //console.log(progressEvent);
                this.zone.run(() => {
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                });
              } else {
              }
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload_vimeo.php', options)
              .then((data) => {
                // success
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  // alert("上傳成功");
                  let url = "https://intervii.com/backend/request.php";
                  let body;
                  //console.log(this.vlog_data);
                      console.log("upload_vimeo");
                  body = { "vimeo_transfer": JSON.stringify({"file_name":this.edit_video.preview_file_name}) };
                  let headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  };
                  this.http.post(url, body, headers)
                    .then(async data => {
                      let temp = JSON.parse(data.data);
                      console.log(data.data);
                      // this.loading.dismiss();
                      if (temp.result == "success") {
                        this.edit_video.preview_file_vimeo = temp.data;
                        let msg_title = "上傳成功";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload success";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载成功";
                        }
                        alert(msg_title);
                      } else {
                        let msg_title = "上傳失敗";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload fail";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载失败";
                        }
                        alert(msg_title);
                      }
                    });
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                // alert(JSON.stringify(data));
                this.global_service.dismiss();
              }, (err) => {
                // error
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                this.global_service.dismiss();
              });
          });
      }


    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }


  async PlayVideo(){
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') },
        orientation: 'landscape',
        shouldAutoClose: true,
        controls: true
      };

      this.streamingMedia.playVideo(('https://intervii.com/media/'+this.edit_video.file_name), options);
  }
  async PlayPreviewVideo(){

        let options: StreamingVideoOptions = {
          successCallback: () => { console.log('Video played') },
          errorCallback: (e) => { console.log('Error streaming') },
          orientation: 'landscape',
          shouldAutoClose: true,
          controls: true
        };

        this.streamingMedia.playVideo(('https://intervii.com/media/'+this.edit_video.preview_file_name), options);
  }
}
