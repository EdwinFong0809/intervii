import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseUnitSinglePage } from './build-course-unit-single.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseUnitSinglePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseUnitSinglePageRoutingModule {}
