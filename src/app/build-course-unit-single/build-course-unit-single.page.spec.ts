import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCourseUnitSinglePage } from './build-course-unit-single.page';

describe('BuildCourseUnitSinglePage', () => {
  let component: BuildCourseUnitSinglePage;
  let fixture: ComponentFixture<BuildCourseUnitSinglePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCourseUnitSinglePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCourseUnitSinglePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
