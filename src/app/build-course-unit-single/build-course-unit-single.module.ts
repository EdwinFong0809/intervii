import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseUnitSinglePageRoutingModule } from './build-course-unit-single-routing.module';

import { BuildCourseUnitSinglePage } from './build-course-unit-single.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseUnitSinglePageRoutingModule
  ],
  declarations: [BuildCourseUnitSinglePage]
})
export class BuildCourseUnitSinglePageModule {}
