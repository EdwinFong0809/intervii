import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-course-question-detail',
  templateUrl: './course-question-detail.page.html',
  styleUrls: ['./course-question-detail.page.scss'],
})
export class CourseQuestionDetailPage implements OnInit {
  question_data: any = {};
  reply: any = [];
  course_detail: any = {};
  purchase: boolean;
  press: boolean = false;
  reply_comment: string;
  user_data: any;
  user_id: any;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
  ) {
    this.question_data = {};
    this.reply = [];
    this.course_detail = {};
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.purchase = this.global_service.course_data.purchase;
    this.course_detail = this.global_service.course_data;
    this.question_data = this.global_service.question_detail;
    //console.log(this.question_data);
    this.user_data = this.global_service.user_data;
    this.user_id = this.global_service.user_data.id;

    this.LoadData();
  }
  LoadData() {
    this.reply = [];


    let url = "https://intervii.com/backend/request.php";
    let body = { "get_comment_by_id": JSON.stringify({ "comment_id": this.question_data.id }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        //console.log(data.data);
        if (temp.result == "success") {
          temp.data.display_name = '';
          temp.data.user_icon = '';
          temp.data.hide = false;
          temp.data.content = temp.data.content.replace(/\n/g, '<br \/>');
          this.question_data = temp.data;

          let body2 = { "get_user_by_id": JSON.stringify({ "user_id": this.question_data.user_id }) };
          this.global_service.isLoading = true;
          this.global_service.presentLoading();
          this.http.post(url, body2, headers)
            .then(data => {
              this.global_service.dismiss();
              let temp2 = JSON.parse(data.data);
              // //console.log(data.data);
              if (temp2.result == "success") {
                this.question_data.display_name = temp2.data.display_name;
                this.question_data.user_icon = temp2.data.icon;
                this.question_data.question_time = this.question_data.create_date.replace('T', ' ').replace('Z', ' ');
                // let current_time = Date.now();
                // let temp_time = new Date(this.question_data.create_date);
                // let time_different = current_time - temp_time.getTime() +(8*3600000);
                // //console.log(temp_time);
                // //console.log(temp_time.getTime());
                // time_different = time_different/1000;
                // if(time_different < 60){
                //   this.question_data.question_time = Math.floor(time_different)+"秒前";
                // }else{
                //   time_different = time_different/60;
                //   if(time_different < 60){
                //     this.question_data.question_time = Math.floor(time_different)+"分鐘前";
                //   }else{
                //     time_different = time_different/60;
                //     if(time_different < 24){
                //       this.question_data.question_time = Math.floor(time_different)+"小時前";
                //     }else{
                //         time_different = time_different/24;
                //         this.question_data.question_time = Math.floor(time_different)+"天前";
                //     }
                //   }
                // }
              }


              let ids = [];
              // this.question_data.content = this.question_data.content.replace(/\n/g, '<br \/>');
              for (let i = 0; i < this.question_data.reply.length; i++) {
                this.question_data.reply[i].display_name = '';
                this.question_data.reply[i].user_icon = '';
                this.question_data.reply[i].content = this.question_data.reply[i].content.replace(/\n/g, '<br \/>');
                ids.push(this.question_data.reply[i].user_id);
              }
              this.reply = this.question_data.reply;

              let body3 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": ids }) };
              this.global_service.isLoading = true;
              this.global_service.presentLoading();
              this.http.post(url, body3, headers)
                .then(data => {
                  this.global_service.dismiss();
                  let temp3 = JSON.parse(data.data);
                  // //console.log(data.data);
                  if (temp3.result == "success") {
                    let id_profile = [];
                    for (let i = 0; i < temp3.data.length; i++) {
                      id_profile[temp3.data[i].id] = temp3.data[i];
                    }

                    for (let i = 0; i < this.reply.length; i++) {
                      this.reply[i].display_name = id_profile[this.reply[i].user_id].display_name;
                      this.reply[i].user_icon = id_profile[this.reply[i].user_id].icon;
                    }
                  }
                });
            });
        }
      });
  }
  async Send() {
    if (!this.press) {
      this.press = true;
      if (this.reply_comment == "") {
        let msg_title = "請勿留空";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Please fill in all";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "请勿留空";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title,
          backdropDismiss:false,
          buttons: [{ text: msg_option_1 }]
        });
        await alert_box.present();
        this.press = false;
      } else {
        let url = "https://intervii.com/backend/request.php";
        var json = { "comment_id": this.question_data.id, "user_id": this.user_id, "title": "", "content": this.reply_comment };
        let body = { "reply_comment": JSON.stringify(json) };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(async data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              let msg_title = "提交成功";
              let msg_option_1 = "確認";
              if (this.global_service.lang == 'en') {
                msg_title = "Success";
                msg_option_1 = "Confirm";
              }
              if (this.global_service.lang == 'cn') {
                msg_title = "提交成功";
                msg_option_1 = "确认";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [{ text: msg_option_1 }]
              });
              await alert_box.present();
              this.reply_comment = "";
              this.LoadData();
            } else {
              let msg_title = "提交失敗";
              let msg_option_1 = "確認";
              if (this.global_service.lang == 'en') {
                msg_title = "Fail";
                msg_option_1 = "Confirm";
              }
              if (this.global_service.lang == 'cn') {
                msg_title = "提交失败";
                msg_option_1 = "确认";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [{ text: msg_option_1 }]
              });
              await alert_box.present();
            }
            this.press = false;
          });
      }
    }
  }

  GoBack() {
    // this.viewCtrl.dismiss();
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
}
