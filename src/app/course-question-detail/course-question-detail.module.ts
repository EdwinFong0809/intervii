import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourseQuestionDetailPageRoutingModule } from './course-question-detail-routing.module';

import { CourseQuestionDetailPage } from './course-question-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourseQuestionDetailPageRoutingModule
  ],
  declarations: [CourseQuestionDetailPage]
})
export class CourseQuestionDetailPageModule {}
