import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CourseQuestionDetailPage } from './course-question-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CourseQuestionDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CourseQuestionDetailPageRoutingModule {}
