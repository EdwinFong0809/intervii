import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VlogPage } from './vlog.page';

describe('VlogPage', () => {
  let component: VlogPage;
  let fixture: ComponentFixture<VlogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VlogPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VlogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
