import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VlogPageRoutingModule } from './vlog-routing.module';

import { VlogPage } from './vlog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VlogPageRoutingModule
  ],
  declarations: [VlogPage]
})
export class VlogPageModule {}
