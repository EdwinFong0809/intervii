import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VlogPage } from './vlog.page';

const routes: Routes = [
  {
    path: '',
    component: VlogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VlogPageRoutingModule {}
