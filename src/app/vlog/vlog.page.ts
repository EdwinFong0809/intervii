import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { Router, NavigationExtras } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from "../global.service";
import { EventsService } from '../events.service';


@Component({
  selector: 'app-vlog',
  templateUrl: './vlog.page.html',
  styleUrls: ['./vlog.page.scss'],
})
export class VlogPage implements OnInit {

  current_page: any = 0;
  categories_data: any = {
    category: ""
  };
  user_id: any;
  temp_event: any;
  current_course: any;
  user_data: any;
  direction: string = "";
  logined: boolean = false;
  current_vlog_tag: string = "最新";
  vlogs: any;
  can_refresh:boolean = true;

  data_ready: any = { type: "全部", course_categories: "所有類別", sort: "由最新" };
  categories: any = "全部";
  list_of_choice: any = ['全部', '運動', '藝術', '美容'];
  current_tab: string = "first_tab";
  searched: boolean = false;
  missing_text:string = "";

  connecting_server: boolean = false;

  refresher: any = null;

  // constructor() { }

  ngOnInit() {
    this.LoadData();
  }

  constructor(
    private alert: AlertController, 
    private storage: Storage,
    public http: HTTP,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private sanitizer: DomSanitizer,
    public events: EventsService,
    private router: Router
  ) {
    this.vlogs = [];
    this.user_id = this.global_service.user_id;
    this.missing_text = "";

    this.events.getObservable().subscribe((data) => {
      console.log(data);
      if(data.data == "refresh_vlog"){
        this.LoadData();
      }
    });
    // this.storage.set('vlog_filter', { course_categories: "所有類別", types: "全部" });
  }
  ionViewWillEnter(){
    this.user_id = this.global_service.user_id;
    this.data_ready = { type: "全部", course_categories: "所有類別", sort: "由最新" };
    if(this.global_service.back_update){
      this.global_service.back_update = false;
      this.LoadData();
    }else{
      this.LoadData();
    }
    this.LoadCategory();
  }
  doRefresh(refresher){
    console.log("refresh");
    this.refresher = refresher;
    this.can_refresh = false;
    setTimeout(function(){
      this.can_refresh = true;
    }, 10000);
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        // if (this.current_vlog_tag != "最新") {
          this.current_vlog_tag = "最新";
          this.vlogs = [];
          this.missing_text = "";
          this.LoadData();
        // }
        break;
      case "second_tab":
        // //console.log(this.tab4);
        this.current_vlog_tag = "分類";
        this.vlogs = [];
        this.missing_text = "";
        this.searched = false;
        // this.LoadData();
        break;
    }
    setTimeout(() => {
      refresher.target.complete();
    }, 2000);
  }
  LoadCategory(){
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_all_active_category": "" };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        if(temp.result=="success"){
          this.global_service.categories = temp.data;
        }
      });
  }

  segmentChanged($event) {
    //console.log($event.detail.value);
    this.current_tab = $event.detail.value;
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        if (this.current_vlog_tag != "最新") {
          this.current_vlog_tag = "最新";
          this.vlogs = [];
          this.missing_text = "";
          this.LoadData();
        }
        break;
      case "second_tab":
        // //console.log(this.tab4);
        this.current_vlog_tag = "分類";
        this.vlogs = [];
        this.missing_text = "";
        this.searched = false;
        // this.LoadData();
        break;
    }
  }

  LoadData() {
    this.connecting_server = true;
    // this.storage.get('user_data').then((val) => {
    if (this.global_service.user_data != undefined && this.global_service.user_data != null) {
      this.user_data = this.global_service.user_data;
      this.logined = true;
      let url = "https://intervii.com/backend/request.php";
      var json = {
        "user_id": this.user_data.id
      };
      let body = { "get_user_by_id": JSON.stringify(json) };
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          this.user_data = temp.data;

          if (this.current_vlog_tag == "最新" || this.data_ready.type == "全部") {
            let body2 = { "get_vlogs": "" };
            this.global_service.isLoading = true;
            this.global_service.presentLoading();
            this.http.post(url, body2, headers)
              .then(data => {
                this.global_service.dismiss();
                // //console.log(data.data);
                let temp = JSON.parse(data.data);
                if (temp.result == "success") {
                  this.vlogs = temp.data;
                  if(this.vlogs.length == 0){
                    if(this.global_service.lang == 'en'){
                      this.missing_text = "No Vlog";
                    }else if(this.global_service.lang == 'cn'){
                      this.missing_text = "没有Vlog";
                    }else{
                      this.missing_text = "沒有Vlog";
                    }
                  }
                }else{
                  if(this.global_service.lang == 'en'){
                    this.missing_text = "No Vlog";
                  }else if(this.global_service.lang == 'cn'){
                    this.missing_text = "没有Vlog";
                  }else{
                    this.missing_text = "沒有Vlog";
                  }
                }
                this.connecting_server = false;
                this.refresher.target.complete();
              });
          } else {
            let body2 = { "get_vlogs_by_category": JSON.stringify({ "category": this.data_ready.type }) };
            this.global_service.isLoading = true;
            this.global_service.presentLoading();
            this.http.post(url, body2, headers)
              .then(data => {
                this.global_service.dismiss();
                //console.log(data.data);
                let temp = JSON.parse(data.data);
                if (temp.result == "success") {
                  this.vlogs = temp.data;
                  if(this.vlogs.length == 0){
                    if(this.global_service.lang == 'en'){
                      this.missing_text = "No Vlog";
                    }else if(this.global_service.lang == 'cn'){
                      this.missing_text = "没有Vlog";
                    }else{
                      this.missing_text = "沒有Vlog";
                    }
                  }
                }else{
                  if(this.global_service.lang == 'en'){
                    this.missing_text = "No Vlog";
                  }else if(this.global_service.lang == 'cn'){
                    this.missing_text = "没有Vlog";
                  }else{
                    this.missing_text = "沒有Vlog";
                  }
                }
                this.connecting_server = false;
                this.refresher.target.complete();
              });
          }
        });
    } else {
      this.user_data = 0;

      let url = "https://intervii.com/backend/request.php";
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      if (this.current_vlog_tag == "最新" || this.data_ready.type == "全部") {
        let body2 = { "get_vlogs": "" };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body2, headers)
          .then(data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              this.vlogs = temp.data;
              if(this.vlogs.length == 0){
                if(this.global_service.lang == 'en'){
                  this.missing_text = "No Vlog";
                }else if(this.global_service.lang == 'cn'){
                  this.missing_text = "没有Vlog";
                }else{
                  this.missing_text = "沒有Vlog";
                }
              }
            }else{
              if(this.global_service.lang == 'en'){
                this.missing_text = "No Vlog";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有Vlog";
              }else{
                this.missing_text = "沒有Vlog";
              }
            }
            this.connecting_server = false;
            this.refresher.target.complete();
          });
      } else {
        let body2 = { "get_vlogs_by_category": JSON.stringify({ "category": this.data_ready.type }) };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body2, headers)
          .then(data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              this.vlogs = temp.data;
              if(this.vlogs.length == 0){
                if(this.global_service.lang == 'en'){
                  this.missing_text = "No Vlog";
                }else if(this.global_service.lang == 'cn'){
                  this.missing_text = "没有Vlog";
                }else{
                  this.missing_text = "沒有Vlog";
                }
              }
            }else{
              if(this.global_service.lang == 'en'){
                this.missing_text = "No Vlog";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有Vlog";
              }else{
                this.missing_text = "沒有Vlog";
              }
            }
            this.connecting_server = false;
            this.refresher.target.complete();
          });
      }
    }
    //console.log("user_data");
    //console.log(this.user_data);
    // });
  }

  async AddVlog() {
    let msg_title = "請先登入";
    let msg_option_1 = "確認";
    if(this.global_service.lang == 'en'){
      msg_title = "Please login first";
      msg_option_1 = "Confirm";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "请先登入";
      msg_option_1 = "确认";
    }
    //console.log(this.user_data);
    if (this.user_data == undefined || this.user_data == null || this.user_data == 0) {
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [{
          text: msg_option_1,
          handler: () => {
          }
        }]
      });
      await alert_box.present();
    } else {
      if(this.global_service.user_data != undefined && this.global_service.user_data!=null && this.global_service.user_data!='' && this.global_service.user_data.is_lecturer!=undefined && this.global_service.user_data.is_lecturer!=null && (this.global_service.user_data.is_lecturer==true || this.global_service.user_data.is_lecturer=='true')){
        
        this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/add-vlog']);
      }else{

      let msg_title = "目前用戶類型不能上傳Vlog";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Current user type cannot upload Vlog";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "目前用户类型不能上传Vlog";
        msg_option_1 = "确定";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
      }
      // const modal = this.modalCtrl.create(AddVlogPage, { course: this });
      // modal.onDidDismiss(data => {
      //   //console.log(data);
      //   if (data != undefined) {
      //     alert(JSON.stringify(data));
      //   }
      // })
      // modal.present();
    }
  }
  GoProfile(id) {
    // if (this.logined) {
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      this.global_service.view_user_id = id;
      this.global_service.back_update = true;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    // }
  }
  OpenVlog(vlog) {
    this.global_service.vlog_data = vlog;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/vlog-detail']);
  }

  InitFilter() {
    this.connecting_server = true;
    this.searched = true;
    this.missing_text = "";
    this.vlogs = [];
    let url = "https://intervii.com/backend/request.php";
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    if (this.data_ready.type == "全部") {
      let body2 = { "get_vlogs": "" };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body2, headers)
        .then(data => {
          this.global_service.dismiss();
          //console.log(data.data);
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.vlogs = temp.data;
            if(this.vlogs.length == 0){
              if(this.global_service.lang == 'en'){
                this.missing_text = "No Vlog";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有Vlog";
              }else{
                this.missing_text = "沒有Vlog";
              }
            }
          }else{
            if(this.global_service.lang == 'en'){
              this.missing_text = "No Vlog";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有Vlog";
            }else{
              this.missing_text = "沒有Vlog";
            }
          }
          this.connecting_server = false;
        });
    } else {
      let body2 = { "get_vlogs_by_category": JSON.stringify({ "category": this.data_ready.type }) };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body2, headers)
        .then(data => {
          this.global_service.dismiss();
          //console.log(data.data);
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.vlogs = temp.data;
            if(this.vlogs.length == 0){
              if(this.global_service.lang == 'en'){
                this.missing_text = "No Vlog";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有Vlog";
              }else{
                this.missing_text = "沒有Vlog";
              }
            }
          }else{
            if(this.global_service.lang == 'en'){
              this.missing_text = "No Vlog";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有Vlog";
            }else{
              this.missing_text = "沒有Vlog";
            }
          }
          this.connecting_server = false;
        });
    }
  }

  VlogsInit(){
    for(let i=0; i<this.vlogs.length; i++){
      this.vlogs[i].content = this.sanitizer.bypassSecurityTrustHtml(this.vlogs[i].content);
    }
  }
}
