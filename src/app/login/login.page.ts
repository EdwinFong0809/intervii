import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams, AlertController, Platform, LoadingController } from '@ionic/angular';//App
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { GlobalService } from "../global.service";
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  ngOnInit() {
  }
  swapped: boolean = false;
  data: any = {
    email: "",
    password: "",
    confirm_password: ""
  };
  hidden_message: any = "";
  hidden_message_show: boolean = false;
  checked: boolean = false;
  login_checked: boolean = false;
  login_pass: boolean = false;
  login_hidden_message: any = "";
  password_normal: boolean = true;
  password_failed: boolean = false;
  password_passed: boolean = false;
  confirm_passed = false;
  confirm_normal = true;
  confirm_failed = false;
  confirm_failed_message: any = "";
  password_failed_message: any = "";
  notification_id: string = "";
  keyboard_close: boolean = false;
  pressed: boolean = false;
  loading: any;
  focus_rate: number = 0;

  constructor(public modalCtrl: ModalController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    private keyboard: Keyboard,
    // public app: App, 
    private storage: Storage,
    public http: HTTP,
    private alert: AlertController,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public status_bar: StatusBar,
    private fb: Facebook,
    // public navParams: NavParams
  ) {
    // this.navCtrl.swipeBackEnabled = true;
    this.storage.get('notification_user_id').then((val) => {
      if (val != undefined && val != null) {
        this.notification_id = val;
      } else {
        this.notification_id = "";
      }
    });
    if (this.platform.is('ios')) {
    } else {
      this.keyboard.onKeyboardShow().subscribe(data => { this.keyboard_close = true; });
      this.keyboard.onKeyboardHide().subscribe(data => { this.keyboard_close = false; });
    }
    // this.loading = this.loadingCtrl.create({
    // });
  }

  ionViewWillEnter() {
    // this.navCtrl.swipeBackEnabled = true;
    this.pressed = false;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LoginPage');
    this.storage.get('notification_user_id').then((val) => {
      if (val != undefined && val != null) {
        this.notification_id = val;
      } else {
        this.notification_id = "";
      }
    });
  }

  SwapPage() {
    //console.log(this.swapped);
    if (!this.swapped) {
      this.swapped = true;
      //registerPage;
      if (this.data.email != "") {
        this.CheckEmailExist();
      } else {
        this.hidden_message_show = false;
        this.hidden_message = "";
        this.checked = false;
      }

      document.getElementById('login_btn').className = 'register_button';
      document.getElementById('register_btn').className = 'login_button';
      document.getElementById('register_btn').style["background-color"] = '#FDADC9';
      document.getElementById('login_btn').style["background-color"] = '#ED2B6F';
      document.getElementById('register_btn').style["color"] = '#FFF';
      document.getElementById('login_btn').style["color"] = '#FFF';
    } else {
      this.swapped = false;
      if (this.data.email != "") {
        this.LoginEmailCheck();
      } else {
        this.login_checked = false;
        this.login_pass = false;
        this.login_hidden_message = "";
      }
      document.getElementById('login_btn').className = 'login_button';
      document.getElementById('register_btn').className = 'register_button';
      document.getElementById('login_btn').style["background-color"] = '#ED2B6F';
      document.getElementById('register_btn').style["background-color"] = '##FDADC9';
      document.getElementById('register_btn').style["color"] = '#FFF';
      document.getElementById('login_btn').style["color"] = '#FFF';
    }
  }

  Pop() {
    // this.navCtrl.navigateBack('/tabs/home');
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  async Login() {
    if (!this.pressed) {
      this.pressed = true;
      if (this.login_hidden_message == "" && this.password_failed_message == ""
        && this.data.email != "" && this.data.password != "") {
        // this.loading.present();
        let url = "https://intervii.com/backend/request.php";
        var json = { "email": this.data.email, "password": this.data.password, "notification_id": this.notification_id };
        let body = { "login": JSON.stringify(json) };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        //console.log(123);
        this.http.post(url, body, headers)
          .then(async data => {
            this.global_service.dismiss();
            // this.loading.dismiss();
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              this.pressed = false;
              this.storage.set('user_id', temp.data.id);
              this.storage.set('user_data', temp.data);

              this.global_service.user_id = temp.data.id;
              this.global_service.user_data = temp.data;
              // this.app.getRootNav().setRoot(TabsPage);
              // const alert_box = await this.alert.create({
              //   // enableBackdropDismiss: false,
              //   message: "登入成功",
              //   buttons: ['OK'],
              //   cssClass: 'custom-alertInfo',
              // });
              // await alert_box.present();
        this.global_service.resetLoading();
              this.navCtrl.navigateRoot(['/tabs/home']);
            } else {
              this.pressed = false;
              let msg_title = "電郵或密碼錯誤";
              let msg_option_1 = "確定";
              if(this.global_service.lang == 'en'){
                msg_title = "Incorrect email or password";
                msg_option_1 = "Confirm";
              }
              if(this.global_service.lang == 'cn'){
                msg_title = "电邮或密码错误";
                msg_option_1 = "确定";
              }
              const alert_box = await this.alert.create({
                // enableBackdropDismiss: false,
                message: msg_title,
                buttons: [msg_option_1],
                cssClass: 'custom-alertInfo',
                backdropDismiss:false,
              });
              await alert_box.present();
            }
          }, async error => {
            this.pressed = false;
            let msg_title = "登入error";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Login error";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "登入error";
              msg_option_1 = "确定";
            }
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: msg_title + JSON.stringify(error),
              buttons: [msg_option_1],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            //console.log(error);
          });
      } else {
        this.pressed = false;
        if (this.data.email != "" && this.data.password != "") {
          if (this.login_hidden_message != "") {
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: this.login_hidden_message,
              buttons: ['OK'],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
          } else if (this.password_failed_message != "") {
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: this.password_failed_message,
              buttons: ['OK'],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
          }
        } else {
          this.pressed = false;
          let msg_title = "請填寫電子郵件和密碼";
          let msg_option_1 = "確定";
          if(this.global_service.lang == 'en'){
            msg_title = "Please fill in email and password";
            msg_option_1 = "Confirm";
          }
          if(this.global_service.lang == 'cn'){
            msg_title = "请填写电子邮件和密码";
            msg_option_1 = "确定";
          }
          const alert_box = await this.alert.create({
            // enableBackdropDismiss: false,
            message: msg_title,
            buttons: [msg_option_1],
            cssClass: 'custom-alertInfo',
            backdropDismiss:false,
          });
          await alert_box.present();
        }
      }
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\.,;:\s@"]+(\.[^<>()[\]\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  ValidatePassword(pw_string) {
    var regex_upper = new RegExp("[A-Z]+");
    var regex_lower = new RegExp("[a-z]+");
    var regex_numeric = new RegExp("[0-9]+");
    var regex_length = String(pw_string);
    var regex_space = /\s/;
    if (!(regex_upper.test(pw_string) || regex_lower.test(pw_string))) {
      //console.log("missing letters");
      if(this.global_service.lang == 'en'){
        this.password_failed_message = "Password require alphabets";
      }else if(this.global_service.lang == 'cn'){
        this.password_failed_message = "密码需要包含字母";
      }else{
        this.password_failed_message = "密碼需要包含字母";
      }
      return false;
    }
    if (!regex_numeric.test(pw_string)) {
      //console.log("missing number");
      if(this.global_service.lang == 'en'){
        this.password_failed_message = "Password require number";
      }else if(this.global_service.lang == 'cn'){
        this.password_failed_message = "密码需要包含数字";
      }else{
        this.password_failed_message = "密碼需要包含數字";
      }
      return false;
    }
    if (regex_space.test(pw_string)) {
      //console.log("contain space");
      if(this.global_service.lang == 'en'){
        this.password_failed_message = "Password not accept blank key";
      }else if(this.global_service.lang == 'cn'){
        this.password_failed_message = "密码不能包含空白键";
      }else{
        this.password_failed_message = "密碼不能包含空白鍵";
      }
      return false;
    }
    if (regex_length.length < 8) {
      if(this.global_service.lang == 'en'){
        this.password_failed_message = "Password not less than 8 alphabet";
      }else if(this.global_service.lang == 'cn'){
        this.password_failed_message = "密码不能少於8个字";
      }else{
        this.password_failed_message = "密碼不能少於8個字";
      }
      //console.log("more than 8 digits plz!");
      return false;
    }
    return true;
  }
  CheckPassword() {
    if (this.data.password != "") {
      if (this.ValidatePassword(this.data.password)) {
        this.password_failed_message = "";
        this.password_passed = true;
        this.password_normal = false;
        this.password_failed = false;
      } else {
        this.password_passed = false;
        this.password_normal = false;
        this.password_failed = true;
      }
    } else {
      this.password_failed_message = "";
      this.password_normal = true;
      this.password_passed = false;
      this.password_failed = false;
    }
  }
  async ConfirmPassword() {
    if (this.data.confirm_password != "") {
      if (this.data.password == "") {
        let msg_title = "請填寫密碼";
        let msg_option_1 = "確定";
        if(this.global_service.lang == 'en'){
          msg_title = "Please fill in password";
          msg_option_1 = "Confirm";
        }
        if(this.global_service.lang == 'cn'){
          msg_title = "请填写密码";
          msg_option_1 = "确定";
        }
        const alert_box = await this.alert.create({
          // enableBackdropDismiss: false,
          message: msg_title,
          buttons: [msg_option_1],
          cssClass: 'custom-alertInfo',
          backdropDismiss:false,
        });
        await alert_box.present();
        this.data.confirm_password = "";
        this.confirm_failed_message = "";
        // this.password_failed_message = "請先填寫密碼";
        if(this.global_service.lang == 'en'){
          this.password_failed_message = "Please fill in password first";
        }else if(this.global_service.lang == 'cn'){
          this.password_failed_message = "请先填写密码";
        }else{
          this.password_failed_message = "請先填寫密碼";
        }
        this.password_normal = false;
        this.password_passed = false;
        this.password_failed = true;
        this.confirm_normal = true;
        this.confirm_passed = false;
        this.confirm_failed = false;
      } else {
        if (this.password_passed) {
          if (this.data.password == this.data.confirm_password) {
            this.confirm_failed_message = "";
            this.confirm_normal = false;
            this.confirm_passed = true;
            this.confirm_failed = false;
          } else {
            if(this.global_service.lang == 'en'){
              this.confirm_failed_message = "Password not match, check again";
            }else if(this.global_service.lang == 'cn'){
              this.confirm_failed_message = "密码不同，请再次检查";
            }else{
              this.confirm_failed_message = "密碼不同，請再次檢查";
            }
            // this.confirm_failed_message = "密碼不同，請再次檢查";
            this.confirm_normal = false;
            this.confirm_passed = false;
            this.confirm_failed = true;
          }
        } else {
          let msg_title = "請填寫正確密碼";
          let msg_option_1 = "確定";
          if(this.global_service.lang == 'en'){
            msg_title = "Please fill in correct password";
            msg_option_1 = "Confirm";
          }
          if(this.global_service.lang == 'cn'){
            msg_title = "请填写正确密码";
            msg_option_1 = "确定";
          }
          const alert_box = await this.alert.create({
            // enableBackdropDismiss: false,
            message: msg_title,
            buttons: [msg_option_1],
            cssClass: 'custom-alertInfo',
            backdropDismiss:false,
          });
          await alert_box.present();
          this.confirm_failed_message = "";
          this.data.confirm_password = "";
        }

      }
    } else {
      this.confirm_failed_message = "";
      this.confirm_normal = true;
      this.confirm_passed = false;
      this.confirm_failed = false;
    }

  }
  CheckEmailExist() {
    if (this.data.email != "") {
      this.checked = true;
      if (this.validateEmail(this.data.email)) {
        this.hidden_message_show = false;
        this.hidden_message = "";
        let url = "https://intervii.com/backend/request.php";
        let body = { "check_email": this.data.email };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            this.global_service.dismiss();
            let temp = data.data;
            console.log(JSON.stringify(data));
            if (temp.includes("pass")) {
              this.hidden_message_show = false;
              this.hidden_message = "";
              //console.log("pass");
            } else if (temp == "user_occur") {
              this.hidden_message_show = true;
              this.hidden_message = "電子郵件地址已被使用";
              if(this.global_service.lang == 'en'){
                this.hidden_message = "Email has been used";
              }else if(this.global_service.lang == 'cn'){
                this.hidden_message = "电子邮件地址已被使用";
              }else{
                this.hidden_message = "電子郵件地址已被使用";
              }
              //console.log("user_occur")
            } else {
              this.hidden_message_show = true;
              // this.hidden_message = "請重新輸入電子郵件地址";
              if(this.global_service.lang == 'en'){
                this.hidden_message = "Password input email again";
              }else if(this.global_service.lang == 'cn'){
                this.hidden_message = "请重新输入电子邮件地址";
              }else{
                this.hidden_message = "請重新輸入電子郵件地址";
              }
            }
          }, error => {
            console.log(error);
          });
      } else {
        this.hidden_message_show = true;
        // this.hidden_message = "電子郵件地址格式不正確";
        if(this.global_service.lang == 'en'){
          this.hidden_message = "Email format incorrect";
        }else if(this.global_service.lang == 'cn'){
          this.hidden_message = "电子邮件地址格式不正确";
        }else{
          this.hidden_message = "電子郵件地址格式不正確";
        }
      }
    } else {
      this.hidden_message_show = false;
      this.hidden_message = "";
      this.checked = false;
    }
  }
  async Register() {
    if (!this.pressed) {
      this.pressed = true;
      if (this.hidden_message_show == false && this.password_failed_message == ""
        && this.data.email != "" && this.data.password != ""
        && this.confirm_failed_message == "" && this.data.confirm_password == this.data.password) {
        // this.alert.create({
        //   message: '確認注冊',
        //   cssClass: 'custom-alertInfo',
        //   buttons: [{ text: 'Cancel' }, {
        //     text: 'OK',
        //     handler: () => {
        // this.loading.present();
        let url = "https://intervii.com/backend/request.php";
        var json = {
          "email": this.data.email,
          "password": this.data.password,
          "notification_id": this.notification_id
        };
        let body = { "register": JSON.stringify(json) };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(async data => {
            this.global_service.dismiss();
            // this.loading.dismiss();
            //console.log(JSON.stringify(data.data));
            let temp = JSON.parse(data.data);
            if (temp.result == "fail") {
              this.pressed = false;
              const alert_box = await this.alert.create({
                message: temp.data,
                buttons: ['OK'],
                cssClass: 'custom-alertInfo',
                backdropDismiss:false,
              });
              await alert_box.present();
            } else if (temp.result == "success") {
              this.pressed = false;
              this.storage.set('user_id', temp.data.id);
              this.storage.set('user_data', temp.data);

              this.global_service.user_id = temp.data.id;
              this.global_service.user_data = temp.data;

              let url = "https://intervii.com/backend/request.php";
              var json2 = {
                "user_id": temp.data.id
              };
              let body2 = { "get_user_by_id": JSON.stringify(json2) };
              let headers2 = {
                'Content-Type': 'application/x-www-form-urlencoded'
              };
              this.global_service.isLoading = true;
              this.global_service.presentLoading();
              this.http.post(url, body2, headers2)
                .then(data => {
                  this.global_service.dismiss();
                  //console.log(JSON.stringify(data.data));
                  let temp2 = JSON.parse(data.data);
                  let profile = temp2.data;
                  // let modal2 = this.modalCtrl.create(EditProfilePage, { user_data: temp.data, profile: temp2.data, page: 'login' });
                  // modal2.present();
                  this.global_service.edit_profile_from = "login";
                  this.global_service.user_profile = profile;
                  this.global_service.resetLoading();
                  this.navCtrl.navigateForward(['/edit-profile']);
                });
            }
          }, error => {
            this.pressed = false;
            //console.log(error);
          });
      } else {
        if (this.data.email != "" && this.data.password != "") {
          this.pressed = false;
          if (this.hidden_message != "") {
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: this.hidden_message,
              buttons: ['OK'],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
          } else if (this.password_failed_message != "") {
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: this.password_failed_message,
              buttons: ['OK'],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
          } else if (this.data.confirm_password == "") {
            let msg_title = "請填寫確認密碼";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Please fill in confirm password";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "请填写确认密码";
              msg_option_1 = "确定";
            }
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: msg_title,
              buttons: [msg_option_1],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
            this.confirm_failed = true;
            this.confirm_passed = false;
            this.confirm_normal = false;
          } else if (this.confirm_failed_message != "") {
            const alert_box = await this.alert.create({
              // enableBackdropDismiss: false,
              message: this.confirm_failed_message,
              buttons: ['OK'],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
          }

        } else if (this.data.email == "" || this.data.password == "") {
          this.pressed = false;
          let msg_title = "請填寫電子郵件和密碼";
          let msg_option_1 = "確定";
          if(this.global_service.lang == 'en'){
            msg_title = "Please fill in email and password";
            msg_option_1 = "Confirm";
          }
          if(this.global_service.lang == 'cn'){
            msg_title = "请填写电子邮件和密码";
            msg_option_1 = "确定";
          }
          const alert_box = await this.alert.create({
            // enableBackdropDismiss: false,
            message: msg_title,
            buttons: [msg_option_1],
            cssClass: 'custom-alertInfo',
            backdropDismiss:false,
          });
          await alert_box.present();
        }
      }
      //console.log('hms' + this.hidden_message_show);
      //console.log('pfm' + this.password_failed_message);
    }
  }
  IsRegEmailNormal() {
    return !this.checked;
  }
  IsRegEmailFail() {
    let boo = false;
    if (this.hidden_message_show) {
      boo = true;
    }
    return boo;
  }
  IsRegEmailPass() {
    let boo = false;
    if (this.checked && !this.hidden_message_show) {
      boo = true;
    }
    return boo;
  }
  LoginEmailCheck() {
    if (this.data.email != "") {
      if (this.validateEmail(this.data.email)) {
        this.login_hidden_message = "";
        this.login_checked = true;
        this.login_pass = true;
      } else {
        if(this.global_service.lang == 'en'){
          this.login_hidden_message = "Email format incorrect";
        }else if(this.global_service.lang == 'cn'){
          this.login_hidden_message = "電子郵件地址格式不正確";
        }else{
          this.login_hidden_message = "電子郵件地址格式不正確";
        }
        this.login_checked = true;
        this.login_pass = false;
      }
    } else {
      this.login_hidden_message = "";
      this.login_checked = false;
      this.login_pass = false;
    }
  }
  IsLogEmailNormal() {
    return !this.login_checked;
  }
  IsLogEmailFail() {
    let boo = false;
    if (this.login_checked && !this.login_pass) {
      boo = true;
    }
    return boo;
  }
  IsLogEmailPass() {
    let boo = false;
    if (this.login_checked && this.login_pass) {
      boo = true;
    }
    return boo;
  }
  FacebookLogin() {
    //ios comment
    if (!this.pressed) {
      this.pressed = true;
      this.fb.login(['email'])
        .then(async (res: FacebookLoginResponse) => {
          this.pressed = false;
          //console.log('Logged into Facebook!', JSON.stringify(res));
          if (res.status == "connected") {
            // res.authResponse.accessToken/userID
            this.fb.api("/me?fields=name", []).then(async (res2: any) => {
              //console.log(JSON.stringify(res2));
              //res2.email
              //check login
              let url = "https://intervii.com/backend/request.php";
              let headers = {
                'Content-Type': 'application/x-www-form-urlencoded'
              };
              var json = { "password": res.authResponse.userID, "notification_id": this.notification_id };
              let body = { "app_login": JSON.stringify(json) };
              this.global_service.isLoading = true;
              this.global_service.presentLoading();
              this.http.post(url, body, headers)
                .then(async data => {
                  this.global_service.dismiss();
                  let temp = JSON.parse(data.data);
                  if (temp.result == "success") {
                    this.pressed = false;
                    this.storage.set('user_id', temp.data.id);
                    this.storage.set('user_data', temp.data);

                    this.global_service.user_id = temp.data.id;
                    this.global_service.user_data = temp.data;
                    // await alert_box.present();
        this.global_service.resetLoading();
                    this.navCtrl.navigateRoot(['/tabs/home']);
                  } else {
                    //check reg


                    var json2 = {
                      "password": res.authResponse.userID,
                      "notification_id": this.notification_id,
                      "display_name": res2.name
                    };
                    let body2 = { "app_register": JSON.stringify(json2) };
                    this.global_service.isLoading = true;
                    this.global_service.presentLoading();
                    this.http.post(url, body2, headers)
                      .then(async data => {
                        this.global_service.dismiss();
                        // this.loading.dismiss();
                        //console.log(JSON.stringify(data.data));
                        let temp = JSON.parse(data.data);
                        if (temp.result == "fail") {
                          this.pressed = false;
                          const alert_box = await this.alert.create({
                            message: temp.data,
                            buttons: ['OK'],
                            cssClass: 'custom-alertInfo',
                            backdropDismiss:false,
                          });
                          await alert_box.present();
                        } else if (temp.result == "success") {
                          this.pressed = false;
                          this.storage.set('user_id', temp.data.id);
                          this.storage.set('user_data', temp.data);

                          this.global_service.user_id = temp.data.id;
                          this.global_service.user_data = temp.data;

                          let url = "https://intervii.com/backend/request.php";
                          var json2 = {
                            "user_id": temp.data.id
                          };
                          let body2 = { "get_user_by_id": JSON.stringify(json2) };
                          let headers2 = {
                            'Content-Type': 'application/x-www-form-urlencoded'
                          };
                          this.global_service.isLoading = true;
                          this.global_service.presentLoading();
                          this.http.post(url, body2, headers2)
                            .then(data => {
                              this.global_service.dismiss();
                              //console.log(JSON.stringify(data.data));
                              let temp2 = JSON.parse(data.data);
                              let profile = temp2.data;
                              this.global_service.edit_profile_from = "login";
                              this.global_service.user_profile = profile;
                              this.global_service.resetLoading();
                              this.navCtrl.navigateForward(['/edit-profile']);
                            });
                        }
                      }, error => {
                        this.pressed = false;
                      });


                  }
                }, async error => {
                  //check reg


                  var json2 = {
                    "password": res.authResponse.userID,
                    "notification_id": this.notification_id,
                    "display_name": res2.name
                  };
                  let body2 = { "app_register": JSON.stringify(json2) };
                  let headers2 = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  };
                  this.global_service.isLoading = true;
                  this.global_service.presentLoading();
                  this.http.post(url, body2, headers)
                    .then(async data => {
                      this.global_service.dismiss();
                      // this.loading.dismiss();
                      //console.log(JSON.stringify(data.data));
                      let temp = JSON.parse(data.data);
                      if (temp.result == "fail") {
                        this.pressed = false;
                        const alert_box = await this.alert.create({
                          message: temp.data,
                          buttons: ['OK'],
                          cssClass: 'custom-alertInfo',
                          backdropDismiss:false,
                        });
                        await alert_box.present();
                      } else if (temp.result == "success") {
                        this.pressed = false;
                        this.storage.set('user_id', temp.data.id);
                        this.storage.set('user_data', temp.data);

                        this.global_service.user_id = temp.data.id;
                        this.global_service.user_data = temp.data;

                        let url = "https://intervii.com/backend/request.php";
                        var json2 = {
                          "user_id": temp.data.id
                        };
                        let body2 = { "get_user_by_id": JSON.stringify(json2) };
                        let headers2 = {
                          'Content-Type': 'application/x-www-form-urlencoded'
                        };
                        this.global_service.isLoading = true;
                        this.global_service.presentLoading();
                        this.http.post(url, body2, headers2)
                          .then(data => {
                            this.global_service.dismiss();
                            //console.log(JSON.stringify(data.data));
                            let temp2 = JSON.parse(data.data);
                            let profile = temp2.data;
                            this.global_service.edit_profile_from = "login";
                            this.global_service.user_profile = profile;
                            this.global_service.resetLoading();
                            this.navCtrl.navigateForward(['/edit-profile']);
                          });
                      }
                    }, error => {
                      this.pressed = false;
                    });
                });


            }).catch(async e => {
              // //console.log(JSON.stringify(e));
              //no email
              this.pressed = false;
              let msg_title = "Facebook 電郵獲取失敗";
              let msg_option_1 = "確定";
              if(this.global_service.lang == 'en'){
                msg_title = "Facebook email request fail";
                msg_option_1 = "Confirm";
              }
              if(this.global_service.lang == 'cn'){
                msg_title = "Facebook 电邮获取失败";
                msg_option_1 = "确定";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                buttons: [msg_option_1],
                cssClass: 'custom-alertInfo',
                backdropDismiss:false,
              });
              await alert_box.present();
            })
          } else {
            //connect fail
            this.pressed = false;
            let msg_title = "Facebook 連結失敗";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Facebook connect fail";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "Facebook 连结失败";
              msg_option_1 = "确定";
            }
            const alert_box = await this.alert.create({
              message: msg_title,
              buttons: [msg_option_1],
              cssClass: 'custom-alertInfo',
              backdropDismiss:false,
            });
            await alert_box.present();
          }
        })
        .catch(e => {
          this.pressed = false;
          //console.log('Error logging into Facebook', e);
        });

      this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
    }
  }
  swipeRight(event: any): any {
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  addFoucs() {
    this.focus_rate++;
  }

  async resuceFocus() {
    await this.delay(500);
    this.focus_rate--;
  }
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  Forget(){
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/forget-password']);
  }
}
