import { Component, ViewChild, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

import { StreamingMedia, StreamingAudioOptions } from '@ionic-native/streaming-media/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';


@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.page.html',
  styleUrls: ['./chatroom.page.scss'],
})
export class ChatroomPage implements OnInit {
  @ViewChild('mySelect', { static: true }) selectRef: any;
  @ViewChild('content_zone', { static: true }) content: any;
  chatroom: any = { user_id: -2, lecturer_id: -3 };
  temp_chatroom: any;
  user_data: any;
  refresher: any;
  reply_content: string = "";
  tag: string = "";
  tags: any;
  recording: boolean = false;
  filePathStr: string;
  fileName: string;
  audio: MediaObject;
  audio_data: string;
  alert_count:number = 0;
  selectOptions = {
    header: '選擇作業'       
  };
  constructor(
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public http: HTTP,
    // public navParams: NavParams, 
    private storage: Storage,
    public navCtrl: NavController,
    private socket: Socket,
    private photoViewer: PhotoViewer,
    public streamingMedia: StreamingMedia,
    public alertCtrl: AlertController,
    private nativeAudio: NativeAudio,
    private file: File,
    private filePath: FilePath,
    public platform: Platform,
    private media: Media,
    private diagnostic: Diagnostic
  ) {
    // this.chatroom = { user_id: -2, lecturer_id: -3 };
    // this.temp_chatroom = {};
    // this.tags = [];
    // this.user_data = { id: -1 };
    this.selectOptions = {
      header: '選擇作業'       
    };
    if(this.global_service.lang == 'en'){
      this.selectOptions = {
        header: 'Select Assignment'       
      };
    }
    if(this.global_service.lang == 'cn'){
      this.selectOptions = {
        header: '选择作业'       
      };
    }
  }

  ngOnInit() {
  }
  // MessageCome() {
  //   // alert("MessageCome");
  //   this.zone.run(() => {
  //     this.LoadData();
  //   });
  // }
  ionViewDidEnter() {
    this.global_service.socket_page = "room";
    this.ConnectToNJS(this.global_service.user_data.id);
    this.platform.resume.subscribe(async () => {
      this.ConnectToNJS(this.global_service.user_data.id);
    });
    this.diagnostic.getMicrophoneAuthorizationStatus().then((status) => {
      //console.log(`AuthorizationStatus`);
      //console.log(status);
      if (status != this.diagnostic.permissionStatus.GRANTED) {
        this.diagnostic.requestMicrophoneAuthorization().then((data) => {
          //console.log(`getCameraAuthorizationStatus`);
          //console.log(data);
        })
      } else {
        //console.log("We have the permission");
      }
    }, (statusError) => {
      //console.log(`statusError`);
      //console.log(statusError);
    });
    this.chatroom = this.global_service.chatroom;
    for (let i = 0; i < this.chatroom.chat_list.length; i++) {
      this.chatroom.chat_list[i].time = this.chatroom.chat_list[i].reply_time.substring(0, 16).replace('T',' ');
    }
    //console.log(this.chatroom);
    //console.log(this.global_service.user_data);
    // this.storage.get('user_data').then((val) => {
    //   if (val != undefined && val != null) {
    this.user_data = this.global_service.user_data;

    let url2 = "https://intervii.com/backend/request.php";
    let body2 = { "get_user_by_id": JSON.stringify({ "user_id": this.user_data.id }) };
    let headers2 = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url2, body2, headers2)
      .then(data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.user_data = temp.data;
          this.storage.set('user_data', this.user_data);
          this.LoadData();
        }
      });
    //   } else {
    //     this.user_data = {};
    //   }
    //   //console.log(this.user_data);
    // });
  }
  LoadData() {
    this.temp_chatroom = {};
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_chatroom_by_id": JSON.stringify({ "chatroom_id": this.chatroom.id }) };
    //let body = { "current_datetime_string" : ""};
    //console.log(body);
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          // this.zone.run(() => {
          this.temp_chatroom = temp.data;
          let course_ids = [];
          course_ids.push(this.temp_chatroom.course_id);
          this.LoadCourse(course_ids);
          //console.log(this.temp_chatroom.lecturer_id);
          //console.log(this.temp_chatroom.user_id);
          //console.log(this.user_data.id);
          if (this.temp_chatroom.lecturer_id == this.user_data.id) {
            if(this.temp_chatroom.lecturer_unread){
              this.global_service.unread_count--;
            }
            //console.log("lecturer_id");
            let body2 = { "lecturer_read": JSON.stringify({ "chatroom_id": this.temp_chatroom.id }) };
            let headers = {
              'Content-Type': 'application/x-www-form-urlencoded'
            };
            this.http.post(url, body2, headers)
              .then(data => {
                //console.log(data.data);
                let temp = JSON.parse(data.data);
                if (temp.result == "success") {
                } else {
                }
              });
          }
          if (this.temp_chatroom.user_id == this.user_data.id) {
            if(this.temp_chatroom.user_unread){
              this.global_service.unread_count--;
            }
            //console.log("user_id");
            let body3 = { "user_read": JSON.stringify({ "chatroom_id": this.temp_chatroom.id }) };
            let headers = {
              'Content-Type': 'application/x-www-form-urlencoded'
            };
            this.http.post(url, body3, headers)
              .then(data => {
                //console.log(data.data);
                let temp = JSON.parse(data.data);
                if (temp.result == "success") {
                } else {
                }
              });
          }
          // });
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  LoadCourse(course_ids) {
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_courses_by_ids": JSON.stringify({ "courses": course_ids }) };
    //let body = { "current_datetime_string" : ""};
    //console.log(body);
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          if (temp.data.length > 0) {
            this.temp_chatroom.course_data = temp.data[0];
            this.LoadTags(this.temp_chatroom.course_data);
          }
          if (this.temp_chatroom.user_id == this.user_data.id) {
            this.GetUserIcon(this.temp_chatroom.lecturer_id);
          }
          if (this.temp_chatroom.lecturer_id == this.user_data.id) {
            this.GetUserIcon(this.temp_chatroom.user_id);
          }
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  GetUserIcon(user_id) {
    let url2 = "https://intervii.com/backend/request.php";
    let body2 = { "get_user_by_id": JSON.stringify({ "user_id": user_id }) };
    //let body = { "current_datetime_string" : ""};
    let headers2 = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url2, body2, headers2)
      .then(data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          if (this.temp_chatroom.user_id == this.user_data.id) {
            this.temp_chatroom.lecturer_data = temp.data;
          }
          if (this.temp_chatroom.lecturer_id == this.user_data.id) {
            this.temp_chatroom.user_data = temp.data;
          }
          //console.log(this.temp_chatroom);
          for (let i = 0; i < this.temp_chatroom.chat_list.length; i++) {
            this.temp_chatroom.chat_list[i].time = this.temp_chatroom.chat_list[i].reply_time.substring(0, 16).replace('T',' ');
          }
          this.chatroom = this.temp_chatroom;
          this.content.resize();
          this.content.scrollToBottom();
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  LoadTags(data_package) {
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_assignments": JSON.stringify(data_package.assignment_list) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        if (JSON.parse(data.data).result == "success") {
          // let return_data = this.multi_unit;
          // this.viewCtrl.dismiss(return_data);
          let assignments = JSON.parse(data.data).data;
          let temp_tags = [];
          for (let i = 0; i < assignments.length; i++) {
            temp_tags.push(assignments[i].title);
          }
          this.tags = temp_tags;
        }
      });
  }
  Reload() {
    this.LoadData();
  }
  Send() {
    // 'user_id''chatroom_id''title''content''content_type''tag'
    let reply_data = {
      "user_id": this.user_data.id,
      "target_id": 0,
      "chatroom_id": this.chatroom.id,
      "title": "",
      "content": this.reply_content,
      "tag": this.tag,
      "content_type": "text"
    };



    let today = new Date();
    // let strDate = 'Y-m-dTh:n:sZ'
    //     .replace('Y', today.getFullYear().toString())
    //     .replace('m', (today.getMonth() >= 9 ? today.getMonth() + 1 : "0" + (today.getMonth() + 1)).toString())
    //     .replace('d', (today.getDate() > 9 ? today.getDate() : "0" + (today.getDate())).toString())
    //     .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
    //     .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString())
    //     .replace('s', (today.getSeconds() > 9 ? today.getSeconds() : "0" + (today.getSeconds())).toString());
    let strDate = 'Y-m-d h:n'
    .replace('Y', today.getFullYear().toString())
    .replace('m', (today.getMonth() >= 9 ? today.getMonth() + 1 : "0" + (today.getMonth() + 1)).toString())
    .replace('d', (today.getDate() > 9 ? today.getDate() : "0" + (today.getDate())).toString())
    .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
    .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString());
        // let strDate = 'h:n'
        //     .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
        //     .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString());
    let temp_reply_data =  {
      "user_id": this.user_data.id,
      "target_id": 0,
      "chatroom_id": this.chatroom.id,
      "title": "",
      "content": this.reply_content,
      "tag": this.tag,
      "content_type": "text",
      "time": strDate
    };
    //console.log(reply_data);

    this.reply_content = "";
    this.tag = "";
    if (reply_data.content != "") {
      reply_data.user_id = this.global_service.user_data.id;
      if (this.chatroom.lecturer_id == this.global_service.user_data.id) {
        reply_data.target_id = this.chatroom.user_id;
      } else {
        reply_data.target_id = this.chatroom.lecturer_id;
      }
      this.chatroom.chat_list.push(temp_reply_data);
      this.socket.emit('send-message', reply_data);
      // let url2 = "https://intervii.com/backend/request.php";
      // let body2 = { "reply_chat": JSON.stringify(reply_data) };
      // //let body = { "current_datetime_string" : ""};
      // let headers2 = {
      //   'Content-Type': 'application/x-www-form-urlencoded'
      // };
      // this.http.post(url2, body2, headers2)
      //   .then(data => {
      //     //console.log(data.data);
      //     let temp = JSON.parse(data.data);
      //     if (temp.result == "success") {
      //       this.LoadData();
      //       // this.content.scrollToBottom();
      //     } else {
      //     }
      //   });
    }
  }
  SendImg() {
    this.presentActionSheet();
  }
  async presentActionSheet() {
    let msg_title = "選擇來源";
    let msg_option_1 = "從手機相簿選擇";
    let msg_option_2 = "拍攝新照片";
    let msg_option_3 = "取消";
    if (this.global_service.lang == 'en') {
      msg_title = 'Select Image Source';
      msg_option_1 = "Camera roll";
      msg_option_2 = "Take photo";
      msg_option_3 = "Cancel";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = '选择来源';
      msg_option_1 = "从手机相簿选择";
      msg_option_2 = "拍摄新照片";
      msg_option_3 = "取消";
    }
    let actionSheet = await this.actionSheetCtrl.create({
      header: msg_title,
      buttons: [
        {
          text: msg_option_1,
          handler: () => {
            //console.log('Load from Library');
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: msg_option_2,
          handler: () => {
            //console.log('Load Camera');
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: msg_option_3,
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  takePicture(sourceType) {
    //console.log('TakePicture');
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      // saveToPhotoAlbum: false,
      // correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // allowEdit: true,
      // targetWidth: 500,
      // targetHeight: 500
    };

    this.camera.getPicture(options).then((imageData) => {
      // this.base64_data = 'data:image/jpeg;base64,' + imageData;
      let reply_data = {
        "user_id": this.user_data.id,
        "target_id": 0,
        "chatroom_id": this.chatroom.id,
        "title": "",
        "content": 'data:image/jpeg;base64,' + imageData,
        "tag": this.tag,
        "content_type": "image"
      };



      let today = new Date();
      // let strDate = 'Y-m-dTh:n:sZ'
      //     .replace('Y', today.getFullYear().toString())
      //     .replace('m', (today.getMonth() >= 9 ? today.getMonth() + 1 : "0" + (today.getMonth() + 1)).toString())
      //     .replace('d', (today.getDate() > 9 ? today.getDate() : "0" + (today.getDate())).toString())
      //     .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
      //     .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString())
      //     .replace('s', (today.getSeconds() > 9 ? today.getSeconds() : "0" + (today.getSeconds())).toString());
    let strDate = 'Y-m-d h:n'
    .replace('Y', today.getFullYear().toString())
    .replace('m', (today.getMonth() >= 9 ? today.getMonth() + 1 : "0" + (today.getMonth() + 1)).toString())
    .replace('d', (today.getDate() > 9 ? today.getDate() : "0" + (today.getDate())).toString())
    .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
    .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString());
          // let strDate = 'h:n'
          //     .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
          //     .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString());
      let temp_reply_data =  {
        "user_id": this.user_data.id,
        "target_id": 0,
        "chatroom_id": this.chatroom.id,
        "title": "",
        "content": 'template.jpg',//'data:image/jpeg;base64,' + imageData,
        "tag": this.tag,
        "content_type": "image",
        "time": strDate
      };
      //console.log(reply_data);

      this.reply_content = "";
      this.tag = "";
      if (reply_data.content != "") {
        reply_data.user_id = this.global_service.user_data.id;
        if (this.chatroom.lecturer_id == this.global_service.user_data.id) {
          reply_data.target_id = this.chatroom.user_id;
        } else {
          reply_data.target_id = this.chatroom.lecturer_id;
        }
        this.chatroom.chat_list.push(temp_reply_data);
        this.socket.emit('send-message', reply_data);
        // let url2 = "https://intervii.com/backend/request.php";
        // let body2 = { "reply_chat": JSON.stringify(reply_data) };
        // //let body = { "current_datetime_string" : ""};
        // let headers2 = {
        //   'Content-Type': 'application/x-www-form-urlencoded'
        // };
        // this.http.post(url2, body2, headers2)
        //   .then(data => {
        //     //console.log(data.data);
        //     let temp = JSON.parse(data.data);
        //     if (temp.result == "success") {
        //       this.LoadData();
        //       // this.content.scrollToBottom();
        //     } else {
        //     }
        //   });
      }
    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }
  SendRecord() {
    this.Record();
  }
  SelectTag() {
    this.selectRef.open();
  }
  ngAfterViewChecked() {
    this.content.scrollToBottom();
  }


  Record() {
    if (this.recording) {
      this.stopRecord();
    } else {
      this.startRecord();
    }
  }
  startRecord() {
    this.recording = true;
    if (this.platform.is('ios')) {
      this.fileName = 'temp_comment.wav';
      this.filePathStr = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePathStr);
    } else{//} if (this.platform.is('android')) {
      this.fileName = 'temp_comment.wav';
      this.filePathStr = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePathStr);
    }
    this.audio.startRecord();
  }
  async stopRecord() {
    this.recording = false;
    this.audio.stopRecord();
    if (this.platform.is('ios')) {
      this.file.readAsDataURL(this.file.documentsDirectory, this.fileName).then(async (contents) => {
        this.audio_data = contents;
        let msg_title = "確認上傳錄音？";
        let msg_option_1 = "確認";
        let msg_option_2 = "取消";
        if(this.global_service.lang == 'en'){
          msg_title = "Upload voice message?";
          msg_option_1 = "Confirm";
          msg_option_2 = "Cancel";
        }
        if(this.global_service.lang == 'cn'){
          msg_title = "确认上传录音？";
          msg_option_1 = "确认";
          msg_option_2 = "取消";
        }
        const alert = await this.alertCtrl.create({
          message: msg_title,
          backdropDismiss:false,
          buttons: [
            {
              text: msg_option_2,
              // role: 'cancel',
              handler: () => {
              }
            },
            {
              text: msg_option_1,
              // role: 'cancel',
              handler: () => {
                this.UploadAudio();
              }
            }
          ]
        });
        await alert.present();
      }).catch((error) => {
        //console.log("1cannot read");
      });
    } else{//} if (this.platform.is('android')) {
      this.file.readAsDataURL(this.file.externalDataDirectory, this.fileName).then(async (contents) => {
        this.audio_data = contents;
        // //console.log("2read file");
        // //console.log(contents.replace('data:audio/vnd.wave;base64,',""));
        // this.data_ready.audio_tutor_comment.data_string = contents;
        // //console.log(this.data_ready.audio_tutor_comment.data_string);
        // this.select_audio = "重新錄音";
        let msg_title = "確認上傳錄音？";
        let msg_option_1 = "確認";
        let msg_option_2 = "取消";
        if(this.global_service.lang == 'en'){
          msg_title = "Upload voice message?";
          msg_option_1 = "Confirm";
          msg_option_2 = "Cancel";
        }
        if(this.global_service.lang == 'cn'){
          msg_title = "确认上传录音？";
          msg_option_1 = "确认";
          msg_option_2 = "取消";
        }
        const alert = await this.alertCtrl.create({
          message: msg_title,
          backdropDismiss:false,
          buttons: [
            {
              text: msg_option_2,
              // role: 'cancel',
              handler: () => {
              }
            },
            {
              text: msg_option_1,
              // role: 'cancel',
              handler: () => {
                this.UploadAudio();
              }
            }
          ]
        });
        await alert.present();
      }).catch((error) => {
        //console.log("2cannot read");
      });
    }
  }
  UploadAudio() {
    let reply_data = {
      "user_id": this.user_data.id,
      "target_id": 0,
      "chatroom_id": this.chatroom.id,
      "title": "",
      "content": this.audio_data,
      "tag": this.tag,
      "content_type": "sound"
    };



    let today = new Date();
    // let strDate = 'Y-m-dTh:n:sZ'
    //     .replace('Y', today.getFullYear().toString())
    //     .replace('m', (today.getMonth() >= 9 ? today.getMonth() + 1 : "0" + (today.getMonth() + 1)).toString())
    //     .replace('d', (today.getDate() > 9 ? today.getDate() : "0" + (today.getDate())).toString())
    //     .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
    //     .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString())
    //     .replace('s', (today.getSeconds() > 9 ? today.getSeconds() : "0" + (today.getSeconds())).toString());
    let strDate = 'Y-m-d h:n'
        .replace('Y', today.getFullYear().toString())
        .replace('m', (today.getMonth() >= 9 ? today.getMonth() + 1 : "0" + (today.getMonth() + 1)).toString())
        .replace('d', (today.getDate() > 9 ? today.getDate() : "0" + (today.getDate())).toString())
        .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
        .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString());
        // let strDate = 'h:n'
        //     .replace('h', (today.getHours() > 9 ? today.getHours() : "0" + (today.getHours())).toString())
        //     .replace('n', (today.getMinutes() > 9 ? today.getMinutes() : "0" + (today.getMinutes())).toString());
            let msg_title = "錄音上傳中";
            if(this.global_service.lang == 'en'){
              msg_title = "Uploading";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "录音上传中";
            }
    let temp_reply_data =  {
      "user_id": this.user_data.id,
      "target_id": 0,
      "chatroom_id": this.chatroom.id,
      "title": "",
      "content": msg_title,
      "tag": this.tag,
      "content_type": "text",
      "time": strDate
    };
    //console.log(reply_data);
    this.reply_content = "";
    this.tag = "";
    if (reply_data.content != "") {
      reply_data.user_id = this.global_service.user_data.id;
      if (this.chatroom.lecturer_id == this.global_service.user_data.id) {
        reply_data.target_id = this.chatroom.user_id;
      } else {
        reply_data.target_id = this.chatroom.lecturer_id;
      }
      this.chatroom.chat_list.push(temp_reply_data);
      this.socket.emit('send-message', reply_data);
      // let url2 = "https://intervii.com/backend/request.php";
      // let body2 = { "reply_chat": JSON.stringify(reply_data) };
      // //let body = { "current_datetime_string" : ""};
      // let headers2 = {
      //   'Content-Type': 'application/x-www-form-urlencoded'
      // };
      // this.http.post(url2, body2, headers2)
      //   .then(data => {
      //     //console.log(data.data);
      //     let temp = JSON.parse(data.data);
      //     if (temp.result == "success") {
      //       this.LoadData();
      //       this.audio_data = "";
      //       // this.content.scrollToBottom();
      //     } else {
      //     }
      //   });
    }
  }
  ShowImage(name) {
    var options = {
      share: true, // default is false
      closeButton: true, // default is true
      copyToReference: true // default is false
    };
    this.photoViewer.show('https://intervii.com/media/' + name, '');
  }
  PlayRecord(name) {
    // this.nativeAudio.preloadSimple('audio1', 'https://intervii.com/media/'+name).then(()=>{this.nativeAudio.play('audio1');}, ()=>{});
    // this.nativeAudio.play('audio1');//.then(onSuccess, onError);
    let options: StreamingAudioOptions = {
      successCallback: () => { console.log('Sound played') },
      errorCallback: (e) => { console.log('Error streaming') },
      initFullscreen: false
      // orientation: 'landscape',
      // shouldAutoClose: true,
      // controls: true
    };

    this.streamingMedia.playAudio('https://intervii.com/media/' + name, options);

  }

  ConnectToNJS(name) {
    this.socket.connect();
      
    this.socket.on('disconnect', (reason) => {
      if (reason === 'io server disconnect') {
        this.socket.connect();
      }else{

        this.socket.connect();
      }
      // else the socket will automatically try to reconnect
    });

    this.socket.emit('reg_user', name);
    if (this.chatroom.lecturer_id == this.global_service.user_data.id) {
      let send_data = { is_lecturer: true, chat_id: this.chatroom.id };
      this.socket.emit('remove-unread', send_data);
    } else if (this.chatroom.user_id == this.global_service.user_data.id) {
      let send_data = { is_lecturer: false, chat_id: this.chatroom.id };
      this.socket.emit('remove-unread', send_data);
    }

    this.socket.fromEvent('message').subscribe(message => {
      this.Reload();
      console.log("message");
      console.log(message);
      if(this.alert_count <=0 && this.global_service.socket_page != "room"){
        this.alert_count++;
        if (this.global_service.socket_page == "room") {
          // this.LoadData();
          this.Reload();
          if (this.chatroom.lecturer_id == this.global_service.user_data.id) {
            let send_data = { is_lecturer: true, chat_id: this.chatroom.id };
            this.socket.emit('remove-unread', send_data);
          } else if (this.chatroom.user_id == this.global_service.user_data.id) {
            let send_data = { is_lecturer: false, chat_id: this.chatroom.id };
            this.socket.emit('remove-unread', send_data);
          }
        }
        setTimeout(function(){ this.alert_count--; }, 500);
      }
    });

    this.socket.fromEvent('my_message').subscribe(message => {
      this.Reload();
      console.log("my_message");
      console.log(message);
      if(this.alert_count <=0 && this.global_service.socket_page != "room"){
        this.alert_count++;
        if (this.global_service.socket_page == "room") {
          // this.LoadData();
          this.Reload();
          if (this.chatroom.lecturer_id == this.global_service.user_data.id) {
            let send_data = { is_lecturer: true, chat_id: this.chatroom.id };
            this.socket.emit('remove-unread', send_data);
          } else if (this.chatroom.user_id == this.global_service.user_data.id) {
            let send_data = { is_lecturer: false, chat_id: this.chatroom.id };
            this.socket.emit('remove-unread', send_data);
          }
        }
        setTimeout(function(){ this.alert_count--; }, 500);
      }
    });
  }

  GoBack() {
    //console.log('Disconnect socket');
    this.global_service.socket_page = "list";
    this.global_service.chatroom_back = true;
    // this.socket.disconnect();
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
}
