import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { HTTP } from "@ionic-native/http/ngx";
import { Storage } from "@ionic/storage";
import { GlobalService } from "../global.service";
import { DomSanitizer } from '@angular/platform-browser';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal/ngx";
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';


@Component({
  selector: 'app-search-user',
  templateUrl: './search-user.page.html',
  styleUrls: ['./search-user.page.scss'],
})
export class SearchUserPage implements OnInit {
  search_name: string = "";
  found_user = [];
  course_data: any = {};
  pressed: boolean = false;
  constructor(
    // private iap: InAppPurchase,
    private iap: InAppPurchase,
    public platform: Platform,
    private storage: Storage,
    public http: HTTP,
    public navCtrl: NavController,
    private streamingMedia: StreamingMedia,
    // public viewCtrl: ViewController,
    // public app: App,
    // public navParams: NavParams,
    private payPal: PayPal,
    private alertCtrl: AlertController,
    public global_service: GlobalService,
    private sanitizer: DomSanitizer,
  ) { }
  ngOnInit() {
  }

  async SelectUser(user_data) {
    //console.log(user_data);
    if (!this.pressed) {
      this.pressed = true;
      let msg_title = "確定把";
      let msg_title_2 = "加入到課堂中？";
      let msg_option_1 = "確定";
      let msg_option_2 = "取消";
      if(this.global_service.lang == 'en'){
        msg_title = "Confirm add ";
        msg_title_2 = " to the course?";
        msg_option_1 = "Confirm";
        msg_option_2 = "Cancel";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "确定把";
        msg_title_2 = "加入到课堂中？";
        msg_option_1 = "确定";
        msg_option_2 = "取消";
      }
      let alert = await this.alertCtrl.create({
        message: msg_title + user_data.display_name + msg_title_2,
        backdropDismiss:false,
        buttons: [
          {
            text: msg_option_2,
            // role: 'cancel',
            handler: () => {
              this.pressed = false;
            }
          },
          {
            text: msg_option_1,
            // role: 'cancel',
            handler: () => {
              let url = "https://intervii.com/backend/request.php";
              var json = { course_id: this.course_data.id, user_id: user_data.id };
              let body = { join_class: JSON.stringify(json) };
              let headers = {
                "Content-Type": "application/x-www-form-urlencoded"
              };
              this.http.post(url, body, headers).then(async data => {
                let temp = JSON.parse(data.data);
                if (temp.result == "success") {

                  let msg_title = "加入成功";
                  let msg_option_1 = "確定";
                  if(this.global_service.lang == 'en'){
                    msg_title = "Add success";
                    msg_option_1 = "Confirm";
                  }
                  if(this.global_service.lang == 'cn'){
                    msg_title = "加入成功";
                    msg_option_1 = "确定";
                  }
                  let alert2 = await this.alertCtrl.create({
                    message: msg_title,
                    backdropDismiss:false,
                    buttons: [{
                      text: msg_option_1,
                      handler: () => {
                        this.global_service.resetLoading();
                        this.navCtrl.pop();
                      }
                    }
                    ]
                  });
                  await alert2.present();
                } else {
                  let msg_title = "加入失敗";
                  let msg_option_1 = "確定";
                  if(this.global_service.lang == 'en'){
                    msg_title = "Add fail";
                    msg_option_1 = "Confirm";
                  }
                  if(this.global_service.lang == 'cn'){
                    msg_title = "加入失败";
                    msg_option_1 = "确定";
                  }
                  let alert2 = await this.alertCtrl.create({
                    message: msg_title,
                    backdropDismiss:false,
                    buttons: [msg_option_1]
                  });
                  await alert2.present();
                }
                this.pressed = false;
              });
            }
          }
        ]
      });
      await alert.present();
    }
  }

  SearchUser() {
    //console.log("search");
    //console.log(this.search_name);
    if (this.search_name != "") {
      let url = "https://intervii.com/backend/request.php";
      var json = { name: this.search_name };
      let body = { find_user: JSON.stringify(json) };
      let headers = {
        "Content-Type": "application/x-www-form-urlencoded"
      };
      this.http.post(url, body, headers).then(async data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.found_user = temp.data;
        }
      });
    }
  }


  ionViewDidEnter() {
    this.course_data = this.global_service.course_data;
  }
}
