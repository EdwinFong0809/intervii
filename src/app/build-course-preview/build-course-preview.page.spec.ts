import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCoursePreviewPage } from './build-course-preview.page';

describe('BuildCoursePreviewPage', () => {
  let component: BuildCoursePreviewPage;
  let fixture: ComponentFixture<BuildCoursePreviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCoursePreviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCoursePreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
