import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCoursePreviewPage } from './build-course-preview.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCoursePreviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCoursePreviewPageRoutingModule {}
