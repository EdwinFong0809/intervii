import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VlogDetailPageRoutingModule } from './vlog-detail-routing.module';

import { VlogDetailPage } from './vlog-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VlogDetailPageRoutingModule
  ],
  declarations: [VlogDetailPage]
})
export class VlogDetailPageModule {}
