import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VlogDetailPage } from './vlog-detail.page';

const routes: Routes = [
  {
    path: '',
    component: VlogDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VlogDetailPageRoutingModule {}
