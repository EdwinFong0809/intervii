import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VlogDetailPage } from './vlog-detail.page';

describe('VlogDetailPage', () => {
  let component: VlogDetailPage;
  let fixture: ComponentFixture<VlogDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VlogDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VlogDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
