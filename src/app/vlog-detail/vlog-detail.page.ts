import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from "../global.service";
import { EventsService } from '../events.service';
// import Player from '@vimeo/player';

@Component({
  selector: 'app-vlog-detail',
  templateUrl: './vlog-detail.page.html',
  styleUrls: ['./vlog-detail.page.scss'],
})
export class VlogDetailPage implements OnInit {
  vlog:any;
  logined: boolean = false;
  vlog_comment: any;
  press: boolean = false;
  reply_comment: string = "";
  constructor(
    public storage: Storage,
    public http: HTTP,
    private alert: AlertController,
    private sanitizer: DomSanitizer,
    // public events: Events, 
    public modalCtrl: ModalController, 
    private streamingMedia: StreamingMedia,
    // public player:VideoPlayer,
    public plt: Platform,
    // public app: App, 
    public navCtrl: NavController, 
    public global_service: GlobalService,
    public events: EventsService,
    // public navParams: NavParams,
    // private route: ActivatedRoute,
    // private router: Router
    ) {
      if (this.global_service.user_data != undefined && this.global_service.user_data != null) {
        this.logined = true;
      }
      this.vlog = this.global_service.vlog_data;
      this.vlog.content = this.vlog.content;//this.sanitizer.bypassSecurityTrustHtml(this.vlog.content);
      this.LoadVlogComment();

      this.events.getObservable().subscribe((data) => {
        console.log(data);
        if(data.data == "update_vlog"){
          this.events.publishSomeData({data:"refresh_vlog"});
          this.global_service.resetLoading();
          this.navCtrl.pop();
        }
      });
    }

  ngOnInit() {
  }

  ionViewWillEnter() {
  }

  LoadVlogComment(){
    let url = "https://intervii.com/backend/request.php";
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var json = { "vlog_id": this.vlog.id};
    let body2 = { "get_vlog_comments": JSON.stringify(json) };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body2, headers)
        .then(data => {
          this.global_service.dismiss();
          //console.log(data.data);
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
                let temp_vlog_comment = JSON.parse(data.data).data;

                let ids = [];

                for (let i = 0; i < temp_vlog_comment.length; i++) {
                  temp_vlog_comment[i].display_name = '';
                  temp_vlog_comment[i].user_icon = '';
                  temp_vlog_comment[i].question_time = '';
                  temp_vlog_comment[i].content = temp_vlog_comment[i].content.replace(/\n/g, '<br \/>');
                  ids.push(temp_vlog_comment[i].user_id);
                }
                this.vlog_comment = temp_vlog_comment;

                let body3 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": ids }) };
                this.http.post(url, body3, headers)
                  .then(data => {
                    // this.global_service.dismiss();
                    let temp3 = JSON.parse(data.data);
                    //console.log(data.data);
                    if (temp3.result == "success") {
                      let id_profile = [];
                      for (let i = 0; i < temp3.data.length; i++) {
                        id_profile[temp3.data[i].id] = temp3.data[i];
                      }
                      for (let i = 0; i < this.vlog_comment.length; i++) {
                        this.vlog_comment[i].display_name = id_profile[this.vlog_comment[i].user_id].display_name;
                        this.vlog_comment[i].user_icon = id_profile[this.vlog_comment[i].user_id].icon;
                        this.vlog_comment[i].question_time = this.vlog_comment[i].create_date.replace('T'," ").replace('Z',"");
                      }
                    }
                  });
          }
        });
  }
  
  GoProfile(id) {
    // if (this.logined) {
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      this.global_service.view_user_id = id;
      this.global_service.back_update = true;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    // }
  }
  async Play(){
    if(this.vlog.file_vimeo != undefined && this.vlog.file_vimeo != null && this.vlog.file_vimeo != ""){
      this.global_service.playVimeo(this.vlog.file_vimeo);
    }else{
      this.global_service.playURL('https://intervii.com/media/'+this.vlog.video);
    }
  }
  async Send() {
    let msg_title = "請勿留空";
    let msg_option_1 = "確定";
    if(this.global_service.lang == 'en'){
      msg_title = "Please don't leave blank";
      msg_option_1 = "Confirm";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "请勿留空";
      msg_option_1 = "确定";
    }

    if (!this.press) {
      this.press = true;
      if (this.reply_comment == "") {
        const alert_box = await this.alert.create({
          message: msg_title,
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
        this.press = false;
      } else {
        let url = "https://intervii.com/backend/request.php";
        var json = { "vlog_id": this.vlog.id, "user_id": this.global_service.user_data.id, "title": "", "content": this.reply_comment };
        let body = { "new_vlog_comment": JSON.stringify(json) };
        //console.log(json);
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(async data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              msg_title = "提交成功";
              if(this.global_service.lang == 'en'){
                msg_title = "Success";
              }
              if(this.global_service.lang == 'cn'){
                msg_title = "提交成功";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [msg_option_1]
              });
              await alert_box.present();
              this.reply_comment = "";
              this.LoadVlogComment();
            } else {
              msg_title = "提交失敗";
              if(this.global_service.lang == 'en'){
                msg_title = "Fail";
              }
              if(this.global_service.lang == 'cn'){
                msg_title = "提交失败";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [msg_option_1]
              });
              await alert_box.present();
            }
            this.press = false;
          });
      }
    }
  }


  Pop() {
    this.global_service.vlog_data = null;
    // this.navCtrl.navigateBack('/tabs/tab4');
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  async EditVlog() {
    if(this.global_service.user_data != undefined && this.global_service.user_data!=null && this.global_service.user_data!='' && this.global_service.user_data.is_lecturer!=undefined && this.global_service.user_data.is_lecturer!=null && (this.global_service.user_data.is_lecturer==true || this.global_service.user_data.is_lecturer=='true')){
      this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/edit-vlog']);
    }else{
      let msg_title = "目前用戶類型不能上傳Vlog";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Current user type cannot upload Vlog";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "目前用户类型不能上传Vlog";
        msg_option_1 = "确定";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }
  }
}
