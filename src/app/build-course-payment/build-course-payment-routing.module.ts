import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCoursePaymentPage } from './build-course-payment.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCoursePaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCoursePaymentPageRoutingModule {}
