import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCoursePaymentPage } from './build-course-payment.page';

describe('BuildCoursePaymentPage', () => {
  let component: BuildCoursePaymentPage;
  let fixture: ComponentFixture<BuildCoursePaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCoursePaymentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCoursePaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
