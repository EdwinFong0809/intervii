import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCoursePaymentPageRoutingModule } from './build-course-payment-routing.module';

import { BuildCoursePaymentPage } from './build-course-payment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCoursePaymentPageRoutingModule
  ],
  declarations: [BuildCoursePaymentPage]
})
export class BuildCoursePaymentPageModule {}
