import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'add-coursework',
    loadChildren: () => import('./add-coursework/add-coursework.module').then( m => m.AddCourseworkPageModule)
  },
  {
    path: 'add-question',
    loadChildren: () => import('./add-question/add-question.module').then( m => m.AddQuestionPageModule)
  },
  {
    path: 'add-review',
    loadChildren: () => import('./add-review/add-review.module').then( m => m.AddReviewPageModule)
  },
  {
    path: 'add-vlog',
    loadChildren: () => import('./add-vlog/add-vlog.module').then( m => m.AddVlogPageModule)
  },
  {
    path: 'build-course',
    loadChildren: () => import('./build-course/build-course.module').then( m => m.BuildCoursePageModule)
  },
  {
    path: 'build-course-content',
    loadChildren: () => import('./build-course-content/build-course-content.module').then( m => m.BuildCourseContentPageModule)
  },
  {
    path: 'build-course-coursework',
    loadChildren: () => import('./build-course-coursework/build-course-coursework.module').then( m => m.BuildCourseCourseworkPageModule)
  },
  {
    path: 'build-course-finish',
    loadChildren: () => import('./build-course-finish/build-course-finish.module').then( m => m.BuildCourseFinishPageModule)
  },
  {
    path: 'build-course-index',
    loadChildren: () => import('./build-course-index/build-course-index.module').then( m => m.BuildCourseIndexPageModule)
  },
  {
    path: 'build-course-payment',
    loadChildren: () => import('./build-course-payment/build-course-payment.module').then( m => m.BuildCoursePaymentPageModule)
  },
  {
    path: 'build-course-preview',
    loadChildren: () => import('./build-course-preview/build-course-preview.module').then( m => m.BuildCoursePreviewPageModule)
  },
  {
    path: 'build-course-price',
    loadChildren: () => import('./build-course-price/build-course-price.module').then( m => m.BuildCoursePricePageModule)
  },
  {
    path: 'build-course-target',
    loadChildren: () => import('./build-course-target/build-course-target.module').then( m => m.BuildCourseTargetPageModule)
  },
  {
    path: 'build-course-title',
    loadChildren: () => import('./build-course-title/build-course-title.module').then( m => m.BuildCourseTitlePageModule)
  },
  {
    path: 'build-course-unit',
    loadChildren: () => import('./build-course-unit/build-course-unit.module').then( m => m.BuildCourseUnitPageModule)
  },
  {
    path: 'build-course-unit-single',
    loadChildren: () => import('./build-course-unit-single/build-course-unit-single.module').then( m => m.BuildCourseUnitSinglePageModule)
  },
  {
    path: 'build-course-url',
    loadChildren: () => import('./build-course-url/build-course-url.module').then( m => m.BuildCourseUrlPageModule)
  },
  {
    path: 'chatlist',
    loadChildren: () => import('./chatlist/chatlist.module').then( m => m.ChatlistPageModule)
  },
  {
    path: 'chatroom',
    loadChildren: () => import('./chatroom/chatroom.module').then( m => m.ChatroomPageModule)
  },
  {
    path: 'course-comment-detail',
    loadChildren: () => import('./course-comment-detail/course-comment-detail.module').then( m => m.CourseCommentDetailPageModule)
  },
  {
    path: 'course-coursework-detail',
    loadChildren: () => import('./course-coursework-detail/course-coursework-detail.module').then( m => m.CourseCourseworkDetailPageModule)
  },
  {
    path: 'course-detail',
    loadChildren: () => import('./course-detail/course-detail.module').then( m => m.CourseDetailPageModule)
  },
  {
    path: 'course-list',
    loadChildren: () => import('./course-list/course-list.module').then( m => m.CourseListPageModule)
  },
  {
    path: 'course-question-detail',
    loadChildren: () => import('./course-question-detail/course-question-detail.module').then( m => m.CourseQuestionDetailPageModule)
  },
  {
    path: 'course-result-detail',
    loadChildren: () => import('./course-result-detail/course-result-detail.module').then( m => m.CourseResultDetailPageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'editor-page',
    loadChildren: () => import('./editor-page/editor-page.module').then( m => m.EditorPagePageModule)
  },
  {
    path: 'editor-preview',
    loadChildren: () => import('./editor-preview/editor-preview.module').then( m => m.EditorPreviewPageModule)
  },
  {
    path: 'follow-tutor',
    loadChildren: () => import('./follow-tutor/follow-tutor.module').then( m => m.FollowTutorPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'my-course',
    loadChildren: () => import('./my-course/my-course.module').then( m => m.MyCoursePageModule)
  },
  {
    path: 'myaccount',
    loadChildren: () => import('./myaccount/myaccount.module').then( m => m.MyaccountPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./setting/setting.module').then( m => m.SettingPageModule)
  },
  {
    path: 'vlog',
    loadChildren: () => import('./vlog/vlog.module').then( m => m.VlogPageModule)
  },
  {
    path: 'vlog-detail',
    loadChildren: () => import('./vlog-detail/vlog-detail.module').then( m => m.VlogDetailPageModule)
  },
  {
    path: 'search-user',
    loadChildren: () => import('./search-user/search-user.module').then( m => m.SearchUserPageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./privacy/privacy.module').then( m => m.PrivacyPageModule)
  },
  {
    path: 'forget-password',
    loadChildren: () => import('./forget-password/forget-password.module').then( m => m.ForgetPasswordPageModule)
  },
  {
    path: 'forget-password',
    loadChildren: () => import('./forget-password/forget-password.module').then( m => m.ForgetPasswordPageModule)
  },
  {
    path: 'edit-vlog',
    loadChildren: () => import('./edit-vlog/edit-vlog.module').then( m => m.EditVlogPageModule)
  },
  {
    path: 'econ-info',
    loadChildren: () => import('./econ-info/econ-info.module').then( m => m.EconInfoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
