import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-course-result-detail',
  templateUrl: './course-result-detail.page.html',
  styleUrls: ['./course-result-detail.page.scss'],
})
export class CourseResultDetailPage implements OnInit {
  assignment_data: any;
  course_detail: any;
  press: boolean = false;
  reply_comment: string;
  user_data: any;
  user_id: any;
  lecturer_data:any;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    ) {
      this.course_detail = this.global_service.course_data;
      this.assignment_data = this.global_service.assignment_data;
    }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.course_detail = this.global_service.course_data;
    this.assignment_data = this.global_service.assignment_data;
    this.user_data = this.global_service.user_data;
    this.user_id = this.global_service.user_data.id;
    this.lecturer_data = {};

    this.LoadData();
  }

  LoadData() {
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_assignment_record_by_id": JSON.stringify({ "assignment_id": this.assignment_data.id }) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        //console.log(data.data);
        if (temp.result == "success") {
          temp.data.display_name = '';
          temp.data.user_icon = '';
          temp.data.hide = false;
          temp.data.content = temp.data.content.replace(/\n/g, '<br \/>');
          this.assignment_data = temp.data;

          let body2 = { "get_user_by_id": JSON.stringify({ "user_id": this.assignment_data.user_id }) };
          this.global_service.isLoading = true;
          this.global_service.presentLoading();
          this.http.post(url, body2, headers)
            .then(data => {
              this.global_service.dismiss();
              let temp2 = JSON.parse(data.data);
              // //console.log(data.data);
              if (temp2.result == "success") {
                this.assignment_data.display_name = temp2.data.display_name;
                this.assignment_data.user_icon = temp2.data.icon;
              }

            });
            if(this.assignment_data.tutor_comment != ''){

              let body3 = { "get_user_by_id": JSON.stringify({ "user_id": this.assignment_data.lecturer_id }) };
              this.global_service.isLoading = true;
              this.global_service.presentLoading();
              this.http.post(url, body3, headers)
                .then(data => {
                  this.global_service.dismiss();
                  let temp3 = JSON.parse(data.data);
                  // //console.log(data.data);
                  if (temp3.result == "success") {
                    this.lecturer_data = temp3.data;
                  }
                });
            }
        }
      });
  }
  async Send() {
    if (!this.press) {
      this.press = true;
      if (this.reply_comment == "") {
        let msg_title = "請勿留空";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Please fill in all";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "请勿留空";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title,
          backdropDismiss:false,
          buttons: [{ text: msg_option_1 }]
        });
        await alert_box.present();
        this.press = false;
      } else {
        let url = "https://intervii.com/backend/request.php";
        var json = { "assignment_id": this.assignment_data.id, "tutor_comment": this.reply_comment };
        let body = { "reply_assignment": JSON.stringify(json) };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(async data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              let msg_title = "提交成功";
              let msg_option_1 = "確認";
              if (this.global_service.lang == 'en') {
                msg_title = "Success";
                msg_option_1 = "Confirm";
              }
              if (this.global_service.lang == 'cn') {
                msg_title = "提交成功";
                msg_option_1 = "确认";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [{ text: msg_option_1 }]
              });
              await alert_box.present();
              this.reply_comment = "";
              this.LoadData();
            } else {
              let msg_title = "提交失敗";
              let msg_option_1 = "確認";
              if (this.global_service.lang == 'en') {
                msg_title = "Fail";
                msg_option_1 = "Confirm";
              }
              if (this.global_service.lang == 'cn') {
                msg_title = "提交失败";
                msg_option_1 = "确认";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [{ text: msg_option_1 }]
              });
              await alert_box.present();
            }
            this.press = false;
          });
      }
    }
  }

  GoBack() {
    // this.viewCtrl.dismiss();
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }

}
