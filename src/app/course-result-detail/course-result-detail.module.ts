import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourseResultDetailPageRoutingModule } from './course-result-detail-routing.module';

import { CourseResultDetailPage } from './course-result-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourseResultDetailPageRoutingModule
  ],
  declarations: [CourseResultDetailPage]
})
export class CourseResultDetailPageModule {}
