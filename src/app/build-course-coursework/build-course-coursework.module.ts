import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseCourseworkPageRoutingModule } from './build-course-coursework-routing.module';

import { BuildCourseCourseworkPage } from './build-course-coursework.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseCourseworkPageRoutingModule
  ],
  declarations: [BuildCourseCourseworkPage]
})
export class BuildCourseCourseworkPageModule {}
