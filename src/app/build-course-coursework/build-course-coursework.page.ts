import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';

@Component({
  selector: 'app-build-course-coursework',
  templateUrl: './build-course-coursework.page.html',
  styleUrls: ['./build-course-coursework.page.scss'],
})
export class BuildCourseCourseworkPage implements OnInit {
  multi_coursework: any;
  number_of_coursework: any = 0;
  coursework_count: any = 0;
  course: any;
  course_id: any;
  user_id: any;
  success_array: any = [];
  loading: any;

  current_index: any = 0;

  uploading: boolean = false;
  @ViewChild('pwa_upload', { static: false }) pwa_upload: ElementRef;
  @ViewChild('pwa_upload_EXTRA', { static: false }) pwa_upload_EXTRA: ElementRef;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private camera: Camera,
    private file: File,
    private filePath: FilePath,
    // private transfer: FileTransfer,
    public actionSheetCtrl: ActionSheetController,
    private chooser: Chooser,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    //console.log('ionViewDidLoad BuildyourcourseCreateCourseworkPage');
    this.multi_coursework = [
    ];
    this.course_id = this.global_service.create_course.id;
    this.success_array = [];
    this.course = this.global_service.create_course;
    //console.log(this.course);
    this.user_id = this.global_service.user_id;

    if (this.global_service.get_coursework) {
      this.global_service.get_coursework = false;
      let url = "https://intervii.com/backend/request.php";
      let body = { "get_assignments_all_status": JSON.stringify(this.course.assignment_list) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          //console.log(data.data);
          if (JSON.parse(data.data).result == "success") {
            // let return_data = this.multi_unit;
            // this.viewCtrl.dismiss(return_data);
            let assignments = JSON.parse(data.data).data;
            //console.log(assignments);
            for (let i = 0; i < assignments.length; i++) {
              //console.log(assignments[i]);
              let temp = {
                coursework_title: assignments[i].title,
                content: assignments[i].detail,
                id: assignments[i].id,
                file_name: "",
                file_data: "",
                file_type: "",
                extra_file_name: ""
              };
              if (assignments[i].file_name != undefined && assignments[i].file_name != null && assignments[i].file_name != "") {
                temp.file_name = assignments[i].file_name;
              }
              if (assignments[i].extra_file_name != undefined && assignments[i].extra_file_name != null && assignments[i].extra_file_name != "") {
                temp.extra_file_name = assignments[i].extra_file_name;
              }
              this.multi_coursework.push(temp);
            }
            this.number_of_coursework = this.multi_coursework.length;
            this.coursework_count = 0;
            //console.log(this.multi_coursework);
          }
          this.global_service.dismiss();
        });
    } else {
      if (this.global_service.back_update) {
        this.global_service.back_update = false;
        this.multi_coursework = this.global_service.edit_coursework;
        this.multi_coursework[this.global_service.edit_coursework_index].content = this.global_service.editor_text;
      } else {
        this.multi_coursework = this.global_service.edit_coursework;
      }
    }
  }
  async Dismiss() {
    let is_data_pass = true;
    for (let i = 0; i < this.multi_coursework.length; i++) {
      if (this.multi_coursework[i].coursework_title == "" || this.multi_coursework[i].content == "") {
        let msg_title = "請輸入第";
        let msg_title_2 = "項內容";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Please input the ";
          msg_title_2 = " content";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "请输入第";
          msg_title_2 = "项内容";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title + (i + 1) + msg_title_2,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
        is_data_pass = false;
        break;
      }
    }
    //console.log(is_data_pass);
    //console.log(this.multi_coursework.length);
    if (this.uploading == false && is_data_pass) {
      if (this.multi_coursework.length > 0) {
        this.uploading = true;
        this.number_of_coursework = this.multi_coursework.length;
        this.coursework_count = 0;
        this.UploadData();
      } else {
        this.UpdateFinalList();
      }
    }
    //let return_data = [ this.data_ready.new_title,this.data_ready.new_description];
    //  this.viewCtrl.dismiss(return_data);
  }
  EnterContent(index) {
    //not ready
    this.global_service.edit_coursework = this.multi_coursework;
    this.global_service.edit_coursework_index = index;
    this.global_service.editor_text = this.multi_coursework[index].content;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/editor-page']);
    // let modal2 = this.modalCtrl.create(BuildyourcourseCreateCourseworkInputPage, { coursework: this.multi_coursework, index: index });
    // modal2.onDidDismiss(data => {
    //   //console.log(data);
    //   if (data != undefined) {
    //   }

    // })
    // modal2.present();
  }
  UploadData() {
    let url = "https://intervii.com/backend/request.php";
    let json = {};
    if (this.multi_coursework[this.coursework_count].extra_file_type != undefined && this.multi_coursework[this.coursework_count].extra_file_type != null && this.multi_coursework[this.coursework_count].extra_file_type != ""
      && this.multi_coursework[this.coursework_count].extra_file_name != undefined && this.multi_coursework[this.coursework_count].extra_file_name != null && this.multi_coursework[this.coursework_count].extra_file_name != ""
      && this.multi_coursework[this.coursework_count].extra_file_data != undefined && this.multi_coursework[this.coursework_count].extra_file_data != null && this.multi_coursework[this.coursework_count].extra_file_data != ""
    ) {
      json = {
        "id": this.multi_coursework[this.coursework_count].id, // course_id was not use in new_assignment accroding to the design
        "user_id": this.user_id,
        "title": this.multi_coursework[this.coursework_count].coursework_title,
        "detail": this.multi_coursework[this.coursework_count].content,//EnterContent result
        "file_type": this.multi_coursework[this.coursework_count].file_type,//Upload result type
        "file_name": this.multi_coursework[this.coursework_count].file_name,//No upload function
        "file_data": this.multi_coursework[this.coursework_count].file_data,//No upload fuction
        "extra_file_type": this.multi_coursework[this.coursework_count].extra_file_type,//Upload result type
        "extra_file_name": this.multi_coursework[this.coursework_count].extra_file_name,//No upload function
        "extra_file_data": this.multi_coursework[this.coursework_count].extra_file_data//No upload fuction
      };
    } else {
      json = {
        "id": this.multi_coursework[this.coursework_count].id, // course_id was not use in new_assignment accroding to the design
        "user_id": this.user_id,
        "title": this.multi_coursework[this.coursework_count].coursework_title,
        "detail": this.multi_coursework[this.coursework_count].content,//EnterContent result
        "file_type": this.multi_coursework[this.coursework_count].file_type,//Upload result type
        "file_name": this.multi_coursework[this.coursework_count].file_name,//No upload function
        "file_data": this.multi_coursework[this.coursework_count].file_data//No upload fuction
      };
    }
    let body;
    //console.log(json);
    if (this.multi_coursework[this.coursework_count].id > -1) {
      body = { "update_assignment": JSON.stringify(json) };
    } else {
      body = { "new_assignment": JSON.stringify(json) };
    }
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        //console.log(data.data);
        if (temp.result == "success") {
          this.success_array.push(temp.data.id);
          this.coursework_count++;
          if (this.coursework_count <= this.number_of_coursework - 1) {
            this.UploadData();
          } else {
            this.UpdateFinalList();
          }

        } else {
          //update fail
          this.coursework_count++;
          if (this.coursework_count <= this.number_of_coursework - 1) {
            this.UploadData();
          } else {
            //do nothing
            this.uploading = false;
          }
        }
        this.global_service.dismiss();
      });
  }
  UpdateFinalList() {
    this.course.assignment_list = this.success_array;
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id,
      "assignment_list": this.success_array
    };
    let body = { "update_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        this.uploading = false;
        this.global_service.dismiss();
        if (JSON.parse(data.data).result == "success") {
          let return_data = this.multi_coursework;
          // this.viewCtrl.dismiss(return_data);
          this.global_service.create_course.assignment_list = this.success_array;
          this.global_service.resetLoading();
          this.navCtrl.pop();
        }
      });
  }
  GoBack() {
    // this.viewCtrl.dismiss();
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  AddCoursework() {
    this.number_of_coursework++;
    let new_array = [{
      coursework_title: "",
      content: "",
      id: -1
    }];
    this.multi_coursework.push.apply(this.multi_coursework, new_array);

  }
  async DeleteCoursework(id) {
    // //console.log(this.multi_coursework);
    // //console.log(id);
    if (this.number_of_coursework == 0) {
      let msg_title = "數量不能小於0";
      let msg_option_1 = "確認";
      if (this.global_service.lang == 'en') {
        msg_title = "Can't less than 0 work";
        msg_option_1 = "Confirm";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "数量不能小於0";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    } else {
      this.multi_coursework.splice(id, 1);
      this.number_of_coursework--;
    }
    //console.log(this.multi_coursework);
    //console.log(id);
  }
  async Upload(index) {
    let msg_title = "請不要選擇檔案大於25mb";
    let msg_option_1 = "確認";
    if (this.global_service.lang == 'en') {
      msg_title = "Please select file not large than 25mb";
      msg_option_1 = "Confirm";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "请不要选择档案大於25mb";
      msg_option_1 = "确认";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      cssClass: 'custom-alertControl',
      backdropDismiss:false,
      buttons: [{
        text: msg_option_1,
        handler: () => {
          if (this.plt.is('android')) {
            this.androidUploadDo(index);
          } else {
            this.UploadDo(index);
          }
        }
      }]
    });
    await alert_box.present();
  }
  async UploadDo(index) {
    this.chooser.getFile('*')
      .then(file => {
        if (file != undefined) {
          let name_parts = file.name.split(".");
          // console.log(JSON.stringify(file));
          // console.log(JSON.stringify(this.multi_coursework[index]));
          this.multi_coursework[index].file_name = file.name;
          this.multi_coursework[index].file_type = name_parts[name_parts.length - 1];
          this.multi_coursework[index].file_data = file.dataURI;
        }
      })
      .catch(async (error: any) => {
        let msg_title = "發生錯誤";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Error";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "发生错误";
          msg_option_1 = "确认";
        }
        console.error(error);
        const alert_box = await this.alert.create({
          message: msg_title,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
      });
  }

  async androidUploadDo(index) {
    this.current_index = index;
    if (this.pwa_upload == null) {
      return;
    }
    this.pwa_upload.nativeElement.click();
  }

  uploadPWA() {
    if (this.pwa_upload == null || this.global_service.isLoading) {
      return;
    }
    const fileList: FileList = this.pwa_upload.nativeElement.files;
    if (fileList && fileList.length > 0) {
      console.log(fileList[0].name);
      this.global_service.isLoading = true;
      let name_parts = fileList[0].name.split(".");
      this.multi_coursework[this.current_index].file_name = fileList[0].name;
      this.multi_coursework[this.current_index].file_type = name_parts[name_parts.length - 1];
      this.global_service.firstFileToBase64(fileList[0]).then((base64: string) => {
        console.log(base64);
        this.multi_coursework[this.current_index].file_data = base64;
        this.global_service.isLoading = false;
      })
    }
  }


  async UploadExtra(index) {
    let msg_title = "請不要選擇檔案大於25mb";
    let msg_option_1 = "確認";
    if (this.global_service.lang == 'en') {
      msg_title = "Please select file not large than 25mb";
      msg_option_1 = "Confirm";
    }
    if (this.global_service.lang == 'cn') {
      msg_title = "请不要选择档案大於25mb";
      msg_option_1 = "确认";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      cssClass: 'custom-alertControl',
      backdropDismiss:false,
      buttons: [{
        text: msg_option_1,
        handler: () => {
          if (this.plt.is('android')) {
            this.androidUploadExtraDo(index);
          } else {
            this.UploadExtraDo(index);
          }
        }
      }]
    });
    await alert_box.present();
  }
  async UploadExtraDo(index) {
    this.chooser.getFile('*')
      .then(file => {
        if (file != undefined) {
          let name_parts = file.name.split(".");
          //console.log(this.multi_coursework[index]);
          this.multi_coursework[index].extra_file_name = file.name;
          this.multi_coursework[index].extra_file_type = name_parts[name_parts.length - 1];
          this.multi_coursework[index].extra_file_data = file.dataURI;
          //console.log(this.multi_coursework[index]);
        }
      })
      .catch(async (error: any) => {
        let msg_title = "發生錯誤";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Error";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "发生错误";
          msg_option_1 = "确认";
        }
        console.error(error);
        const alert_box = await this.alert.create({
          message: msg_title,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
      });
  }


  async androidUploadExtraDo(index) {
    this.current_index = index;
    if (this.pwa_upload_EXTRA == null) {
      return;
    }
    this.pwa_upload_EXTRA.nativeElement.click();
  }

  uploadPWAEXTRA() {
    if (this.pwa_upload_EXTRA == null || this.global_service.isLoading) {
      return;
    }
    const fileList: FileList = this.pwa_upload_EXTRA.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.global_service.isLoading = true;
      console.log(fileList[0].name);
      let name_parts = fileList[0].name.split(".");
      this.multi_coursework[this.current_index].extra_file_name = fileList[0].name;
      this.multi_coursework[this.current_index].extra_file_type = name_parts[name_parts.length - 1];
      this.global_service.firstFileToBase64(fileList[0]).then((base64: string) => {
        console.log(base64);
        this.multi_coursework[this.current_index].extra_file_data = base64;
        this.global_service.isLoading = false;
      })
    }
  }
  async OpenFile(index) {
    window.open('https://intervii.com/media/' + this.multi_coursework[index].file_name, '_system');
  }
  async OpenExtraFile(index) {
    window.open('https://intervii.com/media/' + this.multi_coursework[index].extra_file_data, '_system');
  }
}
