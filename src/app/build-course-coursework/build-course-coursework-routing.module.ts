import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseCourseworkPage } from './build-course-coursework.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseCourseworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseCourseworkPageRoutingModule {}
