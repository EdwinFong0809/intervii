import { Injectable } from '@angular/core';
import { LoadingController, ToastController, Platform } from '@ionic/angular';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import Player from '@vimeo/player';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public user_data: any = null;
  public user_id: any = 0;
  public user_profile: any = null;
  public edit_profile_from: string = "";
  public view_user_id: any = 0;
  public course_data: any = null;
  public vlog_data: any = null;
  public add_vlog: any = null;
  public editor_title: string = "";
  public editor_text: string = "";
  public editor_preview_text: string = "";
  public back_url: string = "";
  public back_update: boolean = false;
  public create_course: any = null;
  public edit_course: boolean = false;
  public get_coursework: boolean = false;
  public edit_coursework: any = [];
  public edit_coursework_index: any = 0;
  public comment_detail: any = null;
  public question_detail: any = null;
  public assignment_data: any = null;

  public editing_profile: boolean = false;
  public editing_profile_data: any = null;

  public new_coursework: any = null;
  public my_course_temp_storage: any = null;

  public isLoading: boolean = false;
  public load_count: any = 0;

  public chat_id: any = 0;
  public unread_count:number = 0;
  public chatroom: any = null;
  public socket_page: string = "";
  public home_socket: boolean = false;
  public list_socket: boolean = false;
  public room_socket: boolean = false;

  public course_work_detail: any = null;
  public course_purchase:boolean = false;

  public edit_course_unit: any = null;

  public price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];
  public iap_is_debug:boolean = false;

  public chatroom_back:boolean = false;
  public from_follow:boolean = false;

  private loading_active:boolean = false;
  private player:Player;
  private player_init:boolean = false;

  public categories:any;
  public lang:string = "zh";

  constructor(
    private loadingController: LoadingController,
    private streamingMedia: StreamingMedia,
    public platform: Platform,
    private toastController: ToastController
  ){
    if (this.platform.is('ios')) {
      // this.price_list = [38,58,108,0];
    }
    
  }

  async playVimeo(vimeo_id){
    this.isLoading = true;
    this.presentLoading();
    var element = document.getElementById('vimeo_player');
    var parent = element.parentElement;
    parent.removeChild(element);
    var para = document.createElement("div"); 
    var att = document.createAttribute("id");
    att.value = "vimeo_player";
    para.setAttributeNode(att);
    parent.appendChild(para);
    const player = new Player('vimeo_player', {
      id: vimeo_id
    });
    console.log("this.presentLoading()");
    // this.presentLoading();
    // setTimeout(function () {
    //   this.dismiss();
    //   console.log("this.dismiss()");
    // }, 2000);
    player.ready().then(function() {
      setTimeout(() => {
        this.dismiss();
        console.log("this.dismiss()");
      }, 2000);
      player.requestFullscreen().then(function() {
        //  console.log("this.dismiss()");
        //  setTimeout(function () {
           player.Play();
        //  }, 1000);
       }).catch(function(error) {
       });
    });
    //   if(this.player_init){
    //     this.player.destroy().then(function() {
    //       this.player = new Player('vimeo_player', {
    //         id: vimeo_id
    //       });
    //       this.player.ready().then(function() {
    //         this.player.requestFullscreen().then(function() {
    //           this.player.Play();
    //          }).catch(function(error) {
    //          });
    //       });
    //   }).catch(function(error) {
    //   });
    // }else{
    //   this.player_init = true;
    //   this.player = new Player('vimeo_player', {
    //     id: vimeo_id
    //   });
    //   this.player.ready().then(function() {
    //     this.player.requestFullscreen().then(function() {
    //       this.player.Play();
    //      }).catch(function(error) {
    //      });
    //   });
    // }
  }

  playURL(URL){
    this.presentLoading();
    var element = document.getElementById('vimeo_player');
    var parent = element.parentElement;
    parent.removeChild(element);
    var para = document.createElement("div"); 
    var att = document.createAttribute("id");
    att.value = "vimeo_player";
    para.setAttributeNode(att);
    parent.appendChild(para);
    // const player2 = new Player('vimeo_player', {
    //   url:URL
    // });
    //   player2.requestFullscreen().then(function() {
    //     player2.Play();
    //    }).catch(function(error) {
    //    });

      let options: StreamingVideoOptions = {
        successCallback: () => { this.dismiss(); },
        errorCallback: (e) => { console.log('Error streaming') },
        orientation: 'landscape',
        shouldAutoClose: true,
        controls: true
      };

      this.streamingMedia.playVideo(URL, options);
    //   if(this.player_init){
    //     this.player.destroy().then(function() {
    //       this.player = new Player('vimeo_player', {
    //         url:URL
    //       });
    //       this.player.ready().then(function() {
    //         this.player.requestFullscreen().then(function() {
    //           this.player.Play();
    //          }).catch(function(error) {
    //          });
    //       });
    //   }).catch(function(error) {
    //   });
    // }else{
    //   this.player_init = true;
    //   this.player = new Player('vimeo_player', {
    //     url:URL
    //   });
    //   this.player.ready().then(function() {
    //     this.player.requestFullscreen().then(function() {
    //       this.player.Play();
    //      }).catch(function(error) {
    //      });
    //   });
    // }
  }


  firstFileToBase64(fileImage: File): Promise<{}> {
    return new Promise((resolve, reject) => {
      let fileReader: FileReader = new FileReader();
      if (fileReader && fileImage != null) {
        fileReader.readAsDataURL(fileImage);
        fileReader.onload = () => {
          resolve(fileReader.result);
        };
        fileReader.onerror = (error) => {
          reject(error);
        };
      } else {
        reject(new Error('No file found'));
      }
    });
  }

  async presentLoading() {
    let msg_title = "請稍候。。。";
    if(this.lang == 'en'){
      msg_title = "Loading。。。";
    }
    if(this.lang == 'cn'){
      msg_title = "请稍候。。。";
    }

    if (this.load_count == 0 && !this.loading_active) {
      return await this.loadingController.create({
        message: msg_title,
        mode: "ios",
        spinner: 'crescent',
        duration: 5000,
      }).then(a => {
        a.present().then(() => {
          if (!this.isLoading) {
            a.dismiss();
          }
        });
        this.loading_active = true;
      });
      this.load_count ++;
    } else {
      this.load_count ++;
    }
    console.log("-------------------------------"+this.load_count+"-------------------------------");
  }

  async resetLoading(){
    this.isLoading = false;
    this.load_count = 0;
    if(this.load_count<=0){
      if(this.loading_active){
        this.loading_active = false; 
        return await this.loadingController.dismiss();
      }
    }
  }

  async dismiss() {
    this.isLoading = false;
    this.load_count--;
    if(this.load_count <= 0 ){
      this.load_count = 0;
    }else{
    }
    console.log("==============================="+this.load_count+"===============================");
    if(this.load_count<=0){
      // return await this.loadingController.dismiss();
      if(this.loading_active){
        this.loading_active = false; 
        return await this.loadingController.dismiss();
      }
    }
  }

  async presentToast(msg, position?) {
    const toast = await this.toastController.create({
      message: msg,
      position: (position != undefined ? position : "bottom"),
      duration: 2500
    });
    toast.present();
  }

}
