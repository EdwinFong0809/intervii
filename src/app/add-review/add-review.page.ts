import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.page.html',
  styleUrls: ['./add-review.page.scss'],
})
export class AddReviewPage implements OnInit {
  title: string = "";
  content: string = "";
  user_id: any;
  course_id: any;
  lecturer_id: any;
  rank: number = 0;
  press: boolean = false;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
    ) { 
      this.title = "";
      this.content = "";
    }

  ngOnInit() {
  }

  SetRank(i){
    if(this.rank == i){
      this.rank = 0;
    }else{
      this.rank = i;
    }
  }

  ionViewDidEnter() {
    this.course_id = this.global_service.course_data.id;
    this.user_id = this.global_service.user_data.id;
    this.lecturer_id = this.global_service.course_data.lecturer_id;
  }
  async Submit() {
    if (!this.press) {
      this.press = true;
      if (this.title == "" || this.content == "") {
        let msg_title = "請勿留空";
        let msg_option_1 = "確定";
        if (this.global_service.lang == 'en') {
          msg_title = "Please don't leave blank";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "请勿留空";
          msg_option_1 = "确定";
        }
        const alert_box = await this.alert.create({
          message: msg_title,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
        this.press = false;
      } else {
        let url = "https://intervii.com/backend/request.php";
          this.content = this.content.replace(/youtu.be/g,"youtube.com/embed");
        var json = { "course_id": this.course_id, "user_id": this.user_id, "lecturer_id": this.lecturer_id, "rank": this.rank, "title": this.title, "content": this.content };
        let body = { "new_review": JSON.stringify(json) };
        //console.log(json);
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(async data => {
            this.global_service.dismiss();
            //console.log(data.data);
            let temp = JSON.parse(data.data);
            let msg_title = "提交成功";
            let msg_option_1 = "確定";
            if (this.global_service.lang == 'en') {
              msg_title = "Success";
              msg_option_1 = "Confirm";
            }
            if (this.global_service.lang == 'cn') {
              msg_title = "提交成功";
              msg_option_1 = "确定";
            }
            if (temp.result == "success") {
              this.GoBack();
              const alert_box = await this.alert.create({
                message: msg_title,
                cssClass: 'custom-alertControl',
                backdropDismiss:false,
                buttons: [msg_option_1]
              });
              await alert_box.present();
            } else {
              msg_title = "提交失敗";
              if (this.global_service.lang == 'en') {
                msg_title = "Fail";
              }
              if (this.global_service.lang == 'cn') {
                msg_title = "提交失败";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                cssClass: 'custom-alertControl',
                backdropDismiss:false,
                buttons: [msg_option_1]
              });
              await alert_box.present();
            }
            this.press = false;
          });
      }
    }
  }

  GoBack() {
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  Preview(){
    // this.content = this.content.replace(/youtu.be/g,"youtube.com/embed");
    // this.navCtrl.push(InfoPreviewPage, {info: this.content });
    this.global_service.editor_preview_text = this.content.replace(/youtu.be/g, "youtube.com/embed");
    // this.navCtrl.push(InfoPreviewPage, { info: this.content });
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/editor-preview']);
  }

}
