import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourseCourseworkDetailPageRoutingModule } from './course-coursework-detail-routing.module';

import { CourseCourseworkDetailPage } from './course-coursework-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourseCourseworkDetailPageRoutingModule
  ],
  declarations: [CourseCourseworkDetailPage]
})
export class CourseCourseworkDetailPageModule {}
