import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CourseCourseworkDetailPage } from './course-coursework-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CourseCourseworkDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CourseCourseworkDetailPageRoutingModule {}
