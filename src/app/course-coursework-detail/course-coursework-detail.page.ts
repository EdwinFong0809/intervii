import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { HTTP } from "@ionic-native/http/ngx";
import { Storage } from "@ionic/storage";
import { GlobalService } from "../global.service";
import { DomSanitizer } from '@angular/platform-browser';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-course-coursework-detail',
  templateUrl: './course-coursework-detail.page.html',
  styleUrls: ['./course-coursework-detail.page.scss'],
})
export class CourseCourseworkDetailPage implements OnInit {

  constructor(
    // private iap: InAppPurchase,
    public platform: Platform,
    private storage: Storage,
    public http: HTTP,
    public navCtrl: NavController,
    private streamingMedia: StreamingMedia,
    // public viewCtrl: ViewController,
    // public app: App,
    // public navParams: NavParams,
    // private payPal: PayPal,
    private alertCtrl: AlertController,
    public global_service: GlobalService,
    private iab: InAppBrowser,
    private sanitizer: DomSanitizer,
    ) { }

  ngOnInit() {
    //console.log(this.global_service.course_work_detail);
  }
  GoBack() {
    // this.navCtrl.navigateBack('/tabs/tab4');
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  OpenLink(link) {
        this.iab.create("https://intervii.com/media/" + link, "_blank", 'location=no,hidden=no');
  }
  AddCourseWork(){
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/add-coursework']);
  }
}
