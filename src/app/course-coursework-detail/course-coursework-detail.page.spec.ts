import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CourseCourseworkDetailPage } from './course-coursework-detail.page';

describe('CourseCourseworkDetailPage', () => {
  let component: CourseCourseworkDetailPage;
  let fixture: ComponentFixture<CourseCourseworkDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCourseworkDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CourseCourseworkDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
