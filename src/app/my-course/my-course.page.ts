import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { Router, NavigationEnd } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-my-course',
  templateUrl: './my-course.page.html',
  styleUrls: ['./my-course.page.scss'],
})
export class MyCoursePage implements OnInit {
  user_id: any;
  temp_event: any;
  current_course: any;
  current_course_tag: string = "";

  bought_course: any = [];
  favourited_course: any = [];
  mycourse_course: any = [];
  streamingtime_course: any = [];
  price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];

  user_data: any = null;
  direction: string = "";
  logined: boolean = false;
  current_scroll: number = 0;
  current_tab: string = "first_tab";
  missing_text: string = "";

  isIOS: boolean = false;
  connecting_server: boolean = false;

  refresher:any = null;
  can_refresh:boolean = true;
  just_reload: boolean = false;

  constructor(
    private http: HTTP,
    // public events: Events,
    // public app: App,
    public platform: Platform,
    // private alert: AlertController,
    private storage: Storage,
    // public modalCtrl: ModalController,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private router: Router
  ) {
    if(this.global_service.lang == 'en'){
      this.missing_text = "No Course";
    }else if(this.global_service.lang == 'cn'){
      this.missing_text = "没有课堂";
    }else{
      this.missing_text = "沒有課堂";
    }
    if (this.platform.is('ios')) {
      this.isIOS = true;
    }
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        //console.log(ev);
        if(ev.url=="/course-detail"){
          this.global_service.my_course_temp_storage = this.current_course;
          //console.log(this.global_service.my_course_temp_storage);
          //console.log(this.current_course);
        }
        if(ev.url=="/tabs/my-course"){
          if(this.global_service.my_course_temp_storage != null){
            this.current_course = this.global_service.my_course_temp_storage;
          }
          //console.log(this.global_service.my_course_temp_storage);
          //console.log(this.current_course);
        }
      }
    });
  }

  ionViewDidEnter() {
    this.price_list = this.global_service.price_list;
    //console.log("ionViewDidEnter " + this.current_course);
    this.user_data = this.global_service.user_data;
  }

  ngOnInit() {
    this.bought_course = [];
    this.favourited_course = [];
    this.mycourse_course = [];
    this.streamingtime_course = [];
    this.missing_text = "";
    //console.log("==================init 1");
    this.LoadInit();
    this.LoadBought();
  }
  LoadInit() {
    this.user_id = this.global_service.user_id;
    this.user_data = this.global_service.user_data;
  }
  GoProfile(id) {
    if (this.user_id == 0 || this.user_id == undefined) {
    } else {
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      this.global_service.view_user_id = id;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    }
  }

  doRefresh(refresher){
    console.log("refresh");
    this.refresher = refresher;
    this.just_reload = true;
    this.can_refresh = false;
    setTimeout(function(){
      this.can_refresh = true;
    }, 10000);
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        if (this.current_course_tag != "已購買") {
          this.current_course_tag = "已購買";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadBought();
        }
        break;
      case "second_tab":
        // //console.log(this.tab2);
        if (this.current_course_tag != "已收藏") {
          this.current_course_tag = "已收藏";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadFavourite();
        }
        break;
      case "third_tab":
        // //console.log(this.tab3);
        if (this.current_course_tag != "我的課堂") {
          this.current_course_tag = "我的課堂";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadMyCourse();
        }
        break;
      case "forth_tab":
        // //console.log(this.tab4);
        if (this.current_course_tag != "直播時間") {
          this.current_course_tag = "直播時間";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadStreaming();
        }
        break;
    }
    setTimeout(() => {
      refresher.target.complete();
    }, 2000);
  }

  segmentChanged($event) {
    //console.log($event.detail.value);
    this.current_tab = $event.detail.value;
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        if (this.current_course_tag != "已購買") {
          this.current_course_tag = "已購買";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadBought();
        }
        break;
      case "second_tab":
        // //console.log(this.tab2);
        if (this.current_course_tag != "已收藏") {
          this.current_course_tag = "已收藏";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadFavourite();
        }
        break;
      case "third_tab":
        // //console.log(this.tab3);
        if (this.current_course_tag != "我的課堂") {
          this.current_course_tag = "我的課堂";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadMyCourse();
        }
        break;
      case "forth_tab":
        // //console.log(this.tab4);
        if (this.current_course_tag != "直播時間") {
          this.current_course_tag = "直播時間";
          this.bought_course = [];
          this.favourited_course = [];
          this.mycourse_course = [];
          this.streamingtime_course = [];
          this.missing_text = "";
          this.current_course = [];
          this.LoadStreaming();
        }
        break;
    }
  }
  OpenCourse(course) {
    // this.app.getRootNav().push(HomeLatestCoursePage, { data: course });
    this.global_service.course_data = course;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/course-detail']);
  }
  LoadData() {
    this.storage.get('user_data').then((val) => {
      if (val != undefined && val != null) {
        this.user_data = val;
        this.logined = true;

        let url2 = "https://intervii.com/backend/request.php";
        let body2 = { "get_user_by_id": JSON.stringify({ "user_id": this.user_data.id }) };
        //let body = { "current_datetime_string" : ""};
        let headers2 = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.http.post(url2, body2, headers2)
          .then(data => {
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              this.user_data = temp.data;
              this.storage.set('user_data', this.user_data);
            }
          });
      } else {
        this.user_data = {};
        this.logined = false;
      }
      if (this.user_data != undefined && this.user_data.follow_course == undefined) {
        this.user_data.follow_course = [];
      }
      //console.log("LoadData " + this.user_data);
    });
    // this.LoadBought();
  }

  LoadBought() {
    this.connecting_server = true;
    this.current_course = [];
    this.bought_course = [];
    this.favourited_course = [];
    this.mycourse_course = [];
    this.streamingtime_course = [];
    if (this.user_data != null) {
      this.user_data = this.global_service.user_data;
      let url = "https://intervii.com/backend/request.php";
      let json = { user_id: this.user_data.id };
      let body = { "get_current_courses": JSON.stringify(json) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log("LoadBought " + data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if (temp.data[i].rank == undefined) {
                temp.data[i].rank = 0;
              }
            }
            this.bought_course = temp.data;
            this.current_course = temp.data;
            if (this.current_course.length == 0) {
              if(this.global_service.lang == 'en'){
                this.missing_text = "No purchased course";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有已购买课堂";
              }else{
                this.missing_text = "沒有已購買課堂";
                }
            }
          } else {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No purchased course";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有已购买课堂";
            }else{
              this.missing_text = "沒有已購買課堂";
              }
          }
          //console.log(this.current_course);
          this.connecting_server = false;
        });
    } else {
      this.connecting_server = false;
    }
    //   } else {
    //     this.user_data = { id: 0 };
    //   }
    // });
  }


  LoadPageData() {
    if (this.user_data != null) {
      this.connecting_server = true;
      let url = "https://intervii.com/backend/request.php";
      let json = { courses: this.user_data.follow_course };
      let body = { "get_fav_courses": JSON.stringify(json) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log("LoadPageData " + data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if (temp.data[i].rank == undefined) {
                temp.data[i].rank = 0;
              }
            }
            this.favourited_course = temp.data;
            this.current_course = temp.data;
            if (this.current_course.length == 0) {
              // this.missing_text = "";
              if(this.global_service.lang == 'en'){
                this.missing_text = "No bookmarked course";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有已收藏课堂";
              }else{
                this.missing_text = "沒有已收藏課堂";
              }
            }
          } else {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No bookmarked course";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有已收藏课堂";
            }else{
              this.missing_text = "沒有已收藏課堂";
            }
          }
          this.connecting_server = false;
        });
    }
  }
  LoadFavourite() {
    this.current_course = [];
    this.bought_course = [];
    this.favourited_course = [];
    this.mycourse_course = [];
    this.streamingtime_course = [];
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "user_id": this.user_data.id
    };
    let body = { "get_user_by_id": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(async data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.user_data = temp.data;
          if (this.user_data.follow_course == undefined) {
            this.user_data.follow_course = [];
          } else {
            this.LoadPageData();
          }
        }
      });
  }

  LoadMyCourse() {
    this.current_course = [];
    this.bought_course = [];
    this.favourited_course = [];
    this.mycourse_course = [];
    this.streamingtime_course = [];
    if (this.user_data != null) {
      this.connecting_server = true;
      this.user_data = this.global_service.user_data;
      let url = "https://intervii.com/backend/request.php";
      let json = { user_id: this.user_data.id };
      let body = { "get_user_open_course": JSON.stringify(json) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log("LoadMyCourse " + data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if (temp.data[i].rank == undefined) {
                temp.data[i].rank = 0;
              }
            }
            this.mycourse_course = temp.data;
            this.current_course = temp.data;
            if (this.current_course.length == 0) {
              this.missing_text = "沒有我的課堂";
            }
          } else {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No My Course";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有我的课堂";
            }else{
              this.missing_text = "沒有我的課堂";
            }
          }
          this.connecting_server = false;
        });
    } else {
      this.connecting_server = false;
    }
  }

  LoadStreaming() {
    this.connecting_server = true;
    this.current_course = [];
    this.bought_course = [];
    this.favourited_course = [];
    this.mycourse_course = [];
    this.streamingtime_course = [];

    if (this.user_data != null) {
      this.user_data = this.global_service.user_data;
      let url = "https://intervii.com/backend/request.php";
      let json = { user_id: this.user_data.id };
      let body = { "get_bought_live_course": JSON.stringify(json) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.global_service.isLoading = true;
      this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log("LoadStreaming " + data.data);
          if (temp.result == "success") {
            for (let i = 0; i < temp.data.length; i++) {
              if (temp.data[i].rank == undefined) {
                temp.data[i].rank = 0;
              }
            }
            this.streamingtime_course = temp.data;
            this.current_course = temp.data;
            if (this.current_course.length == 0) {
              if(this.global_service.lang == 'en'){
                this.missing_text = "No LIVE";
              }else if(this.global_service.lang == 'cn'){
                this.missing_text = "没有直播";
              }else{
                this.missing_text = "沒有直播";
              }
            }
          } else {
            if(this.global_service.lang == 'en'){
              this.missing_text = "No LIVE";
            }else if(this.global_service.lang == 'cn'){
              this.missing_text = "没有直播";
            }else{
              this.missing_text = "沒有直播";
            }
          }
          this.connecting_server = false;
        });
    } else {
      this.connecting_server = false;
    }
  }

  AddFavouriteCourse(id) {
    if (this.user_data != null) {
      if (this.user_id != 0 && this.user_id != undefined) {
        //console.log("AddFavouriteCourse 1 " + this.user_data);

        let url = "https://intervii.com/backend/request.php";
        let body = { "follow_course": JSON.stringify({ "user_id": this.user_data.id, "course_id": id }) };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        // this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            this.global_service.dismiss();
            let temp = JSON.parse(data.data);
            //console.log("AddFavouriteCourse 2 " + data.data);
            if (temp.result == "success") {
              this.user_data.follow_course = temp.data;
              this.global_service.user_data = this.user_data;
              this.storage.set('user_data', this.user_data);
            }


            this.current_course.forEach((course_data, index) => {
              if(course_data.id == id){
                if(this.user_data.follow_course.includes(id)){
                  if(!course_data.followers.includes(this.user_data.id)){
                    course_data.followers.push(this.user_data.id);
                  }
                }else{
                  if(course_data.followers.includes(this.user_data.id)){
                    let index = course_data.followers.indexOf(this.user_data.id, 0);
                    if (index > -1) {
                      course_data.followers.splice(index, 1);
                    }        
                  }
                }
              }
            });
            //console.log(this.user_data);
          });
      }
    }
  }

  login(){
    this.navCtrl.navigateForward(['/login']);
  }
}
