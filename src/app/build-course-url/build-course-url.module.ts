import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseUrlPageRoutingModule } from './build-course-url-routing.module';

import { BuildCourseUrlPage } from './build-course-url.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseUrlPageRoutingModule
  ],
  declarations: [BuildCourseUrlPage]
})
export class BuildCourseUrlPageModule {}
