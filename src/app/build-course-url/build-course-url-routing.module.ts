import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseUrlPage } from './build-course-url.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseUrlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseUrlPageRoutingModule {}
