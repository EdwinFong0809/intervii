import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-build-course-url',
  templateUrl: './build-course-url.page.html',
  styleUrls: ['./build-course-url.page.scss'],
})
export class BuildCourseUrlPage implements OnInit {
  url: any;
  course_id: any;
  course:any = {
    website : ""
  };
  url_content:string= "";

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    //console.log('ionViewDidLoad BuildyourcourseCreateUrlPage');
    this.course = this.global_service.create_course;
    //console.log(JSON.stringify(this.course));
    this.course_id = this.course.id;
    if(this.global_service.user_data.display_name != undefined && this.global_service.user_data.display_name != null && this.global_service.user_data.display_name != ""){
      this.url_content = this.global_service.user_data.display_name + "/";
    }
  }
  async Dismiss() {
    let temp_course_url = this.course.website ;
    if(this.course.website == null || this.course.website == ""){
      let msg_title = "請輸入Url";
      let msg_option_1 = "確認";
      if (this.global_service.lang == 'en') {
        msg_title = "Please input url";
        msg_option_1 = "Confirm";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "请输入Url";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }else if ((temp_course_url.length < 3 || temp_course_url.length > 20) || this.TextRemover(temp_course_url) != "") {
      let msg_title = "網址格式不正確";
      let msg_option_1 = "確認";
      if (this.global_service.lang == 'en') {
        msg_title = "Url format incorrect";
        msg_option_1 = "Confirm";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "网址格式不正确";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }else{
      this.UploadData();
    }
  }
  GoBack(){
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  UploadData(){
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id ,
      "website" : this.course.website
     };
    let body = { "update_course": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if(temp.result == "success"){
          this.global_service.create_course = this.course;
          this.global_service.resetLoading();
          this.navCtrl.pop();
        }else{
          alert(temp.data);
        }
      });
  }


  TextRemover(text) {
    return text.toLowerCase().replace(/[a-zA-Z0-9]+/g, "").replace(/-/g, "").replace(/_/g, "");
  }
}
