import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCourseUrlPage } from './build-course-url.page';

describe('BuildCourseUrlPage', () => {
  let component: BuildCourseUrlPage;
  let fixture: ComponentFixture<BuildCourseUrlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCourseUrlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCourseUrlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
