import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCoursePage } from './build-course.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCoursePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCoursePageRoutingModule {}
