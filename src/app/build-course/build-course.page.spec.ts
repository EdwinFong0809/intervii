import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCoursePage } from './build-course.page';

describe('BuildCoursePage', () => {
  let component: BuildCoursePage;
  let fixture: ComponentFixture<BuildCoursePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCoursePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
