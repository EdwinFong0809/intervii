import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCoursePageRoutingModule } from './build-course-routing.module';

import { BuildCoursePage } from './build-course.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCoursePageRoutingModule
  ],
  declarations: [BuildCoursePage]
})
export class BuildCoursePageModule {}
