import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-build-course',
  templateUrl: './build-course.page.html',
  styleUrls: ['./build-course.page.scss'],
})
export class BuildCoursePage implements OnInit {
  pressed: boolean = false;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private storage: Storage,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.LoadData();
  }

  GoBack() {
    this.global_service.vlog_data = null;
    // this.navCtrl.navigateBack('/tabs/tab4');
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  LoadData() {
    this.storage.get('user_data').then((val) => {
      if (val != undefined && val != null) {
        let url = "https://intervii.com/backend/request.php";
        var json = {
          "user_id": val.id
        };
        let body = { "get_user_by_id": JSON.stringify(json) };
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.http.post(url, body, headers)
          .then(data => {
            this.global_service.dismiss();
            let temp = JSON.parse(data.data);
            this.storage.set('user_data', temp.data);
            this.global_service.user_data = temp.data;

          });
      }
    });
  }

  async BuildCourse() {
    if (!this.pressed) {
      this.pressed = true;
      if (this.global_service.user_id != undefined && this.global_service.user_id != null && this.global_service.user_id != 0) {

        if(this.global_service.user_data != undefined && this.global_service.user_data!=null && this.global_service.user_data!='' && this.global_service.user_data.is_lecturer!=undefined && this.global_service.user_data.is_lecturer!=null && (this.global_service.user_data.is_lecturer==true || this.global_service.user_data.is_lecturer=='true')){
          

        let url = "https://intervii.com/backend/request.php";
        var json = { "user_id": this.global_service.user_id };
        let body = { "get_incomplete_course": JSON.stringify(json) };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(async data => {
            let temp = JSON.parse(data.data);
            if (temp.result == "success") {
              let msg_title = "你有一個未完成的課堂，繼續填寫？";
              let msg_option_1 = "開新課堂";
              let msg_option_2 = "繼續";
              if (this.global_service.lang == 'en') {
                msg_title = "Continue the last incomplete course?";
                msg_option_1 = "New course";
                msg_option_2 = "Continue";
              }
              if (this.global_service.lang == 'cn') {
                msg_title = "你有一个未完成的课堂，继续填写？";
                msg_option_1 = "开新课堂";
                msg_option_2 = "继续";
              }
              const alert_box = await this.alert.create({
                message: msg_title,
                backdropDismiss:false,
                buttons: [{
                  text: msg_option_1,
                  handler: () => {
                    this.NewCourse();
                    this.pressed = false;
                  }

                }, {
                  text: msg_option_2,
                  handler: () => {
                    //console.log(temp.data);
                    this.pressed = false;
                    // this.app.getRootNav().push(BuildyourcourseCreatePage, { data: temp.data, id: this.user_id });
                    this.global_service.create_course = temp.data;
                    this.global_service.edit_course = false;
                    this.global_service.resetLoading();
                    this.navCtrl.navigateForward(['/build-course-index']);
                  }
                }],
                cssClass: 'custom-alertInfo',
              });
              await alert_box.present();
            } else {
              if (temp.data == "user is not lecturer") {
                let msg_title = "你必須先驗證您的電子郵件";
                let msg_option_1 = "確認";
                if (this.global_service.lang == 'en') {
                  msg_title = "Please verify email first";
                  msg_option_1 = "Confirm";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "你必须先验证您的电子邮件";
                  msg_option_1 = "确认";
                }
                const alert_box = await this.alert.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [
                    {
                      text: msg_option_1,
                      handler: () => {
                        this.pressed = false;
                      }
                    }]
                });
                await alert_box.present();
              } else {
                this.NewCourse();
              }
            }
            this.global_service.dismiss();
          });
        }else{

            let msg_title = "目前用戶類型不能開堂";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Current user type cannot create course";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "目前用户类型不能开堂";
              msg_option_1 = "确定";
            }
            const alert_box = await this.alert.create({
              message: msg_title,
              backdropDismiss:false,
              buttons: [
                {
                  text: msg_option_1,
                  handler: () => {
                    this.pressed = false;
                  }
                }]
            });
            await alert_box.present();
        }
      } else {
        let msg_title = "你必須先登錄";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Please login first";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "你必须先登录";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title,
          backdropDismiss:false,
          buttons: [{
            text: msg_option_1,
            handler: () => {
              this.pressed = false;
            }
          }]
        });
        await alert_box.present();
      }
    }
  }
  NewCourse() {
    let url = "https://intervii.com/backend/request.php";
    var json = { "user_id": this.global_service.user_id };
    let body = { "new_course": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        this.global_service.create_course = temp.data;
        this.global_service.edit_course = false;
        this.global_service.dismiss();
        this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/build-course-index']);
      });
  }
}
