import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildCoursePricePage } from './build-course-price.page';

describe('BuildCoursePricePage', () => {
  let component: BuildCoursePricePage;
  let fixture: ComponentFixture<BuildCoursePricePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildCoursePricePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildCoursePricePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
