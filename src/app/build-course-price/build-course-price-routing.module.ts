import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCoursePricePage } from './build-course-price.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCoursePricePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCoursePricePageRoutingModule {}
