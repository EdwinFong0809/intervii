import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCoursePricePageRoutingModule } from './build-course-price-routing.module';

import { BuildCoursePricePage } from './build-course-price.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCoursePricePageRoutingModule
  ],
  declarations: [BuildCoursePricePage]
})
export class BuildCoursePricePageModule {}
