import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-build-course-price',
  templateUrl: './build-course-price.page.html',
  styleUrls: ['./build-course-price.page.scss'],
})
export class BuildCoursePricePage implements OnInit {
  data_ready: any = {
    price: "",
    days: 90
  };
  course_id: any;
  course: any = {
    price: "",
    days: 90
  };
  price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.price_list = this.global_service.price_list;
    //console.log('ionViewDidLoad BuildyourcourseCreatePricePage');
    this.course = this.global_service.create_course;
    //console.log(JSON.stringify(this.course));
    this.course.price = parseInt(this.course.price);// + "";
    this.course_id = this.course.id;//this.navParams.get('course_id');
  }
  async Dismiss() {
    this.course.days = 90;
    if (this.course.days <= 0) {
      let msg_title = "課堂長度不能是0天";
      let msg_option_1 = "確認";
      if(this.global_service.lang == 'en'){
        msg_title = "課堂長度不能是0天";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "課堂長度不能是0天";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: '課堂長度不能是0天',
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    } 
    // else if (this.course.days.split(".")[1] != undefined) {
    //   const alert_box = await this.alert.create({
    //     message: '課堂長度不能包含小數',
    //     cssClass: 'custom-alertControl',
    //     buttons: ['確認']
    //   });
    //   await alert_box.present();
    // } 
    else {
      if (this.price_list[this.course.price] <= 0) {
        let msg_title = "確認建立免費課堂?";
        let msg_option_1 = "確認";
        let msg_option_2 = "取消";
        if(this.global_service.lang == 'en'){
          msg_title = "Confirm create free course?";
          msg_option_1 = "Confirm";
          msg_option_2 = "Cancel";
        }
        if(this.global_service.lang == 'cn'){
          msg_title = "确认建立免费课堂?";
          msg_option_1 = "确认";
          msg_option_2 = "取消";
        }
        const alert_box = await this.alert.create({
          message: msg_title,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [{
            text: msg_option_1,
            handler: () => {
              // this.data_ready.price = "0";
              // let return_data = this.data_ready;
              // this.viewCtrl.dismiss(return_data);
              this.UploadData();
            },
          }, {
            text: msg_option_2,
            handler: () => {
            },
          }]
        });
        await alert_box.present();
      } else {
        this.UploadData();
      }
    }
  }
  ClickPrice(){
    // if(this.course.price == 0){
    //   this.course.price = "";
    // }
  }
  ClickDay(){
    // if(this.course.days == 0){
    //   this.course.days = "";
    // }
  }

  UploadData(){
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id ,
      "days" : parseInt(this.course.days),
      "price" : parseInt(this.course.price)
     };
    let body = { "update_course": JSON.stringify(json) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    //console.log(json);
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        //console.log(data.data);
        this.course.price = parseInt(this.course.price);
        this.global_service.create_course = this.course;
        this.global_service.resetLoading();
        this.navCtrl.pop();
      });
  }
  async GoBack() {
    let msg_title = "當前步驟數據將不會保存/變更";
    let msg_option_1 = "確認";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "Won't save the current content";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "当前步骤数据将不会保存/变更";
      msg_option_1 = "确认";
      msg_option_2 = "取消";
    }
    const alert_box = await this.alert.create({
      message: msg_title,
      cssClass: 'custom-alertControl',
      backdropDismiss:false,
      buttons: [{
        text: msg_option_1,
        handler: () => {
          // this.viewCtrl.dismiss();
          this.global_service.resetLoading();
        this.navCtrl.pop();
        },
      }, {
        text: msg_option_2,
        handler: () => {
        },
      }]
    });
    await alert_box.present();

  }

}
