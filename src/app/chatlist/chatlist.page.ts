import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-chatlist',
  templateUrl: './chatlist.page.html',
  styleUrls: ['./chatlist.page.scss'],
})
export class ChatlistPage implements OnInit {
  chatrooms: any = [];
  temp_chatrooms: any = [];
  user_data: any;
  refresher: any;
  current_tab: any = "first_tab";
  can_refresh:boolean = true;
  alert_count:number = 0;

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
    private socket: Socket,
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.global_service.socket_page = "list";
    this.ConnectToNJS(this.global_service.user_data.id);
    this.platform.resume.subscribe(async () => {
      this.global_service.list_socket == false;
      this.ConnectToNJS(this.global_service.user_data.id);
    });
    // this.storage.get('user_data').then((val) => {
    //   if (val != undefined && val != null) {
    this.user_data = this.global_service.user_data;

    if(this.global_service.chatroom_back){
      this.global_service.chatroom_back = false;
    }else{
      let url2 = "https://intervii.com/backend/request.php";
      let body2 = { "get_user_by_id": JSON.stringify({ "user_id": this.user_data.id }) };
      let headers2 = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      // this.global_service.isLoading = true;
      // this.global_service.presentLoading();
      this.http.post(url2, body2, headers2)
        .then(data => {
          // this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          if (temp.result == "success") {
            this.user_data = temp.data;
            this.storage.set('user_data', this.user_data);
            // this.LoadData();
            this.LoadLecturerData();
          }
        });
      //   } else {
      //     this.user_data = {};
      //   }
      //   //console.log(this.user_data);
      // });
      }
  }

  segmentChanged($event) {

    this.chatrooms = [];
    this.current_tab = $event.detail.value;
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        //console.log(123);
        this.LoadLecturerData();
        break;
      case "second_tab":
        //console.log(456);
        this.LoadUserCourseData();
        break;
    }

  }


  LoadUserCourseData() {
    this.temp_chatrooms = [];
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_lecturer_chatrooms": JSON.stringify({ "lecturer_id": this.user_data.id }) };
    //let body = { "current_datetime_string" : ""};
    //console.log(body);
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.temp_chatrooms = temp.data;
          let course_ids = [];
          for (let i = 0; i < this.temp_chatrooms.length; i++) {
            course_ids.push(this.temp_chatrooms[i].course_id);
          }
          this.LoadCourse(course_ids);
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  LoadLecturerData() {
    this.temp_chatrooms = [];
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_chatrooms": JSON.stringify({ "user_id": this.user_data.id }) };
    //let body = { "current_datetime_string" : ""};
    //console.log(body);
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.temp_chatrooms = temp.data;
          let course_ids = [];
          for (let i = 0; i < this.temp_chatrooms.length; i++) {
            course_ids.push(this.temp_chatrooms[i].course_id);
          }
          this.LoadLecturerCourse(course_ids);
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }

  LoadLecturerCourse(course_ids) {
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_courses_by_ids": JSON.stringify({ "courses": course_ids }) };
    //let body = { "current_datetime_string" : ""};
    //console.log(body);
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let course_data = [];
          for (let i = 0; i < temp.data.length; i++) {
            course_data[temp.data[i].id] = temp.data[i];
          }
          let lecturer_ids = [];
          for (let i = 0; i < this.temp_chatrooms.length; i++) {
            this.temp_chatrooms[i].course_data = course_data[this.temp_chatrooms[i].course_id];
            lecturer_ids.push(this.temp_chatrooms[i].lecturer_id);
          }
          this.GetLecturerIcon(lecturer_ids);
          // //console.log(this.temp_chatrooms);
          // if(this.refresher!=undefined){
          //   this.refresher.complete();
          // }
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }


  LoadCourse(course_ids) {
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_courses_by_ids": JSON.stringify({ "courses": course_ids }) };
    //let body = { "current_datetime_string" : ""};
    //console.log(body);
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let course_data = [];
          for (let i = 0; i < temp.data.length; i++) {
            course_data[temp.data[i].id] = temp.data[i];
          }
          let user_ids = [];
          for (let i = 0; i < this.temp_chatrooms.length; i++) {
            this.temp_chatrooms[i].course_data = course_data[this.temp_chatrooms[i].course_id];
            user_ids.push(this.temp_chatrooms[i].user_id);
          }
          this.GetUserIcon(user_ids);
          // //console.log(this.temp_chatrooms);
          // if(this.refresher!=undefined){
          //   this.refresher.complete();
          // }
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  GetLecturerIcon(user_ids) {
    // this.chatrooms = [];
    let url2 = "https://intervii.com/backend/request.php";
    let body2 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": user_ids }) };
    //let body = { "current_datetime_string" : ""};
    let headers2 = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url2, body2, headers2)
      .then(data => {
        // this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let users = [];
          for (let i = 0; i < temp.data.length; i++) {
            users[temp.data[i].id] = temp.data[i];
          }
          for (let i = 0; i < this.temp_chatrooms.length; i++) {
            this.temp_chatrooms[i].lecturer_data = users[this.temp_chatrooms[i].lecturer_id];
          }

          this.chatrooms = this.temp_chatrooms;
          //console.log(this.chatrooms);
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  GetUserIcon(user_ids) {
    let url2 = "https://intervii.com/backend/request.php";
    let body2 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": user_ids }) };
    //let body = { "current_datetime_string" : ""};
    let headers2 = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url2, body2, headers2)
      .then(data => {
        // this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          let users = [];
          for (let i = 0; i < temp.data.length; i++) {
            users[temp.data[i].id] = temp.data[i];
          }
          for (let i = 0; i < this.temp_chatrooms.length; i++) {
            this.temp_chatrooms[i].user_data = users[this.temp_chatrooms[i].user_id];
          }
          this.chatrooms = this.temp_chatrooms;
          //console.log(this.chatrooms);
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        } else {
          if (this.refresher != undefined) {
            this.refresher.complete();
          }
        }
      });
  }
  // get_courses_by_ids


  doRefresh(refresher) {
    this.refresher = refresher;
    this.can_refresh = false;
    setTimeout(function(){
      this.can_refresh = true;
    }, 10000);
    // this.LoadData();
    switch (this.current_tab) {
      case "first_tab":
        // //console.log(this.tab1);
        this.LoadLecturerData();
        break;
      case "second_tab":
        this.LoadUserCourseData();
        break;
    }
    setTimeout(() => {
      //console.log('Async operation has ended');
      refresher.target.complete();
    }, 2000);
  }

  GoChat(data) {
    // this.app.getRootNav().push(ChatroomPage, { chatroom: data });
    // this.socket.disconnect();
    this.global_service.chatroom = data;
    this.global_service.socket_page = "room";
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/chatroom']);
  }

  GoBack() {
    this.global_service.socket_page = "home";
    // this.socket.disconnect();
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  ConnectToNJS(name) {
    // if (this.global_service.list_socket == false) {
      this.global_service.list_socket = true;
      this.socket.connect();

      this.socket.on('disconnect', (reason) => {
        if (reason === 'io server disconnect') {
          this.socket.connect();
        }else{

          this.socket.connect();
        }
        // else the socket will automatically try to reconnect
      });

      this.socket.emit('reg_user', name);
      this.socket.fromEvent('message').subscribe(message => {
        if(this.alert_count <=0 && this.global_service.socket_page != "room"){
          this.alert_count++;
          if (this.global_service.socket_page == "list") {
            switch (this.current_tab) {
              case "first_tab":default:
                // //console.log(this.tab1);
                this.LoadLecturerData();
                break;
              case "second_tab":
                this.LoadUserCourseData();
                break;
            }
          }
          setTimeout(function(){ this.alert_count--; }, 500);
        }
      });
      this.socket.fromEvent('my_message').subscribe(message => {
        if(this.alert_count <=0 && this.global_service.socket_page != "room"){
          this.alert_count++;
          if (this.global_service.socket_page == "list") {
            switch (this.current_tab) {
              case "first_tab":
                // //console.log(this.tab1);
                this.LoadLecturerData();
                break;
              case "second_tab":
                this.LoadUserCourseData();
                break;
            }
          }
          setTimeout(function(){ this.alert_count--; }, 500);
        }
      });
    // }
  }
}
