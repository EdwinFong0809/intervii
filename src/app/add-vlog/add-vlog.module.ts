import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddVlogPageRoutingModule } from './add-vlog-routing.module';

import { AddVlogPage } from './add-vlog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddVlogPageRoutingModule
  ],
  declarations: [AddVlogPage]
})
export class AddVlogPageModule {}
