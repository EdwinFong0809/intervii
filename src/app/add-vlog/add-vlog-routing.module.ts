import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddVlogPage } from './add-vlog.page';

const routes: Routes = [
  {
    path: '',
    component: AddVlogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddVlogPageRoutingModule {}
