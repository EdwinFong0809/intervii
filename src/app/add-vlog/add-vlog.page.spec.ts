import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddVlogPage } from './add-vlog.page';

describe('AddVlogPage', () => {
  let component: AddVlogPage;
  let fixture: ComponentFixture<AddVlogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVlogPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddVlogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
