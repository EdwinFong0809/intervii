import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditorPagePage } from './editor-page.page';

const routes: Routes = [
  {
    path: '',
    component: EditorPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditorPagePageRoutingModule {}
