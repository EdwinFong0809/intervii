import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from '@ionic/angular';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-editor-page',
  templateUrl: './editor-page.page.html',
  styleUrls: ['./editor-page.page.scss'],
})
export class EditorPagePage implements OnInit {
  focus: boolean = false;
  page_title: string = "";
  content: string = "";
  carot_position: number = 0;
  detail: any;
  coursework: any;
  index: number;

  constructor(
    // private alert: AlertController,
    //  public viewCtrl: ViewController,
    public global_service: GlobalService,
     public navCtrl: NavController,
    //  public navParams: NavParams
     ) {
      this.page_title = "";
      this.detail = this.global_service.editor_title;
      this.content = this.global_service.editor_text;
      this.content = this.content.replace(/<br>/g, "\n");
      // this.content = "";
      this.carot_position = 0;
   }

  ngOnInit() {
  }

  click(input, in_text) {
    //console.log(input.selectionStart);
    //console.log(input.selectionEnd);
    let temp_content_part_1 = this.content.substring(0, input.selectionStart);
    let temp_content_part_2 = this.content.substring(input.selectionStart, input.selectionEnd);
    let temp_content_part_3 = this.content.substring(input.selectionEnd);
    //console.log("temp_content_part_1 : " + temp_content_part_1);
    //console.log("temp_content_part_2 : " + temp_content_part_2);
    //console.log("temp_content_part_3 : " + temp_content_part_3);
        let txt = '';
        let txt2 = '';
    switch (in_text) {
      case "type1":
        txt2 = '內容';
        if(this.global_service.lang == 'en'){
          txt2 = 'Content';
        }else if(this.global_service.lang == 'cn'){
          txt2 = '内容';
        }
        if (input.selectionStart == input.selectionEnd) {
          this.content = temp_content_part_1 + "<b>"+txt2+"</b>" + temp_content_part_3;
        } else {
          this.content = temp_content_part_1 + "<b>" + temp_content_part_2 + "</b>" + temp_content_part_3;
        }
        break;
      case "type2":
        txt2 = '內容';
        if(this.global_service.lang == 'en'){
          txt2 = 'Content';
        }else if(this.global_service.lang == 'cn'){
          txt2 = '内容';
        }
        if (input.selectionStart == input.selectionEnd) {
          this.content = temp_content_part_1 + "<i>"+txt2+"</i>" + temp_content_part_3;
        } else {
          this.content = temp_content_part_1 + "<i>" + temp_content_part_2 + "</i>" + temp_content_part_3;
        }
        break;
      case "type3":
        txt2 = '內容';
        if(this.global_service.lang == 'en'){
          txt2 = 'Content';
        }else if(this.global_service.lang == 'cn'){
          txt2 = '内容';
        }
        if (input.selectionStart == input.selectionEnd) {
          this.content = temp_content_part_1 + "<h2>"+txt2+"</h2>" + temp_content_part_3;
        } else {
          this.content = temp_content_part_1 + "<h2>" + temp_content_part_2 + "</h2>" + temp_content_part_3;
        }
        break;
      case "type4":
        txt2 = '內容';
        if(this.global_service.lang == 'en'){
          txt2 = 'Content';
        }else if(this.global_service.lang == 'cn'){
          txt2 = '内容';
        }
        if (input.selectionStart == input.selectionEnd) {
          this.content = temp_content_part_1 + "<p style='padding-left:100px;border-style: solid;border-color: #da4170;border-width: 0px 0px 0px 2px;'>"+txt2+"</p>" + temp_content_part_3;
        } else {
          this.content = temp_content_part_1 + "<p style='padding-left:100px;border-style: solid;border-color: #da4170;border-width: 0px 0px 0px 2px;'>" + temp_content_part_2 + "</p>" + temp_content_part_3;
        }
        break;
      case "type5":
        txt = '連結';
        txt2 = '內容';
        if(this.global_service.lang == 'en'){
          txt = 'link';
          txt2 = 'Content';
        }else if(this.global_service.lang == 'cn'){
          txt = '连结';
          txt2 = '内容';
        }
        if (input.selectionStart == input.selectionEnd) {
          this.content = temp_content_part_1 + "<a href='"+txt+"'>"+txt2+"</a>" + temp_content_part_3;
        } else {
          this.content = temp_content_part_1 + "<a href='"+txt+"'>" + temp_content_part_2 + "</a>" + temp_content_part_3;
        }
        break;
      case "type6":
        txt = '連結';
        if(this.global_service.lang == 'en'){
          txt = 'link';
        }else if(this.global_service.lang == 'cn'){
          txt = '连结';
        }
        this.content = temp_content_part_1 + "<img src='"+txt+"' >" + temp_content_part_3;
        break;
      case "type7":
        txt = '連結';
        if(this.global_service.lang == 'en'){
          txt = 'link';
        }else if(this.global_service.lang == 'cn'){
          txt = '连结';
        }
        this.content = temp_content_part_1 + "<video controls src='"+txt+"'></video>" + temp_content_part_3;
        break;
      case "type8":
        txt = '連結';
        if(this.global_service.lang == 'en'){
          txt = 'link';
        }else if(this.global_service.lang == 'cn'){
          txt = '连结';
        }
        this.content = temp_content_part_1 + "<iframe id='myFrame' src='"+txt+"'></iframe>" + temp_content_part_3;
        break;
    }
    input.focus();
  }
  field_press(input, from) {
    //console.log("from : " + from);
    if (input.selectionStart || input.selectionStart == '0') {
      //console.log("start : " + input.selectionStart);
      this.carot_position = input.selectionStart;
    }
  }
  focusInput(input) {
    input.setFocus();
  }

  Dismiss() {
    this.content = this.content.replace(/youtu.be/g, "youtube.com/embed");
    this.content = this.content.replace(/\n/g, "<br>");
    this.global_service.back_update = true;
    this.global_service.editor_text = this.content;
    this.global_service.resetLoading();
    this.navCtrl.pop();
    // this.navCtrl.navigateBack(this.global_service.back_url);
  }

  GoBack() {
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }
  Preview() {
    this.global_service.editor_preview_text = this.content.replace(/youtu.be/g, "youtube.com/embed");
    // this.navCtrl.push(InfoPreviewPage, { info: this.content });
        this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/editor-preview']);
  }

}
