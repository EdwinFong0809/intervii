import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditorPagePageRoutingModule } from './editor-page-routing.module';

import { EditorPagePage } from './editor-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditorPagePageRoutingModule
  ],
  declarations: [EditorPagePage]
})
export class EditorPagePageModule {}
