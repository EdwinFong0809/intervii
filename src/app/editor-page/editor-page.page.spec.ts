import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditorPagePage } from './editor-page.page';

describe('EditorPagePage', () => {
  let component: EditorPagePage;
  let fixture: ComponentFixture<EditorPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditorPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
