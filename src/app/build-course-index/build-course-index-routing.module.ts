import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseIndexPage } from './build-course-index.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseIndexPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseIndexPageRoutingModule {}
