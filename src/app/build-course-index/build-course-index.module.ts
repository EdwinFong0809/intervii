import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseIndexPageRoutingModule } from './build-course-index-routing.module';

import { BuildCourseIndexPage } from './build-course-index.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseIndexPageRoutingModule
  ],
  declarations: [BuildCourseIndexPage]
})
export class BuildCourseIndexPageModule {}
