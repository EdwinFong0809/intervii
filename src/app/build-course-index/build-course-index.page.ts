import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-build-course-index',
  templateUrl: './build-course-index.page.html',
  styleUrls: ['./build-course-index.page.scss'],
})
export class BuildCourseIndexPage implements OnInit {
  step_one: any;//title
  step_two: any;//target
  step_three: any;//unit
  step_four: any;//price
  step_five: any;//coursework
  step_six: any;//content
  step_seven: any;//url
  step_eight: any;//preview
  is_title_done: boolean = false;
  is_target_done: boolean = false;
  is_unit_done: boolean = false;
  is_price_done: boolean = false;
  is_coursework_done: boolean = false;
  is_content_done: boolean = false;
  is_url_done: boolean = false;
  is_preview_done: boolean = false;
  is_finished_btn_hidden: boolean = true;
  is_edit: boolean = false;
  user_id: any;
  course_data: any;
  incomplete_list: any = {
    "assignment_list":[], //coursework
    //"create_date":"2019-01-22T16:47:18Z",
    "data_type":"course_data",
    "days":0,//price
    "icon":"",//title-cover-image
  //"id":4,
    "intro":"", //title
    "learn_result":"", //target
    "lecturer_id":1,
    "preview_video":"",//preview
    "price":0,//price
    "profile":"",//??
    "require_knowledge":"", //target
    "require_tools":"", //target
    "status":"incomplete",
    "students":[],//ignore
    "tag":"",//content
    "target_student":"",//target
    "title":"", //title
    "video_list":[],//unit
    "website":""//url
  };

  constructor(
    public http: HTTP,
    private alert: AlertController,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    public actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }
  Pop() {
    this.global_service.create_course = null;
    this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  async ionViewWillEnter() {
    // this.viewCtrl.setBackButtonText('退出');
    //console.log('ionViewDidLoad BuildyourcourseCreatePage');
    this.user_id = this.global_service.user_id
    this.course_data = this.global_service.create_course;
    this.is_edit = this.global_service.edit_course;
    if(this.is_edit == undefined){
      this.is_edit = false;
    }
    //console.log(this.course_data);
    if(this.course_data == "user is not lecturer"){
      this.global_service.resetLoading();
      this.navCtrl.pop();
      let msg_title = "你必須先確認你的電子郵件";
      let msg_option_1 = "確認";
      if (this.global_service.lang == 'en') {
        msg_title = "Please verify email first";
        msg_option_1 = "Confirm";
      }
      if (this.global_service.lang == 'cn') {
        msg_title = "你必须先确认你的电子邮件";
        msg_option_1 = "确认";
      }
      const alert_box = await this.alert.create({
        message: msg_title,
        cssClass: 'custom-alertControl',
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }
    this.ContinueProcessRestoreStep();
  }
  async CheckAllStep(){
    if(this.is_title_done&&this.is_target_done&&this.is_unit_done&&this.is_price_done&&
    // this.is_coursework_done&&
    this.is_content_done&&this.is_url_done&&this.is_price_done){
      this.is_finished_btn_hidden = false;
      if(this.is_edit){
        let msg_title = "你已經完成了所有的步驟，但您仍然可以在上架前編輯每個步驟";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "You have completed all step, but you can still edit before release";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "你已经完成了所有的步骤，但您仍然可以在上架前编辑每个步骤";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
      }
    }
  }
  ContinueProcessRestoreStep(){
    let c = this.course_data;
    //console.log('c: '+c);
    //title
    if(c.intro!=""&&c.title!=""){
      this.is_title_done = true;
      //console.log('title occur')
    }
    //target
    if(c.learn_result!=""&&c.require_knowledge!=""&&c.require_tools!=""&&c.target_student!=""){
      this.is_target_done = true;
    }
    //unit
    if(c.video_list[0]!= undefined){
      this.is_unit_done = true;
    }
    //price
    if(c.days!=0){
      //price can be 0 ?
      this.is_price_done = true;
    }
    //coursework
    if(c.assignment_list[0]!=undefined){
      this.is_coursework_done = true;
    }
      // this.is_coursework_done = true;
    //Content
    if(c.tag != ""){
      this.is_content_done = true;
    }
    //Url
    if(c.website!="" && !((c.website.length < 8 || c.website.length > 20) || this.TextRemover(c.website) != "")){
      this.is_url_done = true;
    }
    //preview
  //   if(c.preview_video!=""){
  //   this.is_preview_done = true;
  // }
    this.CheckAllStep();
  }
  Finished(){
    let live_length = 0;


    let url = "https://intervii.com/backend/request.php";
    let body = { "get_videos": JSON.stringify(this.course_data.video_list) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        if (JSON.parse(data.data).result == "success") {
          // let return_data = this.multi_unit;
          // this.viewCtrl.dismiss(return_data);
          let videos = JSON.parse(data.data).data;
          //console.log(videos);
          for(let i=0;i<videos.length;i++){
            if(videos[i].is_live){
              live_length += videos[i].live_length;
            }
          }
        }
        if(live_length > 0 && !this.is_edit){
            // this.navCtrl.push(BuildyourcourseCreatePaymentPage, {course: this.course_data, live_length: live_length });
          this.ConfirmCourse();
        }else{
          this.ConfirmCourse();
        }
        this.global_service.dismiss();
      });

  }

  pressing = false;

  ConfirmCourse(){
    if(!this.pressing){
      this.pressing = true;
    let url = "https://intervii.com/backend/request.php";
    var json = { "id": this.course_data.id , "status": "completed" };
    let body = { "update_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.global_service.isLoading = true;
    this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
      //console.log(data.data);
      this.pressing = false;
        let temp = JSON.parse(data.data);
        this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/build-course-finish']);
        // this.navCtrl.setRoot(BuildyourcourseCreateFinishedPage);
        this.global_service.dismiss();
      });
    }
  }

  StepOneTitle() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-title']);
    // this.navCtrl.navigateForward(['/build-course-index']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreateTitlePage,{course: this.course_data });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_title_done = true;
  //   this.step_one = data;
  //   this.CheckAllStep();
  //   }
  // })
  // modal.present();
  }
  StepTwoTarget() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-target']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreateTargetPage,{course: this.course_data });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_target_done = true;
  //   this.step_two = data;
  //   }

  // })
  // modal.present();
  }
  StepThreeUnit() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-unit']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreateUnitPage,{course: this.course_data, course_id: this.course_data.id });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_unit_done = true;
  //   this.step_three = data;
  //   this.CheckAllStep();
  //   }
  // })
  // modal.present();
  }
  StepFourPrice() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-price']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreatePricePage,{course: this.course_data });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_price_done = true;
  //   this.step_four = data;
  //   this.CheckAllStep();
  //   }

  // })
  // modal.present();
  }
  StepFiveCoursework() {
    this.global_service.get_coursework = true;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-coursework']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreateCourseworkPage,{course_id: this.course_data.id,course: this.course_data });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_coursework_done = true;
  //   this.step_five = data;
  //   this.CheckAllStep();
  //   }
  // })
  // modal.present();
  }
  StepSixContent() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-content']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreateContentPage,{course: this.course_data });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_content_done = true;
  //   this.step_six = data;
  //   this.CheckAllStep();
  //   }
  // })
  // modal.present();
  }
  StepSevenUrl() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-url']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreateUrlPage,{course: this.course_data });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_url_done = true;
  //   this.step_seven = data;
  //   this.CheckAllStep();
  //   }

  // })
  // modal.present();
  }
  StepEightPreview() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-preview']);
  // const modal = this.modalCtrl.create(BuildyourcourseCreatePreviewPage,{course_id: this.course_data.id });
  // modal.onDidDismiss(data => {
  //   //console.log(data);
  //   if(data!= undefined){
  //   this.is_preview_done = true;
  //   this.step_eight = data;
  //   this.CheckAllStep();
  //   }

  // })
  // modal.present();
  }

  TextRemover(text) {
    return text.toLowerCase().replace(/[a-zA-Z0-9]+/g, "").replace(/-/g, "");
  }

}
