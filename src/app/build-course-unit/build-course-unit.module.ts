import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildCourseUnitPageRoutingModule } from './build-course-unit-routing.module';

import { BuildCourseUnitPage } from './build-course-unit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildCourseUnitPageRoutingModule
  ],
  declarations: [BuildCourseUnitPage]
})
export class BuildCourseUnitPageModule {}
