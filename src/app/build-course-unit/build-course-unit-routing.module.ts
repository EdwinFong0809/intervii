import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildCourseUnitPage } from './build-course-unit.page';

const routes: Routes = [
  {
    path: '',
    component: BuildCourseUnitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildCourseUnitPageRoutingModule {}
