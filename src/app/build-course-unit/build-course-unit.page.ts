import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'app-build-course-unit',
  templateUrl: './build-course-unit.page.html',
  styleUrls: ['./build-course-unit.page.scss'],
})
export class BuildCourseUnitPage implements OnInit {
  video_type: any;
  multi_unit: any;
  number_of_unit: any = 0;
  unit_count: any = 0;
  course: any;
  course_id: any;
  user_id: any;
  success_array: any = [];
  loading: any;
  loaded: boolean = false;
  uploading: boolean = false;
  button_text: string = "下一步";

  constructor(
    public http: HTTP,
    private alert: AlertController,
    // public player:VideoPlayer,
    private streamingMedia: StreamingMedia,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
    private camera: Camera,
    private file: File, 
    private filePath: FilePath,
    private transfer: FileTransfer,
    private photoViewer: PhotoViewer,
    public actionSheetCtrl: ActionSheetController,
  ) {
    if(this.global_service.lang == 'en'){
      this.button_text = "Next";
    }else if(this.global_service.lang == 'cn'){
      this.button_text = "下一步";
    }else{
      this.button_text = "下一步";
    }
  }

  ngOnInit() {
  }


  ionViewDidEnter() {
    //console.log('ionViewDidLoad BuildyourcourseCreateUnitPage');
    this.multi_unit = [
    ];
    this.course_id = this.global_service.create_course.id;
    this.success_array = [];
    this.course = this.global_service.create_course;
    //console.log(this.course);
    this.user_id = this.global_service.user_id;
    //load unit datas
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_all_type_videos": JSON.stringify(this.course.video_list) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        //console.log(data.data);
        if (JSON.parse(data.data).result == "success") {
          let videos = JSON.parse(data.data).data;
          let temp_multi_unit = [];
          //console.log(videos);
          for (let i = 0; i < videos.length; i++) {
            //console.log(videos[i]);
            let temp = {
              video_title: videos[i].title, video_type: "", recording_upload: "",
              live_streaming_date: "", live_streaming_period: 0,
              live_link:"",
              duration:0,
              id: videos[i].id,
              file_name: "",
              file_data: "",
              file_type: "",
              preview_file_name: "",
              preview_file_data: "",
              preview_file_type: ""
            };
            if (videos[i].file_name != undefined && videos[i].file_name != null && videos[i].file_name != "") {
              temp.file_name = videos[i].file_name;
            }
            if (videos[i].duration != undefined && videos[i].duration != null && videos[i].duration != "") {
              temp.duration = videos[i].duration;
            }
            if (videos[i].preview_file_name != undefined && videos[i].preview_file_name != null && videos[i].preview_file_name != "") {
              temp.preview_file_name = videos[i].preview_file_name;
            }
            if (videos[i].is_live) {
              temp.video_type = "live_streaming";
              if (videos[i].live_link != undefined && videos[i].live_link != null && videos[i].live_link != "") {
                temp.live_link = videos[i].live_link;
              }
              temp.live_streaming_period = videos[i].live_length;
              let temp_live_date = new Date(videos[i].live_date);
              temp_live_date.setHours(temp_live_date.getHours()-8);
              temp.live_streaming_date = temp_live_date.getFullYear()+"-"+(temp_live_date.getMonth()+1).toString().padStart(2, '0')+"-"+temp_live_date.getDate().toString().padStart(2, '0')+"T"+temp_live_date.getHours().toString().padStart(2, '0')+":"+temp_live_date.getMinutes().toString().padStart(2, '0')+":00Z";
            } else {
              temp.video_type = "recording";
            }
            temp_multi_unit.push(temp);
          }
          this.multi_unit = temp_multi_unit;
          this.number_of_unit = this.multi_unit.length;
          this.unit_count = 0;
          //console.log(this.multi_unit);
        }
        this.loaded = true;
      });
  }
  async Dismiss() {
    let is_data_pass = true;
    for (let i = 0; i < this.multi_unit.length; i++) {
      if (this.multi_unit[i].video_title == "") {
        let msg_title = "請輸入第";
        let msg_title_2 = "項標題";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Fill in ";
          msg_title_2 = " title";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "请输入第";
          msg_title_2 = "项标题";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title + (i + 1) + msg_title_2,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
        is_data_pass = false;
        break;
      }
      if (this.multi_unit[i].video_type == "") {
        let msg_title = "請選擇第";
        let msg_title_2 = "項影片類型";
        let msg_option_1 = "確認";
        if (this.global_service.lang == 'en') {
          msg_title = "Please select ";
          msg_title_2 = " video type";
          msg_option_1 = "Confirm";
        }
        if (this.global_service.lang == 'cn') {
          msg_title = "请选择第";
          msg_title_2 = "项影片类型";
          msg_option_1 = "确认";
        }
        const alert_box = await this.alert.create({
          message: msg_title + (i + 1) + msg_title_2,
          cssClass: 'custom-alertControl',
          backdropDismiss:false,
          buttons: [msg_option_1]
        });
        await alert_box.present();
        is_data_pass = false;
        break;
      } else if (this.multi_unit[i].video_type == "recording") {
        //console.log(i + 'recording');
      } else if (this.multi_unit[i].video_type == "live_streaming") {
        if (this.multi_unit[i].live_streaming_period < 0) {
          let msg_title = "第";
          let msg_title_2 = "項影片預計長度(分鐘)不能為負數";
          let msg_option_1 = "確認";
          if (this.global_service.lang == 'en') {
            msg_title = "";
            msg_title_2 = " video prefer length (minutes) can't smaller than 0";
            msg_option_1 = "Confirm";
          }
          if (this.global_service.lang == 'cn') {
            msg_title = "第";
            msg_title_2 = "项影片预计长度(分钟)不能为负数";
            msg_option_1 = "确认";
          }
          const alert_box = await this.alert.create({
            message: msg_title + (i + 1) + msg_title_2,
            cssClass: 'custom-alertControl',
            backdropDismiss:false,
            buttons: [msg_option_1]
          });
          await alert_box.present();
          is_data_pass = false;
          break;
        }
        //console.log(i + 'live_streaming');
      }
    }
    if (this.uploading == false && is_data_pass && this.multi_unit.length > 0) {
      this.uploading = true;
      this.number_of_unit = this.multi_unit.length;
      this.unit_count = 0;
      this.UploadData();
    }

  }
  UploadData() {
    //console.log("UploadData");
    let is_live;
    if (this.multi_unit[this.unit_count].video_type == "live_streaming") {
      is_live = true;
    } else {
      is_live = false;
    }
    let url = "https://intervii.com/backend/request.php";
    var json;
    if (is_live) {
      let temp_live_date = new Date(this.multi_unit[this.unit_count].live_streaming_date);
      temp_live_date.setHours(temp_live_date.getHours()-8);
      let date_string = temp_live_date.getFullYear()+"-"+(temp_live_date.getMonth()+1).toString().padStart(2, '0')+"-"+temp_live_date.getDate().toString().padStart(2, '0')+"T"+temp_live_date.getHours().toString().padStart(2, '0')+":"+temp_live_date.getMinutes().toString().padStart(2, '0')+":00";
      json = {
        "id": this.multi_unit[this.unit_count].id,
        "user_id": this.user_id,
        "title": this.multi_unit[this.unit_count].video_title,
        // "is_preview": false,
        "is_live": is_live,
        "live_length": parseInt(this.multi_unit[this.unit_count].live_streaming_period),
        "live_date": date_string,
        "live_link":this.multi_unit[this.unit_count].live_link,
      };
    } else {
      json = {
        "id": this.multi_unit[this.unit_count].id,
        "user_id": this.user_id,
        "title": this.multi_unit[this.unit_count].video_title,
        // "is_preview": false,
        "is_live": is_live,
        "duration": this.multi_unit[this.unit_count].duration,
        "file_type": this.multi_unit[this.unit_count].file_type,
        "file_name": this.multi_unit[this.unit_count].file_name,
        // "file_data": this.multi_unit[this.unit_count].file_data,
        "preview_file_type": this.multi_unit[this.unit_count].preview_file_type,
        "preview_file_name": this.multi_unit[this.unit_count].preview_file_name//,
        // "preview_file_data": this.multi_unit[this.unit_count].preview_file_data
      };
    }
    let body;
    if (this.multi_unit[this.unit_count].id > -1) {
      body = { "update_video": JSON.stringify(json) };
    } else {
      body = { "new_video": JSON.stringify(json) };
    }
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        let temp = JSON.parse(data.data);
        if (temp.result == "success") {
          this.success_array.push(temp.data.id);
          this.unit_count++;
          if (this.unit_count <= this.number_of_unit-1) {
            this.UploadData();
          } else {
            this.UpdateFinalList();
          }

        } else {
          //update fail
          //console.log('Update ' + this.number_of_unit + ' Fail');
          this.unit_count++;
          if (this.unit_count <= this.number_of_unit-1) {
            this.UploadData();
          } else {
            //do nothing
            this.uploading = false;
          }
        }
      });
  }
  UpdateFinalList() {
    this.course.video_list = this.success_array;
    let url = "https://intervii.com/backend/request.php";
    var json = {
      "id": this.course_id,
      "video_list": this.course.video_list
    };
    let body = { "update_course": JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.http.post(url, body, headers)
      .then(data => {
        this.uploading = false;
        if (JSON.parse(data.data).result == "success") {
          let return_data = this.multi_unit;
          this.global_service.create_course = this.course;
          // this.viewCtrl.dismiss(return_data);
        this.global_service.resetLoading();
          this.navCtrl.pop();
        }
      });
  }
  GoBack() {
    // this.viewCtrl.dismiss();
    this.navCtrl.pop();
  }
  AddUnit() {
    let new_data = {
      video_title: "", video_type: "", recording_upload: "",
      live_streaming_date: "", live_streaming_period: 0,
      id: -1
    };
    this.global_service.edit_course_unit = new_data;
    // this.multi_unit.push(new_data);
    // //console.log(this.multi_unit);
        this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-unit-single']);
  }
  EditUnit(index) {
    this.global_service.edit_course_unit = this.multi_unit[index];
    // this.multi_unit.push(new_data);
    // //console.log(this.multi_unit);
        this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/build-course-unit-single']);
  }

  CallLoadVideo(index) {
    this.CallLoadVideoDo(index);
  }
  CallLoadVideoDo(index) {
    var options = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      // alert(data);


      let file_data = data;
      if (this.plt.is('android')) {
                let url = "https://intervii.com/backend/request.php";
                let body = { "new_video_file_id": "" };
                //let body = { "current_datetime_string" : ""};
                let headers = {
                  'Content-Type': 'application/x-www-form-urlencoded'
                };
                this.http.post(url, body, headers)
                  .then(data => {
                    //console.log(data.data);
                    let name_parts = file_data.split('/');
                    let name_parts_sub = name_parts[name_parts.length - 1].split('.');
                    let file_type = name_parts_sub[name_parts_sub.length - 1];
                    if(file_type == 'MOV'){
                      file_type = 'mp4';
                    }
                    let file_name = data.data + "." + file_type;
                    let options: FileUploadOptions = {
                      chunkedMode: false,
                      fileKey: 'fileToUpload',
                      fileName: file_type,
                      httpMethod: 'POST',
                      params: { "file_new_name": file_name }
                    };
                    this.multi_unit[index].file_name = file_name;
                    this.multi_unit[index].file_type = file_type;

                    const fileTransfer: FileTransferObject = this.transfer.create();
                    let msg_option_1 = "上載中";
                    if (this.global_service.lang == 'en') {
                      msg_option_1 = "Uploading";
                    }
                    if (this.global_service.lang == 'cn') {
                      msg_option_1 = "上载中";
                    }
                    this.button_text = msg_option_1+'...0%';
                    this.uploading = true;
                    fileTransfer.onProgress((progressEvent) => {
                        if (progressEvent.lengthComputable) {
                          // //console.log(progressEvent);
                          this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                        } else {
                        }
                    });
                    fileTransfer.upload(file_data, 'https://intervii.com/upload.php', options)
                      .then((data) => {
                        // success
                        // if(data)
                        this.uploading = false;
                        if (data.response != "success") {
                          let msg_title = "上傳失敗";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload fail";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载失败";
                          }
                          alert(msg_title);
                        } else {
                          let msg_title = "上傳成功";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload success";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载成功";
                          }
                          alert(msg_title);
                        }
                        if(this.global_service.lang == 'en'){
                          this.button_text = "Next";
                        }else if(this.global_service.lang == 'cn'){
                          this.button_text = "下一步";
                        }else{
                          this.button_text = "下一步";
                        }
                      }, (err) => {
                        // error
                        this.uploading = false;
                        let msg_title = "上傳失敗";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload fail";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载失败";
                        }
                        alert(msg_title);
                        if(this.global_service.lang == 'en'){
                          this.button_text = "Next";
                        }else if(this.global_service.lang == 'cn'){
                          this.button_text = "下一步";
                        }else{
                          this.button_text = "下一步";
                        }
                        // alert(JSON.stringify(err));
                      });
                  });

      } else {
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.http.post(url, body, headers)
          .then(data => {
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if(file_type == 'MOV'){
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.multi_unit[index].file_name = file_name;
            this.multi_unit[index].file_type = file_type;

            const fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            fileTransfer.onProgress((progressEvent) => {
                if (progressEvent.lengthComputable) {
                  // //console.log(progressEvent);
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                } else {
                }
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload.php', options)
              .then((data) => {
                // success
                // if(data)
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  let msg_title = "上傳成功";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload success";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载成功";
                  }
                  alert(msg_title);
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
              }, (err) => {
                // error
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
              });
          });
      }
    }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }
  CallLoadPreviewVideo(index) {
    this.CallLoadPreviewVideoDo(index);
  }
  CallLoadPreviewVideoDo(index) {
    var options = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      let file_data = data;
      if (this.plt.is('android')) {
                let url = "https://intervii.com/backend/request.php";
                let body = { "new_video_file_id": "" };
                //let body = { "current_datetime_string" : ""};
                let headers = {
                  'Content-Type': 'application/x-www-form-urlencoded'
                };
                this.global_service.isLoading = true;
                this.global_service.presentLoading();
                this.http.post(url, body, headers)
                  .then(data => {
                    let name_parts = file_data.split('/');
                    let name_parts_sub = name_parts[name_parts.length - 1].split('.');
                    let file_type = name_parts_sub[name_parts_sub.length - 1];
                    if(file_type == 'MOV'){
                      file_type = 'mp4';
                    }
                    let file_name = data.data + "." + file_type;
                    let options: FileUploadOptions = {
                      chunkedMode: false,
                      fileKey: 'fileToUpload',
                      fileName: file_type,
                      httpMethod: 'POST',
                      params: { "file_new_name": file_name }
                    };
                    this.multi_unit[index].preview_file_name = file_name;
                    this.multi_unit[index].preview_file_type = file_type;
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    let msg_option_1 = "上載中";
                    if (this.global_service.lang == 'en') {
                      msg_option_1 = "Uploading";
                    }
                    if (this.global_service.lang == 'cn') {
                      msg_option_1 = "上载中";
                    }
                    this.button_text = msg_option_1+'...0%';
                    this.uploading = true;
                    fileTransfer.onProgress((progressEvent) => {
                        if (progressEvent.lengthComputable) {
                          // //console.log(progressEvent);
                          this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                        } else {
                        }
                    });
                    fileTransfer.upload(file_data, 'https://intervii.com/upload.php', options)
                      .then((data) => {
                        // success
                        this.uploading = false;
                        if (data.response != "success") {
                          let msg_title = "上傳失敗";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload fail";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载失败";
                          }
                          alert(msg_title);
                        } else {
                          let msg_title = "上傳成功";
                          if (this.global_service.lang == 'en') {
                            msg_title = "Upload success";
                          }
                          if (this.global_service.lang == 'cn') {
                            msg_title = "上载成功";
                          }
                          alert(msg_title);
                        }
                        if(this.global_service.lang == 'en'){
                          this.button_text = "Next";
                        }else if(this.global_service.lang == 'cn'){
                          this.button_text = "下一步";
                        }else{
                          this.button_text = "下一步";
                        }
                        this.global_service.dismiss();
                        // alert(JSON.stringify(data));
                      }, (err) => {
                        // error
                        this.uploading = false;
                        let msg_title = "上傳失敗";
                        if (this.global_service.lang == 'en') {
                          msg_title = "Upload fail";
                        }
                        if (this.global_service.lang == 'cn') {
                          msg_title = "上载失败";
                        }
                        alert(msg_title);
                        if(this.global_service.lang == 'en'){
                          this.button_text = "Next";
                        }else if(this.global_service.lang == 'cn'){
                          this.button_text = "下一步";
                        }else{
                          this.button_text = "下一步";
                        }
                        this.global_service.dismiss();
                      });
                  });
      }else{
        let url = "https://intervii.com/backend/request.php";
        let body = { "new_video_file_id": "" };
        //let body = { "current_datetime_string" : ""};
        let headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };
        this.global_service.isLoading = true;
        this.global_service.presentLoading();
        this.http.post(url, body, headers)
          .then(data => {
            //console.log(data.data);
            let name_parts = file_data.split('/');
            let name_parts_sub = name_parts[name_parts.length - 1].split('.');
            let file_type = name_parts_sub[name_parts_sub.length - 1];
            if(file_type == 'MOV'){
              file_type = 'mp4';
            }
            let file_name = data.data + "." + file_type;
            let options: FileUploadOptions = {
              chunkedMode: false,
              fileKey: 'fileToUpload',
              fileName: file_type,
              httpMethod: 'POST',
              params: { "file_new_name": file_name }
            };
            this.multi_unit[index].preview_file_name = file_name;
            this.multi_unit[index].preview_file_type = file_type;

            const fileTransfer: FileTransferObject = this.transfer.create();
            let msg_option_1 = "上載中";
            if (this.global_service.lang == 'en') {
              msg_option_1 = "Uploading";
            }
            if (this.global_service.lang == 'cn') {
              msg_option_1 = "上载中";
            }
            this.button_text = msg_option_1+'...0%';
            this.uploading = true;
            fileTransfer.onProgress((progressEvent) => {
                if (progressEvent.lengthComputable) {
                  // //console.log(progressEvent);
                  this.button_text = msg_option_1+'...' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
                } else {
                }
            });
            fileTransfer.upload(file_data, 'https://intervii.com/upload.php', options)
              .then((data) => {
                // success
                this.uploading = false;
                if (data.response != "success") {
                  let msg_title = "上傳失敗";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload fail";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载失败";
                  }
                  alert(msg_title);
                } else {
                  let msg_title = "上傳成功";
                  if (this.global_service.lang == 'en') {
                    msg_title = "Upload success";
                  }
                  if (this.global_service.lang == 'cn') {
                    msg_title = "上载成功";
                  }
                  alert(msg_title);
                }
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                // alert(JSON.stringify(data));
                this.global_service.dismiss();
              }, (err) => {
                // error
                this.uploading = false;
                let msg_title = "上傳失敗";
                if (this.global_service.lang == 'en') {
                  msg_title = "Upload fail";
                }
                if (this.global_service.lang == 'cn') {
                  msg_title = "上载失败";
                }
                alert(msg_title);
                if(this.global_service.lang == 'en'){
                  this.button_text = "Next";
                }else if(this.global_service.lang == 'cn'){
                  this.button_text = "下一步";
                }else{
                  this.button_text = "下一步";
                }
                this.global_service.dismiss();
              });
          });
      }


        }, (err) => {
      // Handle error
      //console.log("Camera issue:" + err);
    });
  }


  DeleteUnit(index) {

    if (this.number_of_unit == 0) {
      // this.alert.create({
      //   message: '數量不能小於0',
      //   cssClass: 'custom-alertControl',
      //   buttons: ['確認']
      // }).present();
    } else {
      this.multi_unit.splice(index, 1);
      this.number_of_unit--;
    }
  }

  async PlayVideo(index){
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played') },
        errorCallback: (e) => { console.log('Error streaming') },
        orientation: 'landscape',
        shouldAutoClose: true,
        controls: true
      };

      this.streamingMedia.playVideo(('https://intervii.com/media/'+this.multi_unit[index].file_name), options);
  }
  async PlayPreviewVideo(index){

        let options: StreamingVideoOptions = {
          successCallback: () => { console.log('Video played') },
          errorCallback: (e) => { console.log('Error streaming') },
          orientation: 'landscape',
          shouldAutoClose: true,
          controls: true
        };

        this.streamingMedia.playVideo(('https://intervii.com/media/'+this.multi_unit[index].preview_file_name), options);
  }
}
