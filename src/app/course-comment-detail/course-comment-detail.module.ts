import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourseCommentDetailPageRoutingModule } from './course-comment-detail-routing.module';

import { CourseCommentDetailPage } from './course-comment-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourseCommentDetailPageRoutingModule
  ],
  declarations: [CourseCommentDetailPage]
})
export class CourseCommentDetailPageModule {}
