import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CourseCommentDetailPage } from './course-comment-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CourseCommentDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CourseCommentDetailPageRoutingModule {}
