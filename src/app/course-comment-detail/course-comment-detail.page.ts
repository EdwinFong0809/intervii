import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, NavParams, AlertController, Platform, IonTabs, IonButton, ActionSheetController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { GlobalService } from "../global.service";

@Component({
  selector: 'app-course-comment-detail',
  templateUrl: './course-comment-detail.page.html',
  styleUrls: ['./course-comment-detail.page.scss'],
})
export class CourseCommentDetailPage implements OnInit {
  comment_data: any = [];

  constructor(
    public http: HTTP,
    private alert: AlertController,
    public loadingCtrl: LoadingController,
    public plt: Platform,
    public navCtrl: NavController,
    public global_service: GlobalService,
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    //console.log('ionViewDidEnter HomeLatestCourseCommentPage');
    this.comment_data = this.global_service.comment_detail;
  }

  GoBack() {
    // this.viewCtrl.dismiss();
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }
}
