import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Platform, IonTabs, IonButton } from '@ionic/angular';
import { EventsService } from '../events.service';
import { HTTP } from "@ionic-native/http/ngx";
import { Storage } from "@ionic/storage";
import { GlobalService } from "../global.service";
import { DomSanitizer } from '@angular/platform-browser';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal/ngx";
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.page.html',
  styleUrls: ['./course-detail.page.scss'],
})
export class CourseDetailPage implements OnInit {
  segment_value = "0";
  selectedTab = 0;
  course_detail: any = {
    course_price: 200
  };
  course_data: any = {};
  course_id: any = {
    id: 0,
    title: "",
    views: 0,
    rate: [],
    src: ""
  };
  price_list: any = [38,53,108,0,248,488,988,1488,2688,4988];
  logined: boolean = false;
  current_live: boolean = false;
  open_course: any = -1;
  live_id: any;
  user_data: any = {
    "assignment_list": [],
    "create_date": "2020-02-04T13:08:15Z",
    "data_type": "course_data",
    "days": 0,
    "followers": [],
    "has_live": false,
    "has_preview": false,
    "has_video": false,
    "icon": "",
    "id": 0,
    "intro": "",
    "learn_result": "",
    "lecturer_id": 0,
    "preview_video": "",
    "price": 5,
    "profile": " ",
    "require_knowledge": "",
    "require_tools": "",
    "status": "incomplete",
    "students": [],
    "tag": "",
    "target_student": "",
    "title": "",
    "total_duration": 0,
    "course_chapter_num": 0,
    "video_list": [],
    "website": ""
  };
  purchase: boolean = false;
  reviewed: boolean = true;
  need_update: boolean = false;
  has_assignment: boolean = false;

  iosProducts = [];
  iosMatchedProduct: any;
  currentPlatform = null;

  image_heart: any = [];
  videos: any = [];
  has_intro: boolean = false;
  has_profile: boolean = false;
  loaded: boolean = false;

  coursework_data: any = [];
  comment_data: any = [];
  selected_result: any = -1;

  current_tab: string = "first_tab";


  question_data: any = [];
  all_question_data: any = [];

  coursework_list: any = [];
  temp_coursework_list: any = [];
  selected_coursework: any;
  coursework_student_result: any = [];
  current_assignment: any = null;
  search_term: string = "";

  live_link: string = "";
  pressed: boolean = false;

  constructor(
    // private iap: InAppPurchase,
    private iap: InAppPurchase,
    public platform: Platform,
    private storage: Storage,
    public http: HTTP,
    public navCtrl: NavController,
    private streamingMedia: StreamingMedia,
    // public viewCtrl: ViewController,
    // public app: App,
    // public navParams: NavParams,
    private iab: InAppBrowser,
    private payPal: PayPal,
    private alertCtrl: AlertController,
    public global_service: GlobalService,
    private sanitizer: DomSanitizer,
    public events: EventsService,
  ) { }

  ngOnInit() {
  }

  pressBack(){
    console.log("pressBack");
    if(this.global_service.from_follow){
      this.global_service.from_follow = false;
      this.events.publishSomeData({data:"refresh_follow"});
    }
  }

  ionViewDidEnter() {
    this.price_list = this.global_service.price_list;
    this.LoadData();

    if (this.platform.is("ios")) {
      this.currentPlatform = "ios";
    }
    else{
    // if (this.platform.is("android")) {
      this.currentPlatform = "android";
    }

    this.platform.ready().then(() => {
      if (this.platform.is("ios")) {
        this.iap
          .getProducts([
            "hk.com.Intervii.course_type_1",
            "hk.com.Intervii.course_type_2",
            "hk.com.Intervii.course_type_3",
            "hk.com.Intervii.course_type_4",
            "hk.com.Intervii.course_type_5",
            "hk.com.Intervii.course_type_6",
            "hk.com.Intervii.course_type_7",
            "hk.com.Intervii.course_type_8",
            "hk.com.Intervii.course_type_9",
          ])
          .then(products => {
            console.log("products: ", products);
            this.iosProducts = products;

            //console.log("Course price: " + this.course_data.price);
            //console.log("Course data: " + this.course_data);
            this.iosProducts.forEach(product => {
              //console.log(
              //   "course p: " +
              //   this.course_data.price +
              //   "   " +
              //   "ios p: " +
              //   product.priceAsDecimal
              // );
              console.log(product);
              if (this.course_data.price >= this.price_list.length) {
                this.course_data.price = this.price_list.length - 1;
              }
              if (product.priceAsDecimal == this.price_list[this.course_data.price]) {
                this.iosMatchedProduct = product;
                console.log("matched product: " + this.iosMatchedProduct);
              }
            });
          })
          .catch(err => {
            //console.log(err);
          });
      }
    });
  }

  buy(product) {
    this.iap
      .buy(product)
      .then(data => {
        //console.log(data);
        //console.log(data.receipt);
        //console.log(data.transactionId);
        // this.enableItems(product);
      })
      .catch(err => {
        //console.log(err);
      });
  }
  GoBack() {
    // this.navCtrl.navigateBack('/tabs/tab4');
        this.global_service.resetLoading();
    this.navCtrl.pop();
  }

  public LoadData() {
    ////console.log('constructor HomeLatestCoursePage');
    this.course_data = this.global_service.course_data;
    this.purchase = false;
    this.logined = false;
    this.user_data = { id: 0 };
    // //console.log(this.navParams);
    //console.log("this.course_data");
    //console.log(this.course_data);
    if (
      Array.isArray(this.course_data.assignment_list) &&
      this.course_data.assignment_list.length > 0
    ) {
      this.has_assignment = true;
    }
    //info
    this.course_data.intro = this.sanitizer.bypassSecurityTrustHtml(this.course_data.intro);
    this.image_heart = this.course_data.rate;
    if (this.course_data.video_list != undefined) {
      this.course_data.course_chapter_num = this.course_data.video_list.length;
    } else {
      this.course_data.course_chapter_num = 0;
    }
    if (this.course_data.intro != undefined && this.course_data.intro != "" && this.course_data.intro.length > 0) {
      this.has_intro = true;
    }
    if (this.course_data.profile != undefined && this.course_data.profile != "" && this.course_data.profile.length > 0) {
      this.has_profile = true;
    }

    this.course_data.purchase = this.purchase;
    //console.log(JSON.stringify(this.global_service.user_data));
    // this.storage.get("user_data").then(val => {
    if (this.global_service.user_data != undefined && this.global_service.user_data != null) {
      this.user_data = this.global_service.user_data;
      this.loadPurchase();
      this.loadReview();
      this.logined = true;
    } else {
      this.user_data = { id: 0 };
      this.logined = false;
    }

    this.videos = [];
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_videos": JSON.stringify(this.course_data.video_list) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        // //console.log(data.data);
        this.videos = [];
        if (JSON.parse(data.data).result == "success") {
          // let return_data = this.multi_unit;
          // this.viewCtrl.dismiss(return_data);
          this.videos = JSON.parse(data.data).data;
          //console.log(this.videos);
          for (var i = 0; i < this.videos.length; i++) {
            if (this.videos[i].live_link != undefined && this.videos[i].live_link != null) {
              let live_date = new Date(this.videos[i].live_date);
              live_date.setHours(live_date.getHours() - 8);
              this.videos[i].display_live_date = "";
              this.videos[i].display_live_date += live_date.getFullYear() + "-" + (live_date.getMonth() + 1).toString().padStart(2, '0') + "-" + live_date.getDate().toString().padStart(2, '0') + " " + live_date.getHours().toString().padStart(2, '0') + ":" + live_date.getMinutes().toString().padStart(2, '0');

              // live_date.setHours(live_date.getHours()+8);
              let end_date = new Date(this.videos[i].live_date);
              end_date.setMinutes(live_date.getMinutes() + this.videos[i].live_length);
              let now_date = new Date();
              now_date.setHours(now_date.getHours() + 8);
              if (now_date > live_date && now_date < end_date) {
                this.current_live = true;
                this.live_link = this.videos[i].live_link;
              }
              if(now_date > live_date && now_date > end_date) {
                this.videos[i].pass_live = true;
              }else{
                this.videos[i].pass_live = false;
              }
            }
          }
        }
      });
    // });

    this.coursework_data = [];

    let coursework_body = { "get_assignments": JSON.stringify(this.global_service.course_data.assignment_list) };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, coursework_body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        this.coursework_data = [];
        if (JSON.parse(data.data).result == "success") {
          // let return_data = this.multi_unit;
          // this.viewCtrl.dismiss(return_data);
          let assignments = JSON.parse(data.data).data;
          for (let i = 0; i < assignments.length; i++) {
            let temp = {
              coursework_title: "", coursework_detail: "",
              file_name: "", extra_file_name: "",
              is_coursework_expend: false
            };
            temp.coursework_title = assignments[i].title;
            temp.coursework_detail = assignments[i].detail;
            temp.file_name = assignments[i].file_name;
            temp.extra_file_name = assignments[i].extra_file_name;
            this.coursework_data.push(temp);
            this.CloseAll();
          }
        }
      });

    this.comment_data = [];
    let body3 = { "get_course_reviews": JSON.stringify({ "course_id": this.global_service.course_data.id }) };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body3, headers)
      .then(data => {
        // this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        //console.log(data.data);
        if (temp.result == "success") {

          let ids = [];

          for (let i = 0; i < temp.data.length; i++) {
            temp.data[i].display_name = '';
            temp.data[i].user_icon = '';
            temp.data[i].content = temp.data[i].content.replace(/\n/g, '<br \/>');
            ids.push(temp.data[i].user_id);
          }
          this.comment_data = temp.data;

          let body2 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": ids }) };
          // this.global_service.isLoading = true;
          // this.global_service.presentLoading();
          this.http.post(url, body2, headers)
            .then(data => {
              // this.global_service.dismiss();
              let temp2 = JSON.parse(data.data);
              //console.log(data.data);
              if (temp2.result == "success") {
                let id_profile = [];
                for (let i = 0; i < temp2.data.length; i++) {
                  id_profile[temp2.data[i].id] = temp2.data[i];
                }
                //console.log(id_profile);
                //console.log(this.comment_data);
                for (let i = 0; i < this.comment_data.length; i++) {
                  this.comment_data[i].display_name = id_profile[this.comment_data[i].user_id].display_name;
                  this.comment_data[i].user_icon = id_profile[this.comment_data[i].user_id].icon;
                }
              }
            });
        }
      });

    this.loadResult();

    this.course_id.id = this.course_data.id;
    this.course_id.title = this.course_data.titles;
    this.course_id.views = this.course_data.views;
    this.course_id.rate = this.course_data.rate;
    this.course_id.src = this.course_data.icon;

    this.storage.get("ios_purchased").then(val => {
      if (val != undefined && val != null) {
        this.purchaseClassIOS_UnfinishedPurchase(
          val.course_id,
          val.receipt,
          val.transaction_id
        );
      }
    });
    this.loaded = true;
  }
  segmentChanged($event) {
    //console.log($event.detail.value);
    this.current_tab = $event.detail.value;
  }
  loadPurchase() {
    let url = "https://intervii.com/backend/request.php";
    var json = { course_id: this.course_data.id, user_id: this.user_data.id };
    let body = { check_course: JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded"
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers).then(data => {
      // this.global_service.dismiss();
      let temp = JSON.parse(data.data);
      if (temp.result == "success") {
        this.purchase = true;
        this.course_data.purchase = this.purchase;
      }
      this.loadQuestions();
    });
  }
  AddFavouriteCourse(id) {
    if (this.user_data != undefined && this.user_data != null && this.user_data.id != 0 && this.user_data.id != undefined) {
      //console.log(this.user_data);

      let url = "https://intervii.com/backend/request.php";
      let body = { "follow_course": JSON.stringify({ "user_id": this.global_service.user_data.id, "course_id": id }) };
      //let body = { "current_datetime_string" : ""};
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      //console.log("==================add favourite body ");
      //console.log(body);
      // this.global_service.isLoading = true;
      // this.global_service.presentLoading();
      this.http.post(url, body, headers)
        .then(data => {
          // this.global_service.dismiss();
          let temp = JSON.parse(data.data);
          //console.log(data.data);
          if (temp.result == "success") {
            this.user_data.follow_course = temp.data;
            this.global_service.user_data = this.user_data;
            // this.storage.set('user_data', this.user_data);


              if(this.course_data.id == id){
                if(this.user_data.follow_course.includes(id)){
                  if(!this.course_data.followers.includes(this.user_data.id)){
                    this.course_data.followers.push(this.user_data.id);
                  }
                }else{
                  if(this.course_data.followers.includes(this.user_data.id)){
                    let index = this.course_data.followers.indexOf(this.user_data.id, 0);
                    if (index > -1) {
                      this.course_data.followers.splice(index, 1);
                    }        
                  }
                }
              }
          }
          //console.log(this.user_data);
        });
    }
  }

  loadQuestions() {

    let url = "https://intervii.com/backend/request.php";
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded"
    };
    this.question_data = [];
    this.all_question_data = [];
    let body4 = { "get_course_comments": JSON.stringify({ "course_id": this.global_service.course_data.id }) };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body4, headers)
      .then(data => {
        // this.global_service.dismiss();
        let temp = JSON.parse(data.data);
        //console.log(data.data);
        if (temp.result == "success") {

          let ids = [];

          for (let i = 0; i < temp.data.length; i++) {
            temp.data[i].display_name = '';
            temp.data[i].user_icon = '';
            temp.data[i].hide = false;
            ids.push(temp.data[i].user_id);
          }
          this.all_question_data = temp.data;
          this.question_data = this.all_question_data;

          let body2 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": ids }) };
          // this.global_service.isLoading = true;
          // this.global_service.presentLoading();
          this.http.post(url, body2, headers)
            .then(data => {
              // this.global_service.dismiss();
              let temp2 = JSON.parse(data.data);
              // //console.log(data.data);
              if (temp2.result == "success") {
                let id_profile = [];
                let current_time = Date.now();
                for (let i = 0; i < temp2.data.length; i++) {
                  id_profile[temp2.data[i].id] = temp2.data[i];
                }

                console.log(this.all_question_data);
                console.log(id_profile);
                for (let i = 0; i < this.all_question_data.length; i++) {
                  this.all_question_data[i].display_name = id_profile[this.all_question_data[i].user_id].display_name;
                  this.all_question_data[i].user_icon = id_profile[this.all_question_data[i].user_id].icon;
                  this.all_question_data[i].question_time = this.all_question_data[i].create_date.replace('T', ' ').replace('Z', ' ');
                }
                this.question_data = this.all_question_data;
              }
            });
        }
      });
  }

  loadReview() {
    let url = "https://intervii.com/backend/request.php";
    var json = { course_id: this.course_data.id, user_id: this.user_data.id };
    let body = { check_review: JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded"
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers).then(data => {
      ////console.log(data.data);
      // this.global_service.dismiss();
      let temp = JSON.parse(data.data);
      if (temp.result == "success") {
        if (temp.data == "empty") {
          this.reviewed = false;
        }
      } else {
        this.reviewed = false;
      }
    });
  }

  loadResult() {
    //console.log("loadResult");
    this.temp_coursework_list = [];
    this.coursework_student_result = [];
    let url = "https://intervii.com/backend/request.php";
    let body = { "get_assignments": JSON.stringify(this.global_service.course_data.assignment_list) };
    let headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers)
      .then(data => {
        // this.global_service.dismiss();
        //console.log(data.data);
        if (JSON.parse(data.data).result == "success") {
          let assignments = JSON.parse(data.data).data;
          this.temp_coursework_list = assignments;
          let body2 = { "get_assignments_record_of_course": this.global_service.course_data.id };
          // this.global_service.isLoading = true;
          // this.global_service.presentLoading();
          this.http.post(url, body2, headers)
            .then(data => {
              // this.global_service.dismiss();
              //console.log(data.data);
              if (JSON.parse(data.data).result == "success") {
                let assignments = JSON.parse(data.data).data;

                let ids = [];

                for (let i = 0; i < assignments.length; i++) {
                  assignments[i].display_name = '';
                  assignments[i].user_icon = '';
                  assignments[i].content = assignments[i].content.replace(/\n/g, '<br \/>');
                  ids.push(assignments[i].user_id);
                }
                this.coursework_student_result = assignments;

                let body3 = { "get_user_by_id_list": JSON.stringify({ "user_id_list": ids }) };
                // this.global_service.isLoading = true;
                // this.global_service.presentLoading();
                this.http.post(url, body3, headers)
                  .then(data => {
                    // this.global_service.dismiss();
                    let temp3 = JSON.parse(data.data);
                    //console.log(data.data);
                    if (temp3.result == "success") {
                      let id_profile = [];
                      for (let i = 0; i < temp3.data.length; i++) {
                        id_profile[temp3.data[i].id] = temp3.data[i];
                      }
                      for (let i = 0; i < this.coursework_student_result.length; i++) {
                        this.coursework_student_result[i].display_name = id_profile[this.coursework_student_result[i].user_id].display_name;
                        this.coursework_student_result[i].user_icon = id_profile[this.coursework_student_result[i].user_id].icon;
                      }
                      this.coursework_list = this.temp_coursework_list;
                      //console.log(this.coursework_list);
                      if (this.coursework_list.length > 0) {
                        this.selected_coursework = this.coursework_list[0].id;
                        this.SelectedCourseworkChange(this.selected_coursework);
                      }
                    }
                  });
              }
            });
        }
      });
  }

  async purchaseAlert(){
    let msg_title = "請先購買";
    let msg_option_1 = "確定";
    if(this.global_service.lang == 'en'){
      msg_title = "Please login first";
      msg_option_1 = "Confirm";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "请先登入";
      msg_option_1 = "确定";
    }
    const alert = await this.alertCtrl.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [msg_option_1]
    });
    await alert.present();
  }

  async notLiveAlert(){
    let msg_title = "不是直播時間";
    let msg_option_1 = "確定";
    if(this.global_service.lang == 'en'){
      msg_title = "Current is not LIVE time";
      msg_option_1 = "Confirm";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "不是直播时间";
      msg_option_1 = "确定";
    }
    const alert = await this.alertCtrl.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [msg_option_1]
    });
    await alert.present();
  }

  async purchaseClassAndroid() {
    if (!this.logined) {
      let msg_title = "請先登入";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Please purchase first";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "请先购买";
        msg_option_1 = "确定";
      }
      const alert = await this.alertCtrl.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert.present();
    } else {

      if (this.course_data.price == 3) {
        let url = "https://intervii.com/backend/request.php";
        let headers = {
          "content-type": "application/x-www-form-urlencoded"
        };
        var json = {
          course_id: this.course_data.id,
          user_id: this.user_data.id
        };
        let body2 = { join_class: JSON.stringify(json) };
        // this.global_service.isLoading = true;
        // this.global_service.presentLoading();
        this.http
          .post(url, body2, headers)
          .then(async data => {
            // this.global_service.dismiss();
            ////console.log(data.data);
            if (
              data.data != undefined &&
              data.data != null &&
              data.data != ""
            ) {
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                this.storage.set("ios_purchased", null);
                let msg_title = "購買成功";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Purchase success";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "购买成功";
                  msg_option_1 = "确定";
                }
                const alert = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert.present();
              } else {
                let msg_title = "購買失敗，請聯絡我們";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Purchase fail, please contact us";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "购买失败，请联络我们";
                  msg_option_1 = "确定";
                }
                const alert = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert.present();
              }
              this.loadPurchase();
            }
          })
          .catch(error => { });
      } else {//if (this.course_data.price == 0 || this.course_data.price == 1 || this.course_data.price == 2) {

      this.payPal
        .init({
          // PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
          // PayPalEnvironmentSandbox: 'YOUR_SANDBOX_CLIENT_ID'
          // PayPalEnvironmentProduction: 'AZg3-2-_3DuHVSGn0CNs2XuTS0GbhlS7HC39O-B3JVpzCLDqDhvwnCrycPy9meCJsHcdN6ZtUWgEK2tY',
          // PayPalEnvironmentSandbox: ''
          PayPalEnvironmentSandbox:
            // "ATRaPkJVdyT1oXa9ZjqD7KkRU4zt2RRXwgPwBGt9Vrk70rPCIYLItD-NBNejPTGTdXgZqaMaRlTQGEsG",
            // "AVMyk0ewnXHkpUPa89YENy6Y1gqYIPaZjgUQFaDc6HM9tVZy93md2bS7tqlB0aa0IkQnWO97y-Fl65wD",
            "",
          PayPalEnvironmentProduction: "AQm5E9v8oLHEU9Arurzff8Csa3Fma1Dhd2S6rb8pZ-xUUgeSaHYCn2dd53qOrR4Mlxbl_a8xEoOhZy5-"
        })
        .then(
          async () => {
            var paypalconfig = new PayPalConfiguration({});

            this.payPal
              .prepareToRender("PayPalEnvironmentProduction", paypalconfig)
              .then(
                async () => {
                  var payment = new PayPalPayment(
                    "" + this.price_list[this.course_data.price],
                    "HKD",
                    "課堂費用",
                    "sale"
                  );
                  this.payPal.renderSinglePaymentUI(payment).then(
                    response => {
                      let url = "https://intervii.com/backend/request.php";
                      let payment_data = {
                        amount: this.price_list[this.course_data.price],
                        course_id: this.course_data.id,
                        payment_order_data: response,
                        payer: this.user_data.id,
                        receiver: this.course_data.lecturer_id
                      };
                      let body = { new_payment: JSON.stringify(payment_data) };
                      let headers = {
                        "content-type": "application/x-www-form-urlencoded"
                      };
                      this.http
                        .post(url, body, headers)
                        .then(async data => {
                          ////console.log(data.data);
                          if (
                            data.data != undefined &&
                            data.data != null &&
                            data.data != ""
                          ) {
                            let temp = JSON.parse(data.data);
                            if (temp.result == "success") {
                              // this.Confirm_Payment();

                              var json = {
                                course_id: this.course_data.id,
                                user_id: this.user_data.id
                              };
                              let body2 = { join_class: JSON.stringify(json) };
                              this.http
                                .post(url, body2, headers)
                                .then(async data => {
                                  ////console.log(data.data);
                                  if (
                                    data.data != undefined &&
                                    data.data != null &&
                                    data.data != ""
                                  ) {
                                    let temp = JSON.parse(data.data);
                                    if (temp.result == "success") {
                                      let msg_title = "購買成功";
                                      let msg_option_1 = "確定";
                                      if(this.global_service.lang == 'en'){
                                        msg_title = "Purchase success";
                                        msg_option_1 = "Confirm";
                                      }
                                      if(this.global_service.lang == 'cn'){
                                        msg_title = "购买成功";
                                        msg_option_1 = "确定";
                                      }
                                      const alert = await this.alertCtrl.create({
                                        message: msg_title,
                                        backdropDismiss:false,
                                        buttons: [msg_option_1]
                                      });
                                      await alert.present();
                                    } else {
                                      //console.log(data);
                                      let msg_title = "購買失敗，請聯絡我們";
                                      let msg_option_1 = "確定";
                                      if(this.global_service.lang == 'en'){
                                        msg_title = "Purchase fail, please contact us";
                                        msg_option_1 = "Confirm";
                                      }
                                      if(this.global_service.lang == 'cn'){
                                        msg_title = "购买失败，请联络我们";
                                        msg_option_1 = "确定";
                                      }
                                      const alert = await this.alertCtrl.create({
                                        message: msg_title,
                                        backdropDismiss:false,
                                        buttons: [msg_option_1]
                                      });
                                      await alert.present();
                                    }
                                    this.loadPurchase();
                                  }
                                })
                                .catch(error => {
                                  //console.log(error);
                                });
                            } else {
                              //console.log(data);
                let msg_title = "購買失敗，請聯絡我們";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Purchase fail, please contact us";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "购买失败，请联络我们";
                  msg_option_1 = "确定";
                }
                const alert = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                              await alert.present();
                            }
                          }
                        })
                        .catch(error => {
                          //console.log(error);
                        });
                    },
                    async () => {
                      let msg_title = "購買失敗，請聯絡我們";
                      let msg_option_1 = "確定";
                      if(this.global_service.lang == 'en'){
                        msg_title = "Purchase fail, please contact us";
                        msg_option_1 = "Confirm";
                      }
                      if(this.global_service.lang == 'cn'){
                        msg_title = "购买失败，请联络我们";
                        msg_option_1 = "确定";
                      }
                      const alert = await this.alertCtrl.create({
                        message: msg_title,
                        backdropDismiss:false,
                        buttons: [msg_option_1]
                      });
                      await alert.present();
                    }
                  );
                },
                async () => {
                  let msg_title = "購買失敗，請聯絡我們";
                  let msg_option_1 = "確定";
                  if(this.global_service.lang == 'en'){
                    msg_title = "Purchase fail, please contact us";
                    msg_option_1 = "Confirm";
                  }
                  if(this.global_service.lang == 'cn'){
                    msg_title = "购买失败，请联络我们";
                    msg_option_1 = "确定";
                  }
                  const alert = await this.alertCtrl.create({
                    message: msg_title,
                    backdropDismiss:false,
                    buttons: [msg_option_1]
                  });
                  await alert.present();
                }
              );
          },
          async () => {
            let msg_title = "購買失敗，請聯絡我們";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Purchase fail, please contact us";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "购买失败，请联络我们";
              msg_option_1 = "确定";
            }
            const alert = await this.alertCtrl.create({
              message: msg_title,
              backdropDismiss:false,
              buttons: [msg_option_1]
            });
            await alert.present();
          }
        );
      }
    }
  }
  async purchaseClassIOS() {

    if (!this.logined) {
      let msg_title = "請先登入";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Please login first";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "请先登入";
        msg_option_1 = "确定";
      }
      const alert = await this.alertCtrl.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert.present();
    } else {

      if (this.course_data.price == 3) {
        let url = "https://intervii.com/backend/request.php";
        let headers = {
          "content-type": "application/x-www-form-urlencoded"
        };
        var json = {
          course_id: this.course_data.id,
          user_id: this.user_data.id
        };
        let body2 = { join_class: JSON.stringify(json) };
        // this.global_service.isLoading = true;
        // this.global_service.presentLoading();
        this.http
          .post(url, body2, headers)
          .then(async data => {
            // this.global_service.dismiss();
            ////console.log(data.data);
            if (
              data.data != undefined &&
              data.data != null &&
              data.data != ""
            ) {
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                this.storage.set("ios_purchased", null);
                let msg_title = "購買成功";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Purchase success";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "购买成功";
                  msg_option_1 = "确定";
                }
                const alert = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert.present();
              } else {
                let msg_title = "購買失敗，請聯絡我們";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Purchase fail, please contact us";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "购买失败，请联络我们";
                  msg_option_1 = "确定";
                }
                const alert = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert.present();
              }
              this.loadPurchase();
            }
          })
          .catch(error => { });
      } else {// if (this.course_data.price == 0 || this.course_data.price == 1 || this.course_data.price == 2) {
        if (
          this.iosMatchedProduct != undefined &&
          this.iosMatchedProduct != null &&
          this.iosMatchedProduct != "" &&
          this.iosMatchedProduct.productId != undefined &&
          this.iosMatchedProduct.productId != null &&
          this.iosMatchedProduct.productId != ""
        ) {

          this.iap
            .buy(this.iosMatchedProduct.productId)
            .then(async data => {
              console.log(data);
              console.log(data.receipt);
              console.log(data.transactionId);

              this.storage.set("ios_purchased", {
                course_id: this.course_data.id,
                receipt: data.receipt,
                transaction_id: data.transactionId
              });

              let url = "https://intervii.com/backend/request.php";
              let body = {
                verify_app_store_receipt: JSON.stringify({
                  is_sandbox: this.global_service.iap_is_debug,
                  receipt: data.receipt,
                  amount: this.price_list[this.course_data.price],
                  course_id: this.course_data.id,
                  payer: this.user_data.id,
                  receiver: this.course_data.lecturer_id
                })
              };
              let headers = {
                "content-type": "application/x-www-form-urlencoded"
              };
              this.http
                .post(url, body, headers)
                .then(async data => {
                  console.log(data);
                  console.log(data.data);
                  if (
                    data.data != undefined &&
                    data.data != null &&
                    data.data != "" &&
                    data.data != "Recipt not exist"
                  ) {
                    var json = {
                      course_id: this.course_data.id,
                      user_id: this.user_data.id
                    };
                    let body2 = { join_class: JSON.stringify(json) };
                    this.http
                      .post(url, body2, headers)
                      .then(async data => {
                        ////console.log(data.data);
                        if (
                          data.data != undefined &&
                          data.data != null &&
                          data.data != ""
                        ) {
                          let temp = JSON.parse(data.data);
                          if (temp.result == "success") {
                            this.storage.set("ios_purchased", null);
                            let msg_title = "購買成功";
                            let msg_option_1 = "確定";
                            if(this.global_service.lang == 'en'){
                              msg_title = "Purchase success";
                              msg_option_1 = "Confirm";
                            }
                            if(this.global_service.lang == 'cn'){
                              msg_title = "购买成功";
                              msg_option_1 = "确定";
                            }
                            const alert = await this.alertCtrl.create({
                              message: msg_title,
                              backdropDismiss:false,
                              buttons: [msg_option_1]
                            });
                            await alert.present();
                          } else {
                            let msg_title = "購買失敗，請聯絡我們";
                            let msg_option_1 = "確定";
                            if(this.global_service.lang == 'en'){
                              msg_title = "Purchase fail, please contact us";
                              msg_option_1 = "Confirm";
                            }
                            if(this.global_service.lang == 'cn'){
                              msg_title = "购买失败，请联络我们";
                              msg_option_1 = "确定";
                            }
                            const alert = await this.alertCtrl.create({
                              message: msg_title,
                              backdropDismiss:false,
                              buttons: [msg_option_1]
                            });
                            await alert.present();
                          }
                          this.loadPurchase();
                        }
                      })
                      .catch(error => { });
                  } else {
                    let msg_title = "購買失敗，請聯絡我們";
                    let msg_option_1 = "確定";
                    if(this.global_service.lang == 'en'){
                      msg_title = "Purchase fail, please contact us";
                      msg_option_1 = "Confirm";
                    }
                    if(this.global_service.lang == 'cn'){
                      msg_title = "购买失败，请联络我们";
                      msg_option_1 = "确定";
                    }
                    const alert = await this.alertCtrl.create({
                      message: msg_title,
                      backdropDismiss:false,
                      buttons: [msg_option_1]
                    });
                    await alert.present();
                  }
                })
                .catch(error => { });
            })
            .catch(async err => {
              //console.log(err);
                let msg_title = "購買失敗，請聯絡我們";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Purchase fail, please contact us";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "购买失败，请联络我们";
                  msg_option_1 = "确定";
                }
                const alert = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
              await alert.present();
            });
        } else {
          let msg_title = "購買失敗，請聯絡我們";
          let msg_option_1 = "確定";
          if(this.global_service.lang == 'en'){
            msg_title = "Purchase fail, please contact us";
            msg_option_1 = "Confirm";
          }
          if(this.global_service.lang == 'cn'){
            msg_title = "购买失败，请联络我们";
            msg_option_1 = "确定";
          }
          const alert = await this.alertCtrl.create({
            message: msg_title,
            backdropDismiss:false,
            buttons: [msg_option_1]
          });
          await alert.present();
        }
      }
      //  else {
      //   let msg_title = "購買失敗，請聯絡我們";
      //   let msg_option_1 = "確定";
      //   if(this.global_service.lang == 'en'){
      //     msg_title = "Purchase fail, please contact us";
      //     msg_option_1 = "Confirm";
      //   }
      //   if(this.global_service.lang == 'cn'){
      //     msg_title = "购买失败，请联络我们";
      //     msg_option_1 = "确定";
      //   }
      //   const alert = await this.alertCtrl.create({
      //     message: msg_title,
      //     backdropDismiss:false,
      //     buttons: [msg_option_1]
      //   });
      //   await alert.present();
      // }
    }
  }

  async purchaseClassIOS_UnfinishedPurchase(course_id, receipt, transaction_id) {
    if (!this.logined) {
      let msg_title = "請先登入";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Please login first";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "请先登入";
        msg_option_1 = "确定";
      }
      const alert = await this.alertCtrl.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert.present();
    } else {
      let url = "https://intervii.com/backend/request.php";
      let body = {
        "verify_app_store_receipt": JSON.stringify({
          is_sandbox: this.global_service.iap_is_debug,
          receipt: receipt,
          amount: this.price_list[this.course_data.price],
          course_id: this.course_data.id,
          payer: this.user_data.id,
          receiver: this.course_data.lecturer_id
        })
      };
      let headers = {
        "content-type": "application/x-www-form-urlencoded"
      };
      this.http
        .post(url, body, headers)
        .then(async data => {
          //console.log(data);
          //console.log(data.data);
          if (
            data.data != undefined &&
            data.data != null &&
            data.data != "" &&
            data.data != "Recipt not exist"
          ) {
            var json = {
              course_id: course_id,
              user_id: this.user_data.id
            };
            let body2 = { join_class: JSON.stringify(json) };
            this.http
              .post(url, body2, headers)
              .then(async data => {
                ////console.log(data.data);
                if (
                  data.data != undefined &&
                  data.data != null &&
                  data.data != ""
                ) {
                  let temp = JSON.parse(data.data);
                  if (temp.result == "success") {
                    this.storage.set("ios_purchased", null);
                    let msg_title = "購買成功";
                    let msg_option_1 = "確定";
                    if(this.global_service.lang == 'en'){
                      msg_title = "Purchase success";
                      msg_option_1 = "Confirm";
                    }
                    if(this.global_service.lang == 'cn'){
                      msg_title = "购买成功";
                      msg_option_1 = "确定";
                    }
                    const alert = await this.alertCtrl.create({
                      message: msg_title,
                      backdropDismiss:false,
                      buttons: [msg_option_1]
                    });
                    await alert.present();
                  } else {
                    let msg_title = "購買失敗，請聯絡我們";
                    let msg_option_1 = "確定";
                    if(this.global_service.lang == 'en'){
                      msg_title = "Purchase fail, please contact us";
                      msg_option_1 = "Confirm";
                    }
                    if(this.global_service.lang == 'cn'){
                      msg_title = "购买失败，请联络我们";
                      msg_option_1 = "确定";
                    }
                    const alert = await this.alertCtrl.create({
                      message: msg_title,
                      backdropDismiss:false,
                      buttons: [msg_option_1]
                    });
                    await alert.present();
                  }
                  this.loadPurchase();
                }
              })
              .catch(error => { });
          } else {
            let msg_title = "購買失敗，請聯絡我們";
            let msg_option_1 = "確定";
            if(this.global_service.lang == 'en'){
              msg_title = "Purchase fail, please contact us";
              msg_option_1 = "Confirm";
            }
            if(this.global_service.lang == 'cn'){
              msg_title = "购买失败，请联络我们";
              msg_option_1 = "确定";
            }
            const alert = await this.alertCtrl.create({
              message: msg_title,
              backdropDismiss:false,
              buttons: [msg_option_1]
            });
            await alert.present();
          }
        })
        .catch(error => { });
    }
  }

  onSegmentChange($event) {
    ////console.log("onSegmentChange");
    let index = parseInt($event);
    // this.tabRef.select(index);
    this.selectedTab = index;
    ////console.log(this.selectedTab);
  }

  AddReview() {
    let url = "https://intervii.com/backend/request.php";
    var json = { course_id: this.course_data.id, user_id: this.user_data.id };
    let body = { check_review: JSON.stringify(json) };
    //let body = { "current_datetime_string" : ""};
    let headers = {
      "Content-Type": "application/x-www-form-urlencoded"
    };
    // this.global_service.isLoading = true;
    // this.global_service.presentLoading();
    this.http.post(url, body, headers).then(data => {
      ////console.log(data.data);
      // this.global_service.dismiss();
      let temp = JSON.parse(data.data);
      if (temp.result == "success") {
        if (temp.data == "empty") {
          this.reviewed = false;
          // this.app.getRootNav().push(AddReviewPage, {
          //   course_id: this.course_data.id,
          //   lecturer_id: this.course_data.lecturer_id,
          //   user_id: this.user_data.id
          // });
        this.global_service.resetLoading();
          this.navCtrl.navigateForward(['/add-review']);
        } else {
          this.reviewed = true;
        }
      } else {
        this.reviewed = false;
        // this.app.getRootNav().push(AddReviewPage, {
        //   course_id: this.course_data.id,
        //   lecturer_id: this.course_data.lecturer_id,
        //   user_id: this.user_data.id
        // });
        this.global_service.resetLoading();
        this.navCtrl.navigateForward(['/add-review']);
      }
    });
  }
  AddComment() {
    // this.app.getRootNav().push(AddQuestionPage, {
    //   course_id: this.course_data.id,
    //   lecturer_id: this.course_data.lecturer_id,
    //   user_id: this.user_data.id
    // });
        this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/add-question']);
  }
  AddCourseWork() {
    // this.app.getRootNav().push(AddCourseworkPage, { course: this.course_data });
        this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/add-coursework']);
  }
  async Edit() {
    if(this.global_service.user_data != undefined && this.global_service.user_data!=null && this.global_service.user_data!='' && this.global_service.user_data.is_lecturer!=undefined && this.global_service.user_data.is_lecturer!=null && (this.global_service.user_data.is_lecturer==true || this.global_service.user_data.is_lecturer=='true')){
      
      this.global_service.create_course = this.course_data;
      this.global_service.edit_course = true;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/build-course-index']);
    }else{

      let msg_title = "目前用戶類型不能開堂";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "Current user type cannot create course";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "目前用户类型不能开堂";
        msg_option_1 = "确定";
      }
      const alert_box = await this.alertCtrl.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }
  }
  async Refund() {
    let msg_title = "確認要退款嗎？";
    let msg_option_1 = "確定";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "Confirm Refund?";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "确认要退款吗？";
      msg_option_1 = "确定";
      msg_option_2 = "取消";
    }
    const alert = await this.alertCtrl.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [
        {
          text: msg_option_2,
          // role: 'cancel',
          handler: () => { }
        },
        {
          text: msg_option_1,
          // role: 'cancel',
          handler: () => {
            let url = "https://intervii.com/backend/request.php";
            var json = {
              course_id: this.course_data.id,
              user_id: this.user_data.id
            };
            let body = { refund_request: JSON.stringify(json) };
            //let body = { "current_datetime_string" : ""};
            let headers = {
              "Content-Type": "application/x-www-form-urlencoded"
            };
            // this.global_service.isLoading = true;
            // this.global_service.presentLoading();
            this.http.post(url, body, headers).then(async data => {
              ////console.log(data.data);
              // this.global_service.dismiss();
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                this.RefundDone();
              } else {
                let msg_title = "操作失敗";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Fail";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "操作失败";
                  msg_option_1 = "确定";
                }
                const alert_box = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert_box.present();
              }
            });
          }
        }
      ]
    });
    await alert.present();
  }
  async RefundDone() {
    let msg_title = "我們會於2個工作天內完成退款及聯絡你";
    let msg_option_1 = "確定";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "Refund will complete in 2 working days, and please wait for our contact";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "我们会於2个工作天内完成退款及联络你";
      msg_option_1 = "确定";
      msg_option_2 = "取消";
    }
    const alert = await this.alertCtrl.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [
        {
          text: msg_option_2,
          // role: 'cancel',
          handler: () => { }
        },
        {
          text: msg_option_1,
          // role: 'cancel',
          handler: () => { }
        }
      ]
    });
    await alert.present();
  }
  async DisableCourse() {
    let msg_title = "確定停用？";
    let msg_option_1 = "確定";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "Confirm inactive?";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "确定停用？";
      msg_option_1 = "确定";
      msg_option_2 = "取消";
    }
    const alert = await this.alertCtrl.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [
        {
          text: msg_option_2,
          // role: 'cancel',
          handler: () => { }
        },
        {
          text: msg_option_1,
          // role: 'cancel',
          handler: () => {
            let url = "https://intervii.com/backend/request.php";
            var json = { id: this.course_data.id, status: "disabled" };
            let body = { update_course: JSON.stringify(json) };
            //let body = { "current_datetime_string" : ""};
            let headers = {
              "Content-Type": "application/x-www-form-urlencoded"
            };
            // this.global_service.isLoading = true;
            // this.global_service.presentLoading();
            this.http.post(url, body, headers).then(async data => {
              // this.global_service.dismiss();
              ////console.log(data.data);
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                this.course_data.stauts = "disabled";
                // this.app.getRootNav().setRoot(TabsPage);
              } else {
                let msg_title = "操作失敗";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Fail";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "操作失败";
                  msg_option_1 = "确定";
                }
                const alert_box = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert_box.present();
              }
            });
          }
        }
      ]
    });
    await alert.present();
  }
  async EnableCourse() {
    let msg_title = "確定啓用？";
    let msg_option_1 = "確定";
    let msg_option_2 = "取消";
    if(this.global_service.lang == 'en'){
      msg_title = "Confirm active?";
      msg_option_1 = "Confirm";
      msg_option_2 = "Cancel";
    }
    if(this.global_service.lang == 'cn'){
      msg_title = "确定啓用？";
      msg_option_1 = "确定";
      msg_option_2 = "取消";
    }
    const alert = await this.alertCtrl.create({
      message: msg_title,
      backdropDismiss:false,
      buttons: [
        {
          text: msg_option_2,
          // role: 'cancel',
          handler: () => { }
        },
        {
          text: msg_option_1,
          // role: 'cancel',
          handler: () => {
            let url = "https://intervii.com/backend/request.php";
            var json = { id: this.course_data.id, status: "completed" };
            let body = { update_course: JSON.stringify(json) };
            //let body = { "current_datetime_string" : ""};
            let headers = {
              "Content-Type": "application/x-www-form-urlencoded"
            };
            // this.global_service.isLoading = true;
            // this.global_service.presentLoading();
            this.http.post(url, body, headers).then(async data => {
              ////console.log(data.data);
              // this.global_service.dismiss();
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                this.course_data.stauts = "completed";
                // this.app.getRootNav().setRoot(TabsPage);
              } else {
                let msg_title = "操作失敗";
                let msg_option_1 = "確定";
                if(this.global_service.lang == 'en'){
                  msg_title = "Fail";
                  msg_option_1 = "Confirm";
                }
                if(this.global_service.lang == 'cn'){
                  msg_title = "操作失败";
                  msg_option_1 = "确定";
                }
                const alert_box = await this.alertCtrl.create({
                  message: msg_title,
                  backdropDismiss:false,
                  buttons: [msg_option_1]
                });
                await alert_box.present();
              }
            });
          }
        }
      ]
    });
    await alert.present();
  }
  login_type: number = 0;
  async StartLive() {
    //console.log(this.live_link);
    if (this.live_link != undefined && this.live_link != "") {
      // window.open(this.live_link, '_system');
      if (this.live_link.includes("http://") || this.live_link.includes("https://")) {
        this.iab.create(this.live_link, "_system", 'location=no,hidden=no');
        // window.open(this.live_link,'_system');
      } else {
        this.iab.create("http://" + this.live_link, "_system", 'location=no,hidden=no');
        // window.open("http://" + this.live_link,'_system');
      }
    } else {
      let msg_title = "沒有直播連結";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "No Live Link";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "没有直播连结";
        msg_option_1 = "确定";
      }
      const alert_box = await this.alertCtrl.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }
    //   this.login_type = 1;
    //   // this.zoomService.logout()
    //   //   .then((success: boolean) => { this.LoginAndStartMeetingAction(); })
    //   //   .catch((error: any) => { this.LoginAndStartMeetingAction(); });
    //   if (this.platform.is("android")) {
    //     this.zoomService.logout()
    //       .then((success: boolean) => {
    //         //console.log("logout======================success");
    //         //console.log(success);
    //         this.LoginAndStartMeetingAction();
    //       })
    //       .catch((error: any) => {
    //         //console.log("logout======================fail");
    //         //console.log(error);
    //         this.LoginAndStartMeetingAction();
    //       });
    //   } else {
    //     this.zoomService.initialize("n9ZhU1CLI6yy2iPuX7ZFcRAzntjKe1Y9Pg4d", "sFBUBRAWs7mnfH8gdto4vCGiwOSv0bzcyTJx")
    //       .then((success: any) => {
    //         //console.log("init======================success");
    //         //console.log(success);
    //         this.zoomService.logout()
    //           .then((success: boolean) => {
    //             //console.log("logout======================success");
    //             //console.log(success);
    //             this.LoginAndStartMeetingAction();
    //           })
    //           .catch((error: any) => {
    //             //console.log("logout======================fail");
    //             //console.log(error);
    //             this.LoginAndStartMeetingAction();
    //           });
    //       })
    //       .catch((error: any) => {
    //         //console.log("init======================fail");
    //         //console.log(error);
    //       });
    //   }
  }
  async JoinLive() {
    if (this.live_link != undefined && this.live_link != "") {
      if (this.live_link.includes("http://") || this.live_link.includes("https://")) {
        // this.iab.create(this.live_link, "_blank", 'location=no,hidden=no');
        this.iab.create(this.live_link, "_system", 'location=no,hidden=no');
        // window.open(this.live_link,'_system');
        // window.open(this.live_link,'_system');
      } else {
        this.iab.create("http://" + this.live_link, "_system", 'location=no,hidden=no');
        // this.iab.create("http://" + this.live_link, "_blank", 'location=no,hidden=no');
        // window.open("http://" + this.live_link,'_system');
      }
      // this.iab.create(this.live_link);
    } else {
      let msg_title = "沒有直播連結";
      let msg_option_1 = "確定";
      if(this.global_service.lang == 'en'){
        msg_title = "No Live Link";
        msg_option_1 = "Confirm";
      }
      if(this.global_service.lang == 'cn'){
        msg_title = "没有直播连结";
        msg_option_1 = "确定";
      }
      const alert_box = await this.alertCtrl.create({
        message: msg_title,
        backdropDismiss:false,
        buttons: [msg_option_1]
      });
      await alert_box.present();
    }
    //   this.login_type = 0;

    //   if (this.platform.is("android")) {
    //     this.zoomService.logout()
    //       .then((success: boolean) => {
    //         //console.log("logout======================success");
    //         //console.log(success);
    //         this.LoginAndStartMeetingAction();
    //       })
    //       .catch((error: any) => {
    //         //console.log("logout======================fail");
    //         //console.log(error);
    //         this.LoginAndStartMeetingAction();
    //       });
    //   } else {
    //     this.zoomService.initialize("n9ZhU1CLI6yy2iPuX7ZFcRAzntjKe1Y9Pg4d", "sFBUBRAWs7mnfH8gdto4vCGiwOSv0bzcyTJx")
    //       .then((success: any) => {
    //         //console.log("init======================success");
    //         //console.log(success);
    //         this.zoomService.logout()
    //           .then((success: boolean) => {
    //             //console.log("logout======================success");
    //             //console.log(success);
    //             this.LoginAndStartMeetingAction();
    //           })
    //           .catch((error: any) => {
    //             //console.log("logout======================fail");
    //             //console.log(error);
    //             this.LoginAndStartMeetingAction();
    //           });
    //       })
    //       .catch((error: any) => {
    //         //console.log("init======================fail");
    //         //console.log(error);
    //       });
    //   }
  }

  // LoginAndStartMeetingAction() {
  //   let url = "https://intervii.com/backend/request.php";
  //   var json = { id: this.user_data.id };
  //   let body = { get_user_zoom: JSON.stringify(json) };
  //   //let body = { "current_datetime_string" : ""};
  //   let headers = {
  //     "Content-Type": "application/x-www-form-urlencoded"
  //   };
  //   // this.global_service.isLoading = true;
  //   // this.global_service.presentLoading();
  //   this.http.post(url, body, headers).then(async data => {
  //     // this.global_service.dismiss();
  //     let temp = JSON.parse(data.data);
  //     if (temp.result == "success") {
  //       this.zoomService.login(temp.data.zoom_ac, temp.data.zoom_pw)
  //         .then((success: any) => { //console.log(success); this.JoinMeetingAction2(); })
  //         .catch((error: any) => //console.log(error));
  //     } else {
  //       const alert2 = await this.alertCtrl.create({
  //         message: "操作失敗",
  //         buttons: ["確定"]
  //       });
  //       await alert2.present();
  //     }
  //   });
  // }

  // JoinMeetingAction2() {
  //   this.zoomService.joinMeeting(this.live_id + "", "qwe123!@#", this.user_data.display_name, {})
  //     .then((success: any) => //console.log(success))
  //     .catch((error: any) => {
  //       //console.log(error);

  //       if (this.login_type == 0) {
  //         alert("連接失敗，Tutor尚未開始課堂");
  //       }
  //       if (this.login_type == 1) {
  //         alert("連接失敗，請再重試一次");
  //       }
  //     });
  // }
  OpenClose(a) {
    // this.coursework_data[a].is_coursework_expend = !this.coursework_data[a].is_coursework_expend;
    // if (this.open_course == -1) {
    //   this.open_course = a;
    // } else {
    //   this.open_course = -1;
    // }
    // this.global_service.assignment_data = ;
    this.global_service.course_work_detail = this.coursework_data[a];
    this.global_service.course_purchase = this.purchase;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/course-coursework-detail']);


    // if (this.coursework_data[a].is_coursework_expend) {
    //   document.getElementById('item_' + a).className = "row_data_set open item item-block item-ios";
    //   for(var i=0; i<this.coursework_data.length; i++){
    //     if(i!=a){
    //       this.coursework_data[i].is_coursework_expend = false;
    //       document.getElementById('item_' + i).className = "row_data_set item item-block item-ios";
    //     }
    //   }
    // } else {
    //   document.getElementById('item_' + a).className = "row_data_set item item-block item-ios";
    //   for(var i=0; i<this.coursework_data.length; i++){
    //     if(i!=a){
    //       this.coursework_data[i].is_coursework_expend = false;
    //       document.getElementById('item_' + i).className = "row_data_set item item-block item-ios";
    //     }
    //   }
    // }
  }
  CloseAll() {
    for (let i = 0; i < this.coursework_data.length; i++) {
      this.coursework_data[i].is_coursework_expend = false;
      // document.getElementById('item_' + i).className = "row_data_set item item-block item-ios";
    }
  }
  CommentDetail(comment) {
    // this.app.getRootNav().push(HomeLatestCourseCommentDetailPage, { comment_detail: comment });
    this.global_service.comment_detail = comment;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/course-comment-detail']);
  }
  SearchContent(term) {
    for (let i = 0; i < this.question_data.length; i++) {
      if (this.search_term == "" || (this.question_data[i].title.includes(this.search_term) ||
        this.question_data[i].display_name.includes(this.search_term) ||
        this.question_data[i].question_time.includes(this.search_term))) {
        this.question_data[i].hide = false;
      } else {
        this.question_data[i].hide = true;
      }
    }
  }
  OpenQuestionDetail(q) {
    // this.app.getRootNav().push(HomeLatestCourseQuestionDetailPage, { question_detail: q, course_detail:this.data_package, purchase:this.purchase });
    this.global_service.question_detail = q;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/course-question-detail']);
  }
  SelectedCourseworkChange($event) {
    //console.log($event);
    this.current_assignment = $event;
    // for (let i = 0; i < this.coursework_student_result.length; i++) {
    //   if (this.coursework_student_result[i].course_track == $event) {
    //     this.coursework_student_result[i].is_belong_course = true;
    //   } else {
    //     this.coursework_student_result[i].is_belong_course = false;
    //   }
    // }
  }
  showResult(data) {
    //console.log(data);
    // this.app.getRootNav().push(HomeLatestCourseResultDetailPage, { assignment_data: data, course_detail: this.data_package });
    this.global_service.assignment_data = data;
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/course-result-detail']);
  }


  async open(index) {
    if (this.purchase || this.course_data.lecturer_id == this.user_data.id) {
      // let options: StreamingVideoOptions = {
      //   successCallback: () => { console.log('Video played') },
      //   errorCallback: (e) => { console.log('Error streaming') },
      //   orientation: 'landscape',
      //   shouldAutoClose: true,
      //   controls: true
      // };

      // this.streamingMedia.playVideo(('https://intervii.com/media/' + this.videos[index].file_name), options);
      this.global_service.playURL('https://intervii.com/media/' + this.videos[index].file_name);
    }else{
      this.purchaseAlert();
    }
  }
  
  openVimeo(index){
    if (this.purchase || this.course_data.lecturer_id == this.user_data.id) {
      this.global_service.playVimeo(this.videos[index].file_vimeo);
    }else{
      this.purchaseAlert();
    }
  }
  async openPreview(index) {


    // // if (this.plt.is('android')) {
    // //   this.player.play(('https://intervii.com/media/'+this.videos[index].preview_file_name),{
    // //       scalingMode: 1
    // //   }).then(() => {
    // //    //console.log('video completed');
    // //   }).catch(err => {
    // //    //console.log(err);
    // //   });
    // // }else{

    // let options: StreamingVideoOptions = {
    //   successCallback: () => { console.log('Video played') },
    //   errorCallback: (e) => { console.log('Error streaming') },
    //   orientation: 'landscape',
    //   shouldAutoClose: true,
    //   controls: true
    // };
    console.log(index);
    console.log(this.videos[index].preview_file_name);


    // this.streamingMedia.playVideo(('https://intervii.com/media/' + this.videos[index].preview_file_name), options);
    this.global_service.playURL('https://intervii.com/media/' + this.videos[index].preview_file_name);
    // }
  }
  
  openVimeoPreview(index){
    // if (this.purchase || this.course_data.lecturer_id == this.user_data.id) {
    console.log(index);
    console.log(this.videos[index].preview_file_vimeo);
      this.global_service.playVimeo(this.videos[index].preview_file_vimeo);
    // }
  }

  GoProfile(id) {
    // this.app.getRootNav().push(MyaccountPage, { id: id });
    if (this.user_data == undefined || this.user_data == null || this.user_data.id == 0) {
    } else {
      // this.app.getRootNav().push(MyaccountPage, { id: id });
      this.global_service.view_user_id = id;
      this.global_service.resetLoading();
      this.navCtrl.navigateForward(['/myaccount']);
    }
  }
  async Report() {
    if (!this.pressed) {
      this.pressed = true;
      const alert_box = await this.alertCtrl.create({
        message: '確認要回報直播課堂失實？',
        cssClass: 'custom-alertInfo',
        backdropDismiss:false,
        buttons: [{
          text: '確認', handler: () => {
            let url = "https://intervii.com/backend/request.php";
            var json = { course_id: this.course_data.id, user_id: this.user_data.id, reason: "Live lesson missing" };
            let body = { course_report: JSON.stringify(json) };
            let headers = {
              "Content-Type": "application/x-www-form-urlencoded"
            };
            this.http.post(url, body, headers).then(async data => {
              let temp = JSON.parse(data.data);
              if (temp.result == "success") {
                const alert2 = await this.alertCtrl.create({
                  message: "回報成功",
                  backdropDismiss:false,
                  buttons: ["確定"]
                });
                await alert2.present();
              } else {
                const alert2 = await this.alertCtrl.create({
                  message: "回報失敗",
                  backdropDismiss:false,
                  buttons: ["確定"]
                });
                await alert2.present();
              }
              this.pressed = false;
            });
          }
        },
        {
          text: '取消', handler: () => {
            this.pressed = false;
          }
        }]
      });
      await alert_box.present();
    }
  }

  AddStudent() {
    this.global_service.resetLoading();
    this.navCtrl.navigateForward(['/search-user']);
  }
}
